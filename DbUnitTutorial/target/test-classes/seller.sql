create table seller (
    id nvarchar2(20) not null,
    name nvarchar2(4) not null,
    email nvarchar2(50) not null
);

alter table seller add (
    constraint pk_seller
    primary key (id)
    using index
);

comment on table seller is '판매자 테이블';
comment on column seller.id is '판매자 아이디';
comment on column seller.name is '판매자 이름';
comment on column seller.email is '판매자 이메일';