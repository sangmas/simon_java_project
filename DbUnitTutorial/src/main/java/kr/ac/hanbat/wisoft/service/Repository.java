package kr.ac.hanbat.wisoft.service;

import java.util.List;

import kr.ac.hanbat.wisoft.domain.Seller;

public interface Repository {

	public void Add(Seller seller);
	
	public Seller FindById(String id);
	
	public void Update(Seller seller);
	
	public void Remove(Seller seller);
	
	public List<Seller> getSellerList();
	
}
