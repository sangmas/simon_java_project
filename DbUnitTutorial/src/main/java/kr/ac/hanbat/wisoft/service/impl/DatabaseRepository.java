package kr.ac.hanbat.wisoft.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import kr.ac.hanbat.wisoft.domain.Seller;
import kr.ac.hanbat.wisoft.service.Repository;

public class DatabaseRepository implements Repository {
	private static final String DBUNIT_DRIVER_CLASS = "oracle.jdbc.driver.OracleDriver";
    private static final String DBUNIT_CONNECTION_URL = "jdbc:oracle:thin:@203.230.100.55:1521:hsora";
    private static final String DBUNIT_USERNAME = "C##SHKIM";
    private static final String DBUNIT_PASSWORD = "dblab321";
    private static final String DBUNIT_SCHEMA = "C##SHKIM";
    
    private Connection conn;
    
    public DatabaseRepository() throws Exception{
    	Class.forName(DBUNIT_DRIVER_CLASS).newInstance();
    	conn = DriverManager.getConnection(DBUNIT_CONNECTION_URL, DBUNIT_USERNAME, DBUNIT_PASSWORD);
    }

	public void Add(Seller seller) {
		// TODO Auto-generated method stub
		PreparedStatement psmt = null;
		ResultSet rs = null;
		
		try{
			String query = "insert into seller values(?,?,?)";
			psmt = conn.prepareStatement(query);
			psmt.setString(1, seller.getId());
			psmt.setString(2, seller.getName());
			psmt.setString(3, seller.getEmail());
			
			psmt.executeUpdate();
		} catch (SQLException sqle){			
	} catch (Exception e) {			
	} finally{
		if(rs !=null) try{rs.close();} catch(Exception e) {}
		if(psmt !=null)	try{psmt.close();} catch(Exception e) {}
		if(conn != null)try {conn.close();} catch(Exception e) {}
	}
		
	}

	public Seller FindById(String id) {
		// TODO Auto-generated method stub
		PreparedStatement psmt = null;
		ResultSet rs = null;
		Seller seller = null;
		
		try{
			String query = "select * from seller where id = ?";
			psmt = conn.prepareStatement(query);
			psmt.setString(1, id);
			rs = psmt.executeQuery();
			
			if(!rs.next()){
				throw new SQLException("No Data Found");
			} 
			seller = new Seller(rs.getString("ID"), rs.getString("NAME"), rs.getString("EMAIL"));
		} catch (SQLException sqle){				
		} catch (Exception e) {} finally{if(rs !=null)
			try{rs.close();} catch(Exception e) {}
			if(psmt !=null)try{	psmt.close();} catch(Exception e) {}
			if(conn != null)try {conn.close();} catch(Exception e) {}				
	}
		return seller;
	}

	public void Update(Seller seller) {
		// TODO Auto-generated method stub
		PreparedStatement psmt = null;
		ResultSet rs = null;
		
		try{
			String query = "update seller set id=?, name=?, email=? where id = ?";
			psmt = conn.prepareStatement(query);
			psmt.setString(1, seller.getId());
			psmt.setString(2, seller.getName());
			psmt.setString(3, seller.getEmail());
			psmt.setString(4, seller.getId());
			rs = psmt.executeQuery();
			
			} catch ( Exception e ) {
	         e.printStackTrace();
			} finally {	         
	         if (psmt != null) try {psmt.close();} catch(SQLException ex){} 
	         if (conn != null) try {conn.close();} catch(SQLException ex){}
	      }   
	}

	public void Remove(Seller seller) {
		// TODO Auto-generated method stub
	     PreparedStatement psmtQuery = null;
	      
	      try {
	         String query = "DELETE FROM SELLER WHERE ID = ?";
	         psmtQuery = conn.prepareStatement(query);
	         psmtQuery.setString(1, seller.getId());
	         psmtQuery.executeQuery();
	         
	      } catch ( Exception e ) {
	         e.printStackTrace();
	      } finally {         
	         if (psmtQuery != null) try {psmtQuery.close();} catch(SQLException ex){} 
	         if (conn != null) try {conn.close();} catch(SQLException ex){}
	      }   
	}

	public List<Seller> getSellerList() {
		// TODO Auto-generated method stub
		return null;
	}
}
