package kr.ac.hanbat.wisoft.service.impl;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import kr.ac.hanbat.wisoft.domain.Seller;
import kr.ac.hanbat.wisoft.service.Repository;

import org.dbunit.Assertion;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.SortedTable;
import org.dbunit.dataset.csv.CsvDataSet;
import org.dbunit.dataset.csv.CsvDataSetWriter;
import org.dbunit.dataset.excel.XlsDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.oracle.OracleDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DatabaseRepositoryTest {
    private static final String DBUNIT_DRIVER_CLASS = "oracle.jdbc.driver.OracleDriver";
    private static final String DBUNIT_CONNECTION_URL = "jdbc:oracle:thin:@203.230.100.55:1521:hsora";
    private static final String DBUNIT_USERNAME = "C##SHKIM";
    private static final String DBUNIT_PASSWORD = "dblab321";
    private static final String DBUNIT_SCHEMA = "C##SHKIM";

    // DbUnit에서 제공하는 DBTestCase를 상속
    private IDatabaseTester databaseTester;
    private IDatabaseConnection connection;

	@Before
	public void setUp() throws Exception {
		// IDatabaseTester 구현체
		databaseTester = new JdbcDatabaseTester(DBUNIT_DRIVER_CLASS, DBUNIT_CONNECTION_URL, DBUNIT_USERNAME, DBUNIT_PASSWORD, DBUNIT_SCHEMA);
        connection = databaseTester.getConnection();
        
        // Oracle 전용 데이터타입을 지원하기 위한 설정
        DatabaseConfig config = connection.getConfig();
        config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new OracleDataTypeFactory());
        
        // 데이터 셋 지정
        IDataSet dataSet = new FlatXmlDataSetBuilder().build(new File("src/test/resources/seller.xml"));
        // 데이터베이스 테이블의 내용을 모두 삭제한 후, 데이터 셋의 값으로 데이터를 등록
        DatabaseOperation.CLEAN_INSERT.execute(connection, dataSet);
	}

	@After
	public void tearDown() throws Exception {
		this.connection.close();
	}

	
	@Test
	public void Test1FindById() throws Exception {
		Seller expectedSeller = new Seller("shkim", "김상현", "shkim@hanbat.ac.kr");
		
		Repository repository = new DatabaseRepository();
		Seller actualSeller = repository.FindById("shkim");
		
		assertEquals(expectedSeller.getId(), actualSeller.getId());
		assertEquals(expectedSeller.getName(), actualSeller.getName());
		assertEquals(expectedSeller.getEmail(), actualSeller.getEmail());
	}

	
	@Test
	public void Test2Add() throws Exception {
		Seller newSeller = new Seller("csoh", "오창세", "csoh@hanbat.ac.kr");
		
		Repository repository = new DatabaseRepository();
		repository.Add(newSeller);
		
		IDataSet currentDbDataSet = databaseTester.getConnection().createDataSet();
		ITable actualTable = currentDbDataSet.getTable("seller");
		
		IDataSet expectedDataSet = new FlatXmlDataSetBuilder().build(new File("src/test/resources/expected_seller.xml"));
		ITable expectedTable = expectedDataSet.getTable("seller");
		
		Assertion.assertEquals(new SortedTable(expectedTable), actualTable);
		
	}
	
	@Test
	public void Test3Update() throws Exception{
		Seller updateSeller = new Seller("hsjeon", "전현식", "ngelmaum@hanbat.ac.kr");
		Repository repository = new DatabaseRepository();
		repository.Update(updateSeller);
		
		IDataSet currentDbDataSet = databaseTester.getConnection().createDataSet();
		ITable actualTable = currentDbDataSet.getTable("seller");
		
		IDataSet expectedDataSet = new XlsDataSet(new FileInputStream(new File("src/test/resources/expected_seller.xls")));
		ITable expectedTable = expectedDataSet.getTable("seller");
		
		Assertion.assertEquals(new SortedTable(expectedTable), actualTable);
	}

	@Test
	public void Test4Delete() throws Exception{
		Seller removeSeller = new Seller("hsjeon", "전현식", "hsjeon@hanbat.ac.kr");
		Repository repository = new DatabaseRepository();
		repository.Remove(removeSeller);
		
		IDataSet currentDbDataSet = databaseTester.getConnection().createDataSet();
		ITable actualTable = currentDbDataSet.getTable("seller");
		
		IDataSet expectedDataSet = new CsvDataSet(new File("src/test/resources/expected_seller"));
		ITable expectedTable = expectedDataSet.getTable("expected_seller");
		
		Assertion.assertEquals(new SortedTable(expectedTable), actualTable);
		
	}
	
	@Test
	public void Test5GetSellerList() throws Exception{
		QueryDataSet dataSet = new QueryDataSet(connection);
		dataSet.addTable("seller", "SELECT * FROM SELLER");
		FlatXmlDataSet.write(dataSet, new FileOutputStream("src/test/resources/seller_output.xml"));
		XlsDataSet.write(dataSet, new FileOutputStream("src/test/resources/seller_output.xls"));
		CsvDataSetWriter.write(dataSet, new File("src/test/resources/seller_output"));
	}
}
