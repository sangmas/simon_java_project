package kr.ac.hanbat.wisoft.domain;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

@Slf4j
public class MemberTest {
	
	List<Member> members = new ArrayList<Member>();

	@Test
	public void test(){
		Member member;		
		member = new Member("wow","simon","sangmaz@naver.com");		
		log.info("register simon");		
		RegMember(member);		
		
		log.info("check member Reg");
		boolean result = CheckMember(member);
		assertTrue(result);		
		
		member = new Member("oocs","cs","sangmas@naver.com");		
		log.info("register cs");		
		RegMember(member);	
		
		//chech member's name
		log.info("check cs's name");
		String memberName = GetMemberName(member);
		assertEquals(member.getName(), memberName);
		
		//all of member's checking
		log.info("allofName");
		GetMemberList();
		
		//remove member!
		log.info("remove member");
		RemoveMember(member);
		GetMemberList();
	}
	
	private void RegMember(Member member){
		members.add(member);
	}
	
	private boolean CheckMember(Member member){
		return members.contains(member);
	}
	
	private String GetMemberName(Member member){
		boolean result = members.contains(member);
		
		if(result){
			System.out.println("Member's name : "+member.getName());
			return member.getName();
		} else {
			return null;
		}
	}
	
	private void GetMemberList() {
		for(Member member : members)
			System.out.println("ID is "+ member.getId()+"and E mail is"+member.getEmail());
	}
	
	private void RemoveMember(Member member){
		members.remove(member);
	}
	
}
