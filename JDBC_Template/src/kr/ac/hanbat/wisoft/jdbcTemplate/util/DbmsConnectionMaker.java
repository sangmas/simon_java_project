package kr.ac.hanbat.wisoft.jdbcTemplate.util;

import java.sql.Connection;

public interface DbmsConnectionMaker {
	void init();

	Connection getConnection();
}
