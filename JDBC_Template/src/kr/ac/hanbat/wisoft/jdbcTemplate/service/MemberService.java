package kr.ac.hanbat.wisoft.jdbcTemplate.service;

import java.util.List;

import kr.ac.hanbat.wisoft.jdbcTemplate.domain.Member;

public interface MemberService {
  List<Member> getMemberList();
}
