package kr.ac.hanbat.wisoft.jdbcTemplate.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import kr.ac.hanbat.wisoft.jdbcTemplate.domain.Member;
import kr.ac.hanbat.wisoft.jdbcTemplate.util.DbmsConnectionMaker;
import kr.ac.hanbat.wisoft.jdbcTemplate.util.MySQLDbmsConnectionMaker;

public class MySQLMemberService implements MemberService {
	Connection connection;
	DbmsConnectionMaker DbmsConnectionMaker = new MySQLDbmsConnectionMaker();

	@Override
	public List<Member> getMemberList() {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Member> members = new ArrayList<Member>();

		try {
			connection = DbmsConnectionMaker.getConnection();
			String query = "select * from members";

			preparedStatement = connection.prepareStatement(query);
			resultSet = preparedStatement.executeQuery();

			Member member;
			while (resultSet.next()) {
				member = new Member();
				member.setNo(resultSet.getInt("MNO"));
				member.setName(resultSet.getString("MNAME"));
				member.setEmail(resultSet.getString("EMAIL"));
				member.setPassword(resultSet.getString("PWD"));
				member.setCreatedDate(resultSet.getDate("CRE_DATE"));
				member.setModifiedDate(resultSet.getDate("MOD_DATE"));

				members.add(member);
				// members.add(new Member(resultSet.getInt("MNO"), resultSet
				// .getString("MNAME"), resultSet.getString("EMAIL"),
				// resultSet.getString("PWD"), resultSet
				// .getDate("CRE_DATE"), resultSet
				// .getDate("MOD_DATE")));
			}

			return members;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (resultSet != null)
				try {
					resultSet.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if (preparedStatement != null)
				try {
					preparedStatement.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if (connection != null)
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}

		return null;
	}
}
