package kr.ac.hanbat.wisoft.jdbcTemplate;

import java.util.ArrayList;
import java.util.List;

import kr.ac.hanbat.wisoft.jdbcTemplate.domain.Member;
import kr.ac.hanbat.wisoft.jdbcTemplate.service.MemberService;
import kr.ac.hanbat.wisoft.jdbcTemplate.service.MySQLMemberService;

public class Application {

	public static void main(String[] args) {
		System.out.println("Starting Program...");

		MemberService memberService = new MySQLMemberService();

		List<Member> members = new ArrayList<>();

		members = memberService.getMemberList();

		for (Member member : members) {
			System.out.println(member.getName());
		}

		System.out.println("Terminated Program...");
	}
}
