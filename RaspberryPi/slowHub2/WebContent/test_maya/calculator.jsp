<%--
/*
 ***************************************************************************************
 * @project  : 이벤트플레이스(2013) 
 * @source   : 이벤트
 * @desc     : 
 * -----------------------------------------------------------------------------------
 * VER      DATE       AUTHOR    DESCRIPTION
 * ---   ----------  ----------  -----------------------------------------------------
 * 1.0   2013.07.20    김영돈            최초 프로그램 작성    
 * -----------------------------------------------------------------------------------
 * Copyright(c) 2009 maya ,  All rights reserved.
 ***************************************************************************************
 */
--%>
<%@include file="../../comm/header.jsp" %>
<%@page import="maya.util.*" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
<%
	request.setCharacterEncoding("UTF-8");
	String pMode = Util.null2str(request.getParameter("a"));
	
	System.out.println("모드 :"+pMode); 
	if("insert".equals(pMode)){
		out.println("등록 처리중입니다.");
	}else if("modify".equals(pMode)){
		out.println("수정 처리중입니다.");
	}else if("delete".equals(pMode)){
		out.println("삭제 처리중입니다.");
		
	} else {
		%>
			<script>
				alert("잘못된 접근입니다.");
				//history.back();
			</script>
		<%
		return;
	}
	
	//권한체크
	
	/*
	if(true){
		//한글 체크
		System.out.println("한글파라메터 : "+da.event_name);
		//return;
	}
	//처리 분기 
	if("insert".equals(pMode)){//등록모드
		da.cre_id =  _authBean.sId; 
		EventDao dao = new EventDao();  
		result = dao.insert(da);
		if(result>0){
			_authBean.messageOutReplace("이벤트  등록 성공","./eventList.jsp");
		}else{
			_authBean.messageOutBack("이벤트 등록 실패");
		}
		
	} else if("modify".equals(pMode)){
		//기존파일 삭제
		if(null!=mfile.getFileParameter(file_param)){
			EventVO old_da = new EventVO();
			old_da = EventDao.getRow(da.event_seq);
			mfile.delFileReal(request.getRealPath("/")+old_da.thumbnail_image);
		}
		/*
		if(!_authBean.isAdmin()){
			_authBean.messageOutBack("관리자만 접근 가능합니다.");
			return;
		}*/
		/*
		out.println("이벤트 정보 수정중");
		result = EventDao.update(da); 
		if(result>0){
			//ActionBean.actionLog("이벤트 정보 수정", da.event_seq, _authBean.sId,request.getRemoteAddr());
			_authBean.messageOutReplace("이벤트 정보 수정 성공","./eventList.jsp");
		}else{
			_authBean.messageOutBack("이벤트 정보 수정 실패");
		}
	*/
%>
