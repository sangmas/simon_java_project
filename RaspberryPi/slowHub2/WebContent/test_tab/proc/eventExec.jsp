<%--
/*
 ***************************************************************************************
 * @project  :  이벤트플레이스(2013) 
 * @source   : 이벤트
 * @desc     : 
 * -----------------------------------------------------------------------------------
 * VER      DATE       AUTHOR    DESCRIPTION
 * ---   ----------  ----------  -----------------------------------------------------
 * 1.0   2013.07.20    김영돈            최초 프로그램 작성    
 * -----------------------------------------------------------------------------------
 * Copyright(c) 2009 maya ,  All rights reserved.
 ***************************************************************************************
 */
--%>
<%@page import="maya.util.MayaFileUp"%>
<%@page import="maya.mboard.MfileArticle"%> 
<%@page import="ev.event2.*"%>
<%@page import="ep.ActionLog.ActionBean"%> 
<%@page import="maya.util.Util"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.oreilly.servlet.MultipartRequest" %>
<%@page import="java.text.*"%>
<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@ include file="../../comm/header.jsp" %>

<%

if(!_authBean.isAdmin()){
	_authBean.messageOutBack("관리자만 접근할 수 있습니다.");
	return;
}  
	request.setCharacterEncoding("UTF-8");

	//첨부파일저장
	MayaFileUp  mfile = new MayaFileUp(); 
	String pMode =  request.getParameter("pMode"); 
	String saveDir = _saveDir;//저장경로지정    
	mfile.setSaveDir(request.getRealPath("/")+saveDir);
	if(pMode==null)	{
		mfile.doPost(request, response);//multipart 파라메터처리 & 파일 업로드
		pMode = Util.null2str(mfile.getParameter("pMode")); 
	}

	int result = 0;
	
	EventVO da = new EventVO();
	 
	da.event_seq = Util.xssReplace(mfile.getParameter("event_seq")); 
	da.event_name = Util.xssReplace(mfile.getParameter("event_name"));
	da.start_dt = Util.xssReplace(mfile.getParameter("start_dt"));
	da.end_dt = Util.xssReplace(mfile.getParameter("end_dt"));
	da.host_name = Util.xssReplace(mfile.getParameter("host_name"));
	da.host_tel = Util.xssReplace(mfile.getParameter("host_tel"));
	da.host_type = Util.xssReplace(mfile.getParameter("host_type"));
	da.host_addr = Util.xssReplace(mfile.getParameter("host_addr"));
	da.progress_status = Util.xssReplace(mfile.getParameter("progress_status"));
	
	//da.host_addr2 = Util.xssReplace(mfile.getParameter("event_name"));

	da.notice_period = Util.null2zero(Util.xssReplace(mfile.getParameter("notice_period")));
	da.reappl_time = Util.null2zero(Util.xssReplace(mfile.getParameter("reappl_time")));
	da.thumbnail_image = Util.xssReplace(mfile.getParameter("thumbnail_image"));
	da.supervision_client = Util.xssReplace(mfile.getParameter("supervision_client"));
	da.service_charge_normal_price = Util.null2zero(Util.xssReplace(mfile.getParameter("service_charge_normal_price")));
	da.service_charge_dis_rate = Util.null2zero(Util.xssReplace(mfile.getParameter("service_charge_dis_rate")));
	da.application_capacity = Util.xssReplace(mfile.getParameter("application_capacity"));
	da.info_offer_flg = Util.xssReplace(mfile.getParameter("info_offer_flg"));
	da.client_seq = Util.null2zero(Util.xssReplace(mfile.getParameter("client_seq")));
	
	

	MfileArticle mfa = new MfileArticle();
	String file_param = "upfile1";
	if(null!=mfile.getFileParameter(file_param)){
		mfa.targetfile = saveDir+"/"+mfile.getFileParameter(file_param);
		mfa.filename = Util.null2str(mfile.getFileParameter(file_param+"_name"));
		mfa.filesize = Util.null2str(mfile.getFileParameter(file_param+"_size"));
		da.thumbnail_image = mfa.targetfile;
	}
	System.out.println(">>>>up : "+saveDir+"/"+mfile.getFileParameter(file_param));
	
	
	System.out.println("모드 :"+pMode); 
	//권한체크
	
	if(true){
		//한글 체크
		System.out.println("한글파라메터 : "+da.event_name);
		//return;
	}
	//처리 분기 
	if("insert".equals(pMode)){//등록모드
		da.cre_id =  _authBean.sId; 
		EventDao dao = new EventDao();  
		result = dao.insert(da);
		if(result>0){
			_authBean.messageOutReplace("이벤트  등록 성공","./eventList.jsp");
		}else{
			_authBean.messageOutBack("이벤트 등록 실패");
		}
		
	} else if("modify".equals(pMode)){
		//기존파일 삭제
		if(null!=mfile.getFileParameter(file_param)){
			EventVO old_da = new EventVO();
			old_da = EventDao.getRow(da.event_seq);
			mfile.delFileReal(request.getRealPath("/")+old_da.thumbnail_image);
		}
		/*
		if(!_authBean.isAdmin()){
			_authBean.messageOutBack("관리자만 접근 가능합니다.");
			return;
		}*/
		out.println("이벤트 정보 수정중");
		result = EventDao.update(da); 
		if(result>0){
			//ActionBean.actionLog("이벤트 정보 수정", da.event_seq, _authBean.sId,request.getRemoteAddr());
			_authBean.messageOutReplace("이벤트 정보 수정 성공","./eventList.jsp");
		}else{
			_authBean.messageOutBack("이벤트 정보 수정 실패");
		}
	} else {
		_authBean.messageOutBack(" 잘못된 접근입니다.");
	}
	
%>
