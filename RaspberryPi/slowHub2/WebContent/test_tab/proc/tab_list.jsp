<%--
/*
 ***********************************************************************************
 * @project  : 이벤트플레이스(2013) 
 * @source   : 이벤트 등록
 * @desc     : 
 *----------------------------------------------------------------------
 * VER      DATE       AUTHOR    DESCRIPTION
 * ---   ----------  ----------  ------------------------------------------
 * 1.0   2013.07.22    김영돈    최초 프로그램 작성       
 * ----------- ----------  -----------------------------------------------
 * Copyright(c) 2010 maya ,  All rights reserved.
 *************************************************************************
 */
--%> 
<%@page import="maya.util.Util"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="slowHub.testTab.TabDao"%>
<%@page import="slowHub.testTab.TabVO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="htmlParser.HMParser" %> 
<%@ include file="../../comm/header.jsp" %>
<%
	//외부 파라메터 받기
	//String seq = Util.requestParamter(request, "seq");
String keyword = Util.requestParamter(request, "keyword");
String orderkey = Util.requestParamter(request, "orderkey");
	String appendUrl = "";
	appendUrl += "&keyword="+keyword;
	appendUrl += "&orderkey="+orderkey;
	String curr_page = Util.requestParamter(request, "page");
	int startIdx = 1;
	int ipp = 10;
	if(!"".equals(curr_page)){
		startIdx = (Integer.parseInt(curr_page)-1) * ipp+1;
	}
	
	String serverRoot = pageContext.getServletContext().getRealPath("/");//0.파일 상대경로 찾기
	HMParser parser = new HMParser(_serverRoot+"/test_tab/html/tab_list.html");//1.html 파일을 parser 읽어 들임
	parser.regBlock("list");// T 2.1 블럭등록 및 템플릿 변수 치환
	//parser.regBlock("list_null");
	List<TabVO> list_tab = new ArrayList<TabVO>();
	//list_tab = TabDao.getList();
	TabVO p_tvo = new TabVO();
	int list_tab_count = TabDao.getListCnt(p_tvo);
	//PageUtil.makePage(전체페이지수, 현페이지, 추가URL);
	// 기본값 : ipp=10, bpp=5;
	//PageUtil.getPageStr();
	
	int total_page = (list_tab_count/ ipp)+1; 
	String page_str ="";//1 2  13
	int int_curr_page = Integer.parseInt(curr_page);
	int bpp = 5;
	int block_start  = (int_curr_page / bpp ) * bpp + 1  ;
	int block_end = block_start + bpp - 1;
	if(block_end>total_page){
		block_end = total_page;
	}
	page_str += "<a href='?page=1'>1</a>...";
	int prev_start = block_start - bpp;
	int next_start = block_start + bpp;
	String prev_block = "";
	if((int_curr_page-bpp) > 0){
		prev_block = " <a href='?page="+prev_start+appendUrl+"'><<이전</a> ";
	}
	String next_block =  "";
	if((int_curr_page+bpp)< total_page){
		next_block =  " <a href='?page="+next_start+appendUrl+"'>다음>></a> ";
	}
	for(int i=block_start;i<=block_end;i++){
		if(int_curr_page==i){
			page_str += "<B><font size='9'>"+i+"</font></B>";
		} else {
			page_str += " <a href='?page="+i+appendUrl+"'>"+i+"</a>";
		}
	}
	page_str += "... <a href='?page="+total_page+appendUrl+"'>"+total_page+"</a>";

	page_str = prev_block+page_str+next_block;
	
	System.out.println("리스트 갯수:"+list_tab_count);
	System.out.println("전체페이지수:"+total_page);
	System.out.println("페이지 문자열:"+page_str);
	
	
	list_tab = TabDao.getList(p_tvo,startIdx,ipp);
	for(int i=0; i < list_tab.size(); i++){
		TabVO tvo = new TabVO();
		tvo = list_tab.get(i);
		System.out.println(tvo.seq+"번째");
		parser.setVar("seq", tvo.seq);//1. parser의 parser변수<@@H[ccc]@@>을 원하는 값으로 치환
		parser.setVar("title", tvo.title);//1. parser의 parser변수<@@H[ccc]@@>을 원하는 값으로 치환
		parser.setVar("contents", tvo.contents);//1. parser의 parser변수<@@H[ccc]@@>을 원하는 값으로 치환
		parser.setBlock("list",true);// T 2.1 블럭등록 및 템플릿 변수 치환
	}
	parser.setVar("page_str", page_str);//1. parser의 parser변수<@@H[ccc]@@>을 원하는 값으로 치환

    parser.clearVars();//2-1. 남아있는 parser 변수를 없애줌.
	parser.clearBlock();//2-2. 남아있는 parser block를 없애줌.
	out.println(parser.getPage());//99. 치환작업을 완료한 parser의 내용을 string 반환
%>
