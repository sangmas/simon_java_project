<%--
 /*
 ***************************************************************************************
 * @project  : 연구문서관리 (2011) 
 * @source  : 공통헤더처리
 * @desc    :  공통 작성 부문
 * -----------------------------------------------------------------------------------
 * VER      DATE       AUTHOR    DESCRIPTION
 * ---   ----------  ----------  -----------------------------------------------------
 * 1.0   2011.11.20    김영돈            최초 프로그램 작성    
 * -----------------------------------------------------------------------------------
 * Copyright(c) 2010 maya ,  All rights reserved.
 ***************************************************************************************
 */ 
--%>
<jsp:useBean id="menuSession"  class="maya.menu.MainMenuBean" scope="session" />
<%
	request.setCharacterEncoding("utf-8");
//out.println("sss 검색값:"+request.getParameter("sv"));
if(request.getParameter("mn")!=null){
	menuSession.calcMenu(request.getParameter("mn"));  
}
//System.out.println("menumenu:"+request.getParameter("mn"));
//System.out.println("menumenu:"+menuSession.getMdFull());
//ep.member.AuthorityBean _authBean = new ep.member.AuthorityBean(session, request, response);
//String _serverRoot =pageContext.getServletContext().getRealPath("/");
String _serverRoot =request.getRealPath("/");

String _basePath = "/testWeb2"; //Web 호출 URL 기본경로	
String _serverImg_path = _serverRoot+"\\upfile\\facImage\\";
//String _serverImg_path = "/jeus/tmax/devsang/upfile/facImage/";
//============================================================================

  

//============================================================================
// 멀티파트에서 업로드 되는 임시폴더의 위치 지정 2012.01.03  By Hyochun
//============================================================================
//String _saveDir = "J:\\workspace\\testWeb2\\WebContent\\upfile\\test1";
String _saveDir = "/upfile/test1";
//String _saveDir = DocConst.DOC_TEMP_PATH;
System.out.println("경로 :"+_saveDir);

 
/* 
if(!_authBean.isLogin()){
	_authBean.messageOutClose("로그인 후에 사용가능합니다.");
}
*/
%> 