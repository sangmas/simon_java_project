<%--
/*
 ***********************************************************************************
 * @project  : 이벤트플레이스(2013) 
 * @source   : 이벤트 등록
 * @desc     : 
 *----------------------------------------------------------------------
 * VER      DATE       AUTHOR    DESCRIPTION
 * ---   ----------  ----------  ------------------------------------------
 * 1.0   2013.07.22    김영돈    최초 프로그램 작성       
 * ----------- ----------  -----------------------------------------------
 * Copyright(c) 2010 maya ,  All rights reserved.
 *************************************************************************
 */
--%>
<%@page import="maya.util.Util"%>
<%@page import="java.util.List" %> 
<%@page import="java.util.ArrayList" %>
<%@page import="slowHub.atmosphere.AtmosphereDao" %>
<%@page import="slowHub.atmosphere.TabAtmosphereVO" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="htmlParser.HMParser" %>
<%@ include file="../../comm/header.jsp" %>
<%
	//String node_seq = maya.util.Util.null2str(request.getParameter("node_seq"));
	String keyword = Util.requestParamter(request, "keyword");
	String orderkey = Util.requestParamter(request, "orderkey");
	String appendUrl = "";
	appendUrl += "&keyword="+keyword;
	appendUrl += "&orderkey="+orderkey;

	int curr_page=1;
	try {
		curr_page = Integer.parseInt(Util.requestParamter(request, "page"));
	}catch (Exception e){}
	//if(curr_page == null)
	//	curr_page = "1";
	
	int startIdx = 1;
	int ipp = 10;
	startIdx = (curr_page-1) * ipp+1;
	
	String serverRoot = pageContext.getServletContext().getRealPath("/");//0. 파일 상대경로 찾기
	HMParser parser = new HMParser(_serverRoot+"/ksh_ui_ver3/html/slow_ksh_main.html");//1. html 파일을 parser로 읽어들임
	//int len = parser.setFile(_serverRoot+"/test_device_ksh/html/device_view2.html");
    //parser.setVar("fMode", fMode);
      
    parser.regBlock("list");// T 2.1 블럭등록 및 템플릿 변수 치환
    
    List<TabAtmosphereVO> list_tab = new ArrayList<TabAtmosphereVO>();
    //list_tab = AtmosphereDao.getList();
    TabAtmosphereVO p_tvo = new TabAtmosphereVO(); //검색조건변수
    int list_tab_count = AtmosphereDao.getListCnt(p_tvo);
    int total_page = (list_tab_count/ipp)+1;
    String page_str = "";
    int bpp = 5;
    int block_start = (curr_page / bpp) * bpp + 1;
    int block_end = block_start + bpp -1;
    if(block_end>total_page)
    {
    	block_end = total_page;
    }
    page_str += "<a href='?page=1'>1</a>...";
    int prev_start = block_start - bpp;
    int next_start = block_start + bpp;
   
    String prev_block = "";    
    if(curr_page - bpp > 1)
    {
    	prev_block = "<a href='?page="+prev_start+appendUrl+"'><<<이전</a>";
    }    
    
    String next_block = "";    
    if(curr_page + bpp < total_page)
    {
   		next_block = "<a href='?page="+next_start+appendUrl+"'>다음>>></a>";	
    }
    
    for(int i=1;i<total_page;i++)
    {    	
    	if(curr_page == i)    	
    		page_str +="<B><font size='9'>"+i+"</font></B>";
    	else    	
    		page_str +=" <a href='?page="+i+appendUrl+"'>"+i+"</a>";
    }
   
    page_str += ". . . <a href='?page="+total_page+appendUrl+"'>"+total_page+"</a>";
    
    page_str = prev_block + page_str + next_block;
    
    System.out.println("list 개수 : " +list_tab_count);
    System.out.println("page 개수 : " +total_page);
    System.out.println("page 문자열 : " +page_str);
    
    list_tab = AtmosphereDao.getList(p_tvo,startIdx,ipp);  
    
    for(int i=0; i<list_tab.size(); i++)
    {
    	TabAtmosphereVO tvo = new TabAtmosphereVO();
    	tvo = list_tab.get(i);  
    	System.out.println(tvo.node_seq+"번째");
    	
    	parser.setVar("node_seq", tvo.node_seq); //1. parser의 parser변수 <@@H[ccc]@@>을 원하는 값으로 치환
    	parser.setVar("node_name", tvo.node_name); //1. parser의 parser변수 <@@H[ccc]@@>을 원하는 값으로 치환
    	parser.setVar("co2", tvo.co2); //1. parser의 parser변수 <@@H[ccc]@@>을 원하는 값으로 치환
    	parser.setVar("connect_dt", tvo.connect_dt); //1. parser의 parser변수 <@@H[ccc]@@>을 원하는 값으로 치환
    	parser.setBlock("list",true);// T 2.1 블럭등록 및 템플릿 변수 치환   
    }
    parser.setVar("page_str", page_str); 
    
    parser.clearVars(); //2-1. 남아있는 parser 변수를 없애줌
	parser.clearBlock(); //2-2. 남아있는 parser block를 없애줌 // 아직이해불가
	out.println(parser.getPage()); //99. parser의 내용을 string으로 반환
%>
