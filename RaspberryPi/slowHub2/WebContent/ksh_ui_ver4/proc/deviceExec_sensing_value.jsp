<%--
/*
 ***************************************************************************************
 * @project  : 이벤트플레이스(2013) 
 * @source   : 이벤트
 * @desc     : 
 * -----------------------------------------------------------------------------------
 * VER      DATE       AUTHOR    DESCRIPTION
 * ---   ----------  ----------  -----------------------------------------------------
 * 1.0   2013.07.20    김영돈            최초 프로그램 작성    
 * -----------------------------------------------------------------------------------
 * Copyright(c) 2009 maya ,  All rights reserved.
 ***************************************************************************************
 */
--%>
<%@page import="slowHub.sensing_value.sensing_valueDao"%>
<%@page import="slowHub.sensing_value.sensing_valueVO"%>
<%@page import="maya.util.MayaFileUp"%>
<%@page import="maya.util.Util"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.oreilly.servlet.MultipartRequest" %>
<%@page import="java.text.*"%>
<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.util.Date" %>
<%@ include file="../../comm/header.jsp" %>

<%
	request.setCharacterEncoding("UTF-8");
	java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	//String node_seq = maya.util.Util.null2str(request.getParameter("node_seq"));
	String sensor_seq = maya.util.Util.null2str(request.getParameter("sensor_seq"));
	String sensing_value = maya.util.Util.null2str(request.getParameter("sensing_value"));
	String sensing_dt = formatter.format(new java.util.Date());
	
	String pMode = maya.util.Util.null2str(request.getParameter("pMode"));
	

	 
	
	sensing_valueVO vo = new sensing_valueVO();
	vo.seq = 0;
	vo.sensor_seq = Integer.parseInt(sensor_seq);
	vo.sensing_value = Float.parseFloat(sensing_value);
	vo.sensing_dt = sensing_dt;

	//vo.seq = seq;
	//이 객체를 통째로 파라미터로 넘길 수 있다.
	
	System.out.println("모드 :"+pMode);  
	if("insert".equals(pMode)){
		//등록 수행...
		if(sensing_valueDao.insert(vo)>0){
%>
			<script>
				alert("등록 성공");
				//location.href="device_list.jsp";
				//history.back();
			</script>
		<%
			} else {
		%>
			<script>
				alert("등록 실패");
				history.back();
			</script>
		<%
			}
				
			}else if("modify".equals(pMode)){
			//수정 수행...
			if(sensing_valueDao.update(vo)>0){
		%>
				<script>
					alert("수정 성공");
					location.href="device_view.jsp?seq=<%=vo.seq%>";
					//history.back();
				</script>
			<%
				} else {
			%>
				<script>
					alert("수정 실패");
					history.back();
				</script>
			<%
				}	
				
				}else if("delete".equals(pMode)){
					//수정 수행...
					if(sensing_valueDao.delete(vo.seq)>0){
			%>
			<script>
				alert("삭제 성공");
				location.href="device_list.jsp";
				//history.back();
			</script>
		<%
		} else {
			%>
			<script>
				alert("삭제 실패");
				history.back();
			</script>
		<%
		}	
		
	} else {
		%> 
			<script>
				alert("잘못된 접근입니다.");
				//history.back();
			</script>
		<%
		return;
	}
	
	//권한체크
	
	/*
	if(true){
		//한글 체크
		System.out.println("한글파라메터 : "+da.event_name);
		//return;
	}
	//처리 분기 
	if("insert".equals(pMode)){//등록모드
		da.cre_id =  _authBean.sId; 
		EventDao dao = new EventDao();  
		result = dao.insert(da);
		if(result>0){
			_authBean.messageOutReplace("이벤트  등록 성공","./eventList.jsp");
		}else{
			_authBean.messageOutBack("이벤트 등록 실패");
		}
		
	} else if("modify".equals(pMode)){
		//기존파일 삭제
		if(null!=mfile.getFileParameter(file_param)){
			EventVO old_da = new EventVO();
			old_da = EventDao.getRow(da.event_seq);
			mfile.delFileReal(request.getRealPath("/")+old_da.thumbnail_image);
		}
		/*
		if(!_authBean.isAdmin()){
			_authBean.messageOutBack("관리자만 접근 가능합니다.");
			return;
		}*/
		/*
		out.println("이벤트 정보 수정중");
		result = EventDao.update(da); 
		if(result>0){
			//ActionBean.actionLog("이벤트 정보 수정", da.event_seq, _authBean.sId,request.getRemoteAddr());
			_authBean.messageOutReplace("이벤트 정보 수정 성공","./eventList.jsp");
		}else{
			_authBean.messageOutBack("이벤트 정보 수정 실패");
		}
	*/
%>
