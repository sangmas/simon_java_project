<%--
/*
 ***************************************************************************************
 * @project  : 이벤트플레이스(2013) 
 * @source   : 이벤트
 * @desc     : 
 * -----------------------------------------------------------------------------------
 * VER      DATE       AUTHOR    DESCRIPTION
 * ---   ----------  ----------  -----------------------------------------------------
 * 1.0   2013.07.20    김영돈            최초 프로그램 작성    
 * -----------------------------------------------------------------------------------
 * Copyright(c) 2009 maya ,  All rights reserved.
 ***************************************************************************************
 */
--%>
<%@page import="slowHub.testLjh.TabLjhDao"%>
<%@page import="slowHub.testLjh.TabLjhVO"%>
<%@ page import = "slowSerial.LJH.slowRxTx_Send"  %> 
<%@ page import = "slowSerial.LJH.slowRxTx_Receive"  %> 
<%@page import="maya.util.MayaFileUp"%>
<%@page import="maya.util.Util"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.oreilly.servlet.MultipartRequest" %>
<%@page import="java.text.*"%>
<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.util.Date" %>


<%
	request.setCharacterEncoding("UTF-8");
%>

<%

	request.setCharacterEncoding("euc-kr");
	//Date now = new Date();    Mon Apr 14 13:16:34 KST 2014  
	java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	String today = formatter.format(new java.util.Date()); 

	System.out.println("*******시리얼통신을 받기위해 기다립니다.*******");
	System.out.println("*******시리얼통신을 받기위해 기다립니다.*******");
	
	String Send_Num = request.getParameter("Send_Num");     
	System.out.println("Send_Num : "+Send_Num);
	String signal="";
	int signal_int=0;
	
		
		signal = "$60:1:4:"+Send_Num;
		signal_int=Integer.parseInt(Send_Num)+1;
		Send_Num=Integer.toString(signal_int);
	//String str_now = String.valueOf(now); 
	
	String signal_revise = signal.replaceAll(":","/");
	//String Psignal = signal_revise+"/"+str_now;
	String Psignal = signal_revise+"/"+today;
	
	System.out.println("시리얼리슨을 시작합니다!");
	System.out.println("신호 : "+signal);

	System.out.println("[slow_LJH] Testing Serial Communication!!");
	
	slowRxTx_Send crt = new slowRxTx_Send(); 
	crt.setSwitchCommand(Psignal);  
	Thread myth = new Thread(crt);
	myth.start();
	
	slowRxTx_Receive crt1 = new slowRxTx_Receive(); 
	Thread myth1 = new Thread(crt1);
	myth1.start();
	
	myth.join();
	myth1.join();
	
	System.out.println("[slow_LJH] Ending Serial Communication!!");

	
	System.out.println("*******시리얼통신을 종료합니다.*******");
	System.out.println("*******시리얼통신을 종료합니다.*******");
%>

			<script>
				//alert("통신성공");
				if(<%=Send_Num%><50){
					location.href = './Serial_Sender.jsp?Send_Num=<%=Send_Num%>';
				}
				else{
					location.href = location.href='../html/main_view.html';
				}
			</script>

			
			