<%--
/*
 ***********************************************************************************
 * @project  : 이벤트플레이스(2013) 
 * @source   : 이벤트 등록
 * @desc     : 
 *----------------------------------------------------------------------
 * VER      DATE       AUTHOR    DESCRIPTION
 * ---   ----------  ----------  ------------------------------------------
 * 1.0   2013.07.22    김영돈    최초 프로그램 작성       
 * ----------- ----------  -----------------------------------------------
 * Copyright(c) 2010 maya ,  All rights reserved.
 *************************************************************************
 */
--%> 
<%@page import="maya.util.Util"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="slowHub.testLjh.TabLjhDao"%>
<%@page import="slowHub.testLjh.TabLjhVO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="htmlParser.HMParser" %> 
<%@ include file="../../comm/header.jsp" %>
<%
	//외부 파라메터 받기
	//String seq = Util.requestParamter(request, "seq");
	String curr_page = Util.requestParamter(request, "page");
	int startIdx = 1;
	int ipp = 15;
	if(!"".equals(curr_page)){
		startIdx = (Integer.parseInt(curr_page)-1) * ipp+1;
	}
	
	String serverRoot = pageContext.getServletContext().getRealPath("/");//0.파일 상대경로 찾기
	HMParser parser = new HMParser(_serverRoot+"/test_ljh/html/tab_list.html");//1.html 파일을 parser 읽어 들임
	parser.regBlock("list");// T 2.1 블럭등록 및 템플릿 변수 치환
	//parser.regBlock("list_null");
	List<TabLjhVO> list_tab = new ArrayList<TabLjhVO>();
	//list_tab = TabDao.getList();
	TabLjhVO p_tvo = new TabLjhVO();
	list_tab = TabLjhDao.getList(p_tvo,startIdx,ipp);
	for(int i=0; i < list_tab.size(); i++){
		TabLjhVO tvo = new TabLjhVO();
		tvo = list_tab.get(i);
		System.out.println(tvo.seq+"번");
		parser.setVar("seq", tvo.seq);//1. parser의 parser변수<@@H[ccc]@@>을 원하는 값으로 치환
		parser.setVar("node_id", tvo.node_id);//1. parser의 parser변수<@@H[ccc]@@>을 원하는 값으로 치환
		parser.setVar("sensor_type", tvo.sensor_type);//1. parser의 parser변수<@@H[ccc]@@>을 원하는 값으로 치환
		parser.setVar("value", tvo.value);//1. parser의 parser변수<@@H[ccc]@@>을 원하는 값으로 치환
		parser.setVar("time", tvo.time);//1. parser의 parser변수<@@H[ccc]@@>을 원하는 값으로 치환
		parser.setBlock("list",true);// T 2.1 블럭등록 및 템플릿 변수 치환
	}

    parser.clearVars();//2-1. 남아있는 parser 변수를 없애줌.
	parser.clearBlock();//2-2. 남아있는 parser block를 없애줌.
	out.println(parser.getPage());//99. 치환작업을 완료한 parser의 내용을 string 반환
%>
