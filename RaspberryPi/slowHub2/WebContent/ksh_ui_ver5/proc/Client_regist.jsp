<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.2/jquery.mobile-1.4.2.min.css">
<script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="http://code.jquery.com/mobile/1.4.2/jquery.mobile-1.4.2.min.js"></script>
</head>
<body>

<script type="text/javascript">
function check() {
	if(document.SM_form.mem_id.value.length==0){
		alert("아이디를 입력해주세요");
			SM_form.mem_id.focus();
			return false;
	}	
	if(document.SM_form.mem_pwd.value.length==0){
		alert("패스워드를 입력해주세요");
		SM_form.mem_pwd.focus();
		return false;
	}
	if(document.SM_form.mem_pwd.value!=document.SM_form.mem_pwd2.value){
		alert("패스워드가 일치하지 않습니다.");
			SM_form.mem_pwd2.focus();
			return false;
	}	
	if(document.SM_form.mem_name.value.length==0){
		alert("성명을 입력해주세요");
		SM_form.mem_name.focus();
		return false;
	}
	if(document.SM_form.mem_hp.value.length==0){
		alert("핸드폰번호를 입력해주세요");
		SM_form.mem_hp.focus();
		return false;
	}
	if(document.SM_form.mem_mail.value.length==0){
		alert("메일주소를 입력해주세요");
		SM_form.mem_mail.focus();
		return false;
	}
	if(document.SM_form.mem_addr.value.length==0){
		alert("주소를 입력해주세요");
			SM_form.mem_addr.focus();
			return false;
	}	
	
	document.SM_form.submit();
	
}
</script>

<!-- main -->
<div data-role="page" id="page">
  <br>
  <br>



  <div data-role="main">
			<form name="SM_form" method="post" action="Client_regist_act.jsp">
		        	<center><table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" >
		        	<table width="500" >
					<tr> <td colspan = "2" height="50"> <B><font size="5" face="휴먼편지체"><center>  SLOW IoT 사용자등록  </center></font></B></td></tr>
		
					<tr>  
							<td width="150"><center><font face="맑은 고딕"><B> 아이디</B></font></center> </td>    
		     			   <td width="350"> <input type="text" name="mem_id" size = "60"> </td>
					</tr>
					<tr>  
							<td width="150"><center><font face="맑은 고딕"><B> 비밀번호 </B></font></center> </td>    
		     			   <td width="350"> <input type="password" name="mem_pwd" size = "60"> </td>
					</tr>
					<tr>  
							<td width="150"><center><font face="맑은 고딕"><B> 비밀번호 확인 </B></font></center> </td>    
		     			   <td width="350"> <input type="password" name="mem_pwd2" size = "60"> </td>
					</tr>
					<tr>  
							<td width="150"><center><font face="맑은 고딕"><B> 성명 </B></font></center> </td>    
		     			   <td width="350"> <input type="text" name="mem_name" size = "60"> </td>
					</tr>
					<tr>  
							<td width="150"><center><font face="맑은 고딕"><B> 핸드폰번호 </B></font></center> </td>    
		     			   <td width="350"> <input type="text" name="mem_hp" size = "60"> </td>
					</tr>		
					<tr>  
							<td width="150"><center><font face="맑은 고딕"><B> E-mail </B></font></center> </td>    
		     			   <td width="350"> <input type="text" name="mem_mail" size = "60"> </td>
					</tr>
					<tr>  
							<td width="150"><center><font face="맑은 고딕"><B> 주소 </B></font></center> </td>    
		     			   <td width="350"> <input type="text" name="mem_addr" size = "60"> </td>
					</tr>
		    		
		       		 <tr>
				            <td colspan="2" align="center">
				                <input type="button" value="가입하기" onClick="check();"  >
				                <input type="reset"  value="다시작성">
				            </td>
		       		 </tr>
		
					</table></center>
			</form>
			
  </div>
</div> 

</body>
</html>



