<%--
/*
 ***********************************************************************************
 * @project  : 이벤트플레이스(2013) 
 * @source   : 이벤트 등록
 * @desc     : 
 *----------------------------------------------------------------------
 * VER      DATE       AUTHOR    DESCRIPTION
 * ---   ----------  ----------  ------------------------------------------
 * 1.0   2013.07.22    김영돈    최초 프로그램 작성       
 * ----------- ----------  -----------------------------------------------
 * Copyright(c) 2010 maya ,  All rights reserved.
 *************************************************************************
 */
--%> 
<%@page import="slowHub.testDevice.TabDeviceDao"%>
<%@page import="slowHub.testDevice.TabDeviceVO"%>
 
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="htmlParser.HMParser" %> 
<%@ include file="../../comm/header.jsp" %>
<%
	//로그인여부 판단
	String serverRoot = pageContext.getServletContext().getRealPath("/");//weblogic 8.1적용
	//어디서 실행되는지 확인하기 위해서
	String fMode = maya.util.Util.null2str(request.getParameter("fMode"));
	String unit_seq = maya.util.Util.null2str(request.getParameter("unit_seq"));
	
	//out.println("외부 변수 : tab_seq = "+tab_seq);//화면에 출력
	
	TabDeviceVO tvo = new TabDeviceVO(); //VO value object : 값들을 담을수 있는 객체 //변수 17개들어있음
    		
	HMParser parser = new HMParser(); //HTML에 있는걸 jsp에 뿌려주는 역할
	int len = parser.setFile(_serverRoot+"/ksh_ui_ver2/html/slow_ksh_device_add.html");
	//등록폼을 가지고 수정폼도 사용할 것입니다.
	if("modify".equals(fMode)){
		tvo = TabDeviceDao.getRow(unit_seq);  //인스턴스 값을 쿼리문을 통해 불러와 값을 넣어줍니다.
		//out.println("<BR>number : "+tvo.unit_seq);
		//out.println("<BR>number : "+tvo.unit_name);
		//out.println("<BR>number : "+tvo.communication_type);
		if(tvo==null){
			out.println("해당 정보를 찾을 수 없습니다.");
			return;
		}  
		parser.setVar("fMode", "수정"); 
		parser.setVar("pMode", "modify");
		parser.setVar("seq_readonly", "readonly");	
	
	} else {
		parser.setVar("fMode", "등록");
		parser.setVar("pMode", "insert");
		parser.setVar("seq_readonly", "");	
		}
	
	parser.setVar("unit_seq", tvo.unit_seq);
    parser.setVar("unit_name", tvo.unit_name);
    parser.setVar("reg_dt", tvo.reg_dt);
    parser.setVar("connect_state", tvo.connect_state);
    parser.setVar("connect_dt", tvo.connect_dt);
    parser.setVar("unit_identifier", tvo.unit_identifier);
    parser.setVar("product_code", tvo.product_code);
    parser.setVar("compatible_device_code", tvo.compatible_device_code);
    parser.setVar("manufacture_company_name", tvo.manufacture_company_name);
    parser.setVar("product_name", tvo.product_name);
    parser.setVar("communication_type", tvo.communication_type);
    parser.setVar("cert_key", tvo.cert_key);
    parser.setVar("external_open_flg", tvo.external_open_flg);
    parser.setVar("install_postion_coord", tvo.install_postion_coord);
    parser.setVar("install_inout_type", tvo.install_inout_type);
    parser.setVar("id", tvo.id);
    parser.setVar("external_cert_key", tvo.external_cert_key);   
   
   

    parser.clearVars();
	parser.clearBlock();
	String resultStr = parser.getPage();
	out.println(resultStr);
%>
