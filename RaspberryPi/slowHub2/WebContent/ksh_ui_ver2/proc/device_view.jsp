<%--
/*
 ***********************************************************************************
 * @project  : 이벤트플레이스(2013) 
 * @source   : 이벤트 등록
 * @desc     : 
 *----------------------------------------------------------------------
 * VER      DATE       AUTHOR    DESCRIPTION
 * ---   ----------  ----------  ------------------------------------------
 * 1.0   2013.07.22    김영돈    최초 프로그램 작성       
 * ----------- ----------  -----------------------------------------------
 * Copyright(c) 2010 maya ,  All rights reserved.
 *************************************************************************
 */
--%> 
<%@page import="slowHub.testDevice.TabDeviceDao"%>
<%@page import="slowHub.testDevice.TabDeviceVO"%>
<%@page import="java.util.List"%> 
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="htmlParser.HMParser" %>
<%@page import="maya.util.*"%> 
<%@ include file="../../comm/header.jsp" %>
<%	
	
	//String unit_seq = maya.util.Util.null2str(request.getParameter("unit_seq"));
	String unit_seq = Util.requestParamter(request, "seq");//unit_seq를 seq라고 인식하는 코드
	
	
	TabDeviceVO tvo = new TabDeviceVO(); //VO value object : 값들을 담을수 있는 객체 //변수 17개들어있음
	tvo = TabDeviceDao.getRow(unit_seq);  //인스턴스 값을 쿼리문을 통해 불러와 값을 넣어줍니다.
			
	if(tvo==null){
		out.println("해당 정보를 찾을 수 없습니다.");
		return;
	}
	
	
	String serverRoot = pageContext.getServletContext().getRealPath("/");//0. 파일 상대경로 찾기
	HMParser parser = new HMParser(_serverRoot+"/ksh_ui_ver2/html/slow_ksh_device_state.html");//1. html 파일을 parser로 읽어들임
	//int len = parser.setFile(_serverRoot+"/test_device_ksh/html/device_view2.html");
		
    parser.setVar("unit_seq", tvo.unit_seq); //1. parser의 parser변수 <@@H[ccc]@@>을 원하는 값으로 치환
    parser.setVar("unit_name", tvo.unit_name);
    parser.setVar("reg_dt", tvo.reg_dt);
    parser.setVar("connect_state", tvo.connect_state);
    parser.setVar("connect_dt", tvo.connect_dt);
    parser.setVar("unit_identifier", tvo.unit_identifier);
    parser.setVar("product_code", tvo.product_code);
    parser.setVar("compatible_device_code", tvo.compatible_device_code);
    parser.setVar("manufacture_company_name", tvo.manufacture_company_name);
    parser.setVar("product_name", tvo.product_name);
    parser.setVar("communication_type", tvo.communication_type);
    parser.setVar("cert_key", tvo.cert_key);
    parser.setVar("external_open_flg", tvo.external_open_flg);
    parser.setVar("install_postion_coord", tvo.install_postion_coord);
    parser.setVar("install_inout_type", tvo.install_inout_type);
    parser.setVar("id", tvo.id);
    parser.setVar("external_cert_key", tvo.external_cert_key);   
         
	//parser.setVar("fMode", fMode);
    
	parser.clearVars(); //2-1. 남아있는 parser 변수를 없애줌
	parser.clearBlock(); //2-2. 남아있는 parser block를 없애줌 // 아직이해불가
	out.println(parser.getPage()); //99. parser의 내용을 string으로 반환
%>
