package maya.menu;

import javax.servlet.http.HttpServletRequest;

/***************************************************************************************
 * <pre>
 * <BR\> 프로젝트 : 연구문서관리(2011) 
 * <BR\> 소스 : 메뉴세션처리 Bean
 * <BR\> -----------------------------------------------------------------------------------
 * <BR\> VER      DATE       AUTHOR    DESCRIPTION
 * <BR\> ---   ----------  ----------  -----------------------------------------------------
 * <BR\> 1.0   2010.08.18    김영돈           최초 프로그램 작성    
 * <BR\> -----------------------------------------------------------------------------------
 * <BR\> Copyright(c) 2010 ADDLAB ,  All rights reserved.
 ***************************************************************************************
 * </pre>
**/
public class MainMenuBean {  

	private boolean flag;//메뉴세션이 처음 사용되는지 여부 
	private String md0 = "0";
	private String md1 = "0";
	private String md2 = "0"; 
	private String md3 = "0";
	private String md4 = "0";
	private String md5 = "0";
	
	public String menu_name = "";
	
	/**
	 * 메뉴세션 전체 가져오기
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public String getMdFull() {
		String md = "";
		md = getMd0()+getMd1()+getMd2()+getMd3()+getMd4()+getMd5();
		System.out.println("menumenu3:"+md);
		md = md.replaceAll("0","");
		return md;
	}

	/**
	 * 메뉴세션 1번째 가져오기
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public String getMd0() {
		if(md0==null||"".equals(md0)) md0="0";
		return md0;
	}

	/**
	 * 메뉴세션 2번째 가져오기
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public String getMd1() {
		if(md1==null||"".equals(md1)) md1="0";
		return md1;
	}

	/**
	 * 메뉴세션 3번째 가져오기
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public String getMd2() {
		if(md2==null||"".equals(md2)) md2="0";
		return md2;
	}

	/**
	 * 메뉴세션 4번째 가져오기
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public String getMd3() {
		if(md3==null||"".equals(md3)) md3="0";
		return md3;
	}

	/**
	 * 메뉴세션 5번째 가져오기
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public String getMd4() {
		if(md4==null||"".equals(md4)) md4="0";
		return md4;
	}

	/**
	 * 메뉴세션 6번째 가져오기
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public String getMd5() {
		if(md5==null||"".equals(md5)) md5="0";
		return md5;
	}

	/**
	 * 메뉴세션 처음 사용여부 플레그 조회
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public boolean getFlag() {
		return flag;
	}

	/**
	 * 메뉴세션 1번째 저장
	 * @param string
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public void setMd0(String string) {
		md0 = string;
	    flag = true;
	}

	/**
	 * 메뉴세션 2번째 저장
	 * @param string
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public void setMd1(String string) {
		md1 = string;
		flag = true;
	}
	
	/**
	 * 메뉴세션 3번째 저장
	 * @param string
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public void setMd2(String string) {
		md2 = string;
		flag = true;
	}

	/**
	 * 메뉴세션 4번째 저장
	 * @param string
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public void setMd3(String string) {
		md3 = string;
		flag = true;
	}
	
	/**
	 * 메뉴세션 5번째 저장
	 * @param string
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public void setMd4(String string) {
		md4 = string;
		flag = true;
	}
	
	/**
	 * 메뉴세션 6번째 저장
	 * @param string
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public void setMd5(String string) {
		md5 = string;
		flag = true;
	}

	
	/**
	 * 메뉴를 일괄지정
	 * @param string0
	 * @param string1
	 * @param string2
	 * @param string3
	 * @param string4
	 * @param string5
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public void setMd(String string0,String string1,String string2,String string3,String string4, String string5 ) {
		//메뉴 Default 세팅 추가 (2006-01-17)
		if(string0==null || "".equals(string0)){
			string0 = "0"; 
			string1 = "0"; 
			string2 = "0"; 
			string3 = "0"; 
			string4 = "0"; 
			string5 = "0"; 
		}
		if(string1==null || "".equals(string1)){
			string1 = "0"; 
			string2 = "0"; 
			string3 = "0"; 
			string4 = "0"; 
			string5 = "0"; 
		}
		if(string2==null || "".equals(string2)){
			string2 = "0"; 
			string3 = "0"; 
			string4 = "0"; 
			string5 = "0"; 
		}
		if(string3==null || "".equals(string3)){
			string3 = "0"; 
			string4 = "0"; 
			string5 = "0"; 
		}
		if(string4==null || "".equals(string4)){
			string4 = "0"; 
			string5 = "0"; 
		}
		if(string5==null || "".equals(string5)){
			string5 = "0"; 
		}
		
		md0 = string0;
		md1 = string1;
		md2 = string2;
		md3 = string3;
		md4 = string4;
		md5 = string5;
		System.out.println("menumenu2:"+md0+"[1]"+md1+"[2]"+md2+"[3]"+md3+"[4]"+md4+"[5]"+md5);
		flag = true;
	}
	
	/**
	 * request로 주어진 각 메뉴 Depth를 계산( 하위 Depth가 주어지지않았을경우 초기 셋팅 ) 
	 * @param request
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public void calcMenu(HttpServletRequest request){
		String depth0_code = request.getParameter("depth0_code");
		String depth1_code = request.getParameter("depth1_code");
		String depth2_code = request.getParameter("depth2_code");
		String depth3_code = request.getParameter("depth3_code");
		String depth4_code = request.getParameter("depth4_code");
		String depth5_code = request.getParameter("depth5_code");

		this.calcMenu(depth0_code, depth1_code, depth2_code, depth3_code, depth4_code, depth5_code );
	}
	
	/**
	 * 주어진메뉴를 세션으로 변경
	 * @param mn
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public void calcMenu(String mn){
		String[] menu =  {"","","","","","",""};
		for(int i=0; i<5; i++){
			try {
				menu[i+1] = mn.substring(i,i+1);
			} catch (java.lang.StringIndexOutOfBoundsException e){
				menu[i+1] = "";
			}
		}
		System.out.println("menumenu1:"+menu[1]+"[1]"+menu[2]+"[2]"+menu[3]+"[3]"+menu[4]+"[4]"+menu[5]+"[5]"+menu[6]);
		this.setMd(menu[1],menu[2],menu[3],menu[4],menu[5],menu[6]);
	}
	
	/**
	 * 주어진 메뉴를 세션으로 변경
	 * @param depth0_code
	 * @param depth1_code
	 * @param depth2_code
	 * @param depth3_code
	 * @param depth4_code
	 * @param depth5_code
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 */
	public void calcMenu(String depth0_code, String depth1_code, String depth2_code, String depth3_code, String depth4_code, String depth5_code ){
		if(depth0_code==null || "null".equals(depth0_code)){
			depth0_code = "0";
		}
		if(depth1_code==null||"null".equals(depth1_code)){
			depth1_code = this.getMd1(); 
		} else if(depth2_code==null||"null".equals(depth2_code)){
				depth2_code = "0";
				depth3_code = "0";
				depth4_code = "0";
				depth5_code = "0";
		} else if(depth3_code==null||"null".equals(depth3_code)){
			depth3_code = "0";
			depth4_code = "0";
			depth5_code = "0";
		} else if(depth4_code==null||"null".equals(depth4_code)){
			depth4_code = "0";
			depth5_code = "0";
		} else if(depth5_code==null||"null".equals(depth5_code)){
			depth5_code = "0";
		}
		if(depth2_code==null||"null".equals(depth2_code)){
			depth2_code = this.getMd2();
		}
		if(depth3_code==null||"null".equals(depth3_code))
			depth3_code = this.getMd3();
		if(depth4_code==null||"null".equals(depth4_code))
			depth4_code = this.getMd4();
		if(depth5_code==null||"null".equals(depth5_code))
			depth5_code = this.getMd5();
		
		
		this.setMd(depth0_code, depth1_code, depth2_code, depth3_code, depth4_code, depth5_code );
	}
	
	/**
	 * 메뉴 네비게이션 가져오기
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 */
	/*
	public String getMenuNaviString(){
		 MenuStatisticsBean mb = new MenuStatisticsBean();
		 return mb.getMenuNaviString(this.getMdFull());
	}
	*/
	/**
	 * 해당 메뉴의 메뉴명 가져오기 
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 */
	/*
	public String getMenuName(){
		 MenuStatisticsBean mb = new MenuStatisticsBean();
		MenuArticle ma  = new  MenuArticle();
		ma = (MenuArticle)mb.getMenu(this.getMdFull());
		 return ma.menu_name;
	}
	*/
	
}
