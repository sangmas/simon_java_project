package maya.util;
/***************************************************************************************
 * <pre>
 * <BR\> 프로젝트 : 연구문서관리(2011) 
 * <BR\> 소스 :   페이지 만들기
 * <BR\> -----------------------------------------------------------------------------------
 * <BR\> VER      DATE       AUTHOR    DESCRIPTION
 * <BR\> ---   ----------  ----------  -----------------------------------------------------
 * <BR\> 1.0   2010.08.18    김영돈           최초 프로그램 작성    
 * <BR\> -----------------------------------------------------------------------------------
 * <BR\> Copyright(c) 2010 ADDLAB ,  All rights reserved.
 ***************************************************************************************
 * </pre>
**/
public class MakePage{
	private int totPage;// 전체 페이지수 
	private int ipp;// 페이지당 표시 row 수
	private int idxGroup = 5;//페이지 번호 그룹 갯수
	private String url;//링크 URL
	private String add_tag;//추가태크
	private String pageStr;//화면표시페이지 문자열

	// 출력 스트링 설정(2006-01-05)
	/*
	private String pageStrPrev = "◀";
	private String  pageStrNext = "▶";
	private String  pageStrPageStart = "[";
	private String  pageStrPageEnd = "]";
	private String  pageStrCurrPageStart = "[<B>";
	private String  pageStCurrrPageEnd = "</B>]";
	*/
	/*
	private String  pageTagPrev = "<img src='/images/common/bullet_prev_01.gif' border=0>";
	private String  pageTagNext = "<img src='/images/common/main_img-more.gif' border=0>";
	private String  pageTagPageStart = "<B>";
	private String  pageTagPageEnd = "</B>";//?/
	private String  pageTagCurrPageStart = "<font color='#FF7200'><B>";
	private String  pageTagCurrPageEnd = "page</B></font>";//?/
	*/	
	/* hum용 페이징 */
	//private String _basePath = "/docmng";
	private String _basePath = "";
	private String  pageTagPrev ="<img src=\""+_basePath+"/images/sub/ic_prev01.gif\" alt=\"이전 10페이지\" border=0/>&nbsp;&nbsp;&nbsp;";
	private String  pageTagNext = "&nbsp;&nbsp;&nbsp;<img src=\""+_basePath+"/images/sub/ic_next01.gif\" alt=\"다음 10페이지\"  border=0 />";
	private String  pageTagPageStart ="<span class=\"paging_num\">";
	private String  pageTagPageEnd = "</span>";
	private String  pageTagCurrPageStart = " <B><font color=''>";
	private String  pageTagCurrPageEnd = "</font></B> ";
	public MakePage(int postCount, int ipp){
		int vmod = postCount % ipp;
		this.totPage = (vmod == 0) ? postCount/ipp : (postCount/ipp)+1;
		this.ipp = ipp;
		
	}

	/**
	 * 전체페이지 조회
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public int getTotPage() {
		return totPage;
	}
	/**
	 * 페이지 문자열 조회
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 */
	public String getPageStr() {
		return pageStr;
	}

	/**
	 * 페이지 표시 문자열 만들기 
	 * @param curpage 현재 페이지
	 * @param url 링크 url
	 * @param add_tag 파라메터 url
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 */
	public void makePageIndex(int curpage, String url, String add_tag) {
		int totpage = this.totPage;
		int idxcur; // 현재 페이지그룹
		int idxtot; //전체 페이지그룹 수
		int idxpage;
		int tmp;
		
		String outStr = "";
		String outStr2 = "";
		String outStr3 = "";
		
		if (add_tag == null){
			add_tag = "";
		}
		idxcur = (curpage - 1) / idxGroup + 1;
		idxtot = (totpage - 1) / idxGroup + 1;
		
		for(int i =1; i <= idxGroup ; i++){ //한 그룹당 페이지 수 
			idxpage = (idxcur -1) * idxGroup + i;
			if(idxpage <= totpage) {
				if(idxpage == curpage){
					outStr = outStr + " <a href=# class='rd'> "+pageTagCurrPageStart+ idxpage + pageTagCurrPageEnd+"</a>&nbsp;";
				} else {
					outStr = outStr + " <A class='rd' HREF='"+url+"?page="+idxpage + add_tag +"'>"+pageTagPageStart+idxpage+pageTagPageEnd+"</A>&nbsp;";
				}
			}
				if(i==1){
					tmp = idxpage -1;
					outStr2 = " <A class='rd' HREF='"+url+"?page="+tmp+add_tag + "'>"+pageTagPrev+"</A>";
					outStr3 = " <B><A class='rd' HREF='"+url+"?page=1"+add_tag + "'>1</A> ...</B>";
				
					if(idxcur ==1){
						outStr = outStr;
					} else {
						//outStr = outStr3 + outStr2 + outStr;
						outStr = outStr2 + outStr3 + outStr;
					}
					
				} else if (i==idxGroup) {
					tmp = idxpage +1;
					outStr2 = " <A class='rd' HREF='"+url+"?page="+tmp+add_tag + "'>"+pageTagNext+"</A>";
					outStr3 = " <B>... <A class='rd' HREF='"+url+"?page="+totpage+add_tag + "'>"+totpage+"</A></B>";

					if(idxcur == idxtot) {
						outStr = outStr;
					} else {
						//outStr = outStr + outStr2 + outStr3;
						outStr = outStr + outStr3 + outStr2;
					}
				}
				
		}
		this.pageStr = outStr;
	}
} 
