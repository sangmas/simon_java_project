package maya.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
/*
import com.tobesoft.xplatform.tx.*;
import com.tobesoft.xplatform.data.*;
import com.tobesoft.xplatform.data.datatype.*;
*/

/***************************************************************************************
 * <pre>
 * <BR\> 프로젝트 : 연구문서관리(2011) 
 * <BR\>  소스 : 공통 Util 기능 모음입니다.
 * <BR\> -----------------------------------------------------------------------------------
 * <BR\> VER      DATE       AUTHOR    DESCRIPTION
 * <BR\> ---   ----------  ----------  -----------------------------------------------------
 * <BR\> 1.0   2010.08.18    김영돈           최초 프로그램 작성    
 * <BR\> -----------------------------------------------------------------------------------
 * <BR\> Copyright(c) 2010 ADDLAB ,  All rights reserved.
 ***************************************************************************************
 * </pre>
**/

public class  Util
{
	
	/**
	 * 단일 row ResultSet 가져오기 
	 * @param dsName
	 * @param rs
	 * @return
	 * @throws SQLException
	 */ 
	/*
	public static DataSet getDataSetByRS(String dsName,  ResultSet rs) throws SQLException {
		
		return getDataSetByRS(dsName, rs, false);
	}
*/
	/*
	ResultSet을 이용하여 miplatform DataSet 만들기 
	*/
	/*
	public static DataSet getDataSetByRS(String dsName,  ResultSet rs, boolean isMultiRows) throws SQLException {
		
		/****miPlaform : DataSet 생성 **********/
	/*
		DataSet ds = new DataSet(dsName);
		//resultSet Meta를 이용하여 DataSet 생성
		ResultSetMetaData rsMetaData = rs.getMetaData();
		int numberOfColums = rsMetaData.getColumnCount();
		//System.out.println("컬럼수 : "+numberOfColums);
		for(int i=1; i <= numberOfColums; i++ ){
			ds.addColumn(rsMetaData.getColumnName(i),DataTypes.STRING,256);
		}
		//결과 처리
		if(isMultiRows){
			while(rs.next())
			{
				int row = ds.newRow();
				//ds.set(row,"SEQNUM",rs.getString("SEQNUM"));
				for(int i=1; i <= numberOfColums; i++ ){
					ds.set(row,rsMetaData.getColumnName(i),rs.getString(i));
				}
			}
		} else {
			if(rs.next())
			{
				int row = ds.newRow();
				//ds.set(row,"SEQNUM",rs.getString("SEQNUM"));
				for(int i=1; i <= numberOfColums; i++ ){
					ds.set(row,rsMetaData.getColumnName(i),rs.getString(i));
				}
			}
			
		}
		return ds;
	}
	*/
	/**
	 * 주어진 컬럼값이 null, 공백이 아니면 update 컬럼 Query를 생성
	 * @param colName
	 * @param colValue
	 * @return
	 */
	/*
	public static String makeUpdateColumn(String colName, String colValue){
		String colQuery = "";
		if(!"".equals(Util.null2str(colValue))){
			colQuery += ",  "+colName+" = '"+Util.null2str(colValue)+"' ";
		}
		return colQuery;
	}	
	
	/*자바스크립트용 날짜생성기
	 * 
	 */
	public static String makeJsDateString(String date_time){
		String result = "";
		try {
			result += date_time.substring(0,4);
			//start_dt_str += ","+ da.start_dt.substring(5,7);//월을 -1 해줌.. javascript 문법
			result += ","+(Integer.parseInt(date_time.substring(5,7))-1);
			result += ","+ date_time.substring(8,10);
			result += ","+ date_time.substring(11,13);
			result += ","+ date_time.substring(14,16);
			result += ","+ date_time.substring(17,19);
			result += ",0 ";
		} catch(Exception e){
			e.printStackTrace();
		}
		return result;
		
	}
	
	
	/**
	 * 문자열 EUC -> Uni코드로 변경
	 * @param s
	 * @return
	 */
	public static String EucToUni(String s){
		String result = "";
		try {
			result = new String(s.getBytes("8859_1"),"EUC-KR");
		} catch (Exception e){
			System.out.println(e);
		}
		return result;
	}
	/**
 	 * 문자열을 16진수 유니코드로 변경
	 * @param  
	 * @return 
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public static String StrtoUni(String str)	{
		String uni = "" ;
		for ( int i = 0 ; i < str.length() ; i++)
		{
			char chr = str.charAt(i) ;
			String hex = Integer.toHexString(chr) ;
			uni += "\\u"+hex ;
		}
		return uni ;
	}

	 /**
	* 16진수 유니코드를 문자열로 변경
	* @param  
	* @return 
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	*/
	public static String UnitoStr(String uni)	{
		String str = "" ;
		java.util.StringTokenizer str1 = new java.util.StringTokenizer(uni,"\\u") ;
		while(str1.hasMoreTokens())
		{
			String str2 = str1.nextToken() ;
			int i = Integer.parseInt(str2,16) ;
			str += (char)i ;
		}
		return str ;
	}
	
	 /**
	* 파일명의 확장자를 추출하여 리턴
	* @param  파일명
	* @return 확장자
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	*/
	public static String getFileExt(String file){
		String fileExt = file.substring(file.lastIndexOf(".")+1, file.length()) ;
		return fileExt;
	}
	
	 /**
	* 문자열의 문자를 치환한다.
	* @param  치환대상 문자열
	* @param  치환할 문자열
	* @return  치환결과
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	*/
	public static String rplc(String str, String n1, String n2) {
		int itmp = 0;
		if (str==null) return "";
		
		String tmp = str;
		StringBuffer sb = new StringBuffer();
		sb.append("");
		while (tmp.indexOf(n1)>-1) {
			itmp = tmp.indexOf(n1);
			sb.append(tmp.substring(0,itmp));
			sb.append(n2);
			tmp = tmp.substring(itmp+n1.length());
		}
		sb.append(tmp);
		return sb.toString();
	}

	 /**
	* 입력된 문자코드를 8859_1 -> ksc5601 로 변환
	* @param  문자열
	* @return  치환결과
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	*/
	public static String enKor(String s)
	{
		String s1 = null;
		try{
			s1 = new String(s.getBytes("8859_1"),"ksc5601");
		}catch(Exception e){}
		return s1;
	}

	 /**
	* 입력된 문자코드를 8859_1 -> ksc5601 로 변환
	* @param  문자열
	* @return  치환결과
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	*/
	public static String deKor(String s)
	{
		String s1 = null;
		try{
			s1 = new String(s.getBytes("ksc5601"),"8859_1");
		}catch(Exception e){}
		return s1;
	}


	 /**
	* 입력된 문자코드를 Cp1252 -> EUC_KR 로 변환
	* @param  문자열
	* @return  치환결과
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	*/
	public static String toHan(String s)
	{
		String s1 = null;
		try{
			s1 = new String(s.getBytes("Cp1252"),"EUC_KR");
		}catch(Exception e){}
		return s1;
	}

	 /**
	* 입력된 문자코드를 Cp1252 -> EUC_KR 로 변환
	* @param  문자열
	* @return  치환결과
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	*/
	public static String toEng(String s)
	{
		String s1 = null;
		try{
			s1 = new String(s.getBytes("EUC_KR"),"Cp1252");
		}catch(Exception e){}
		return s1;
	}
	
	 /**
	* 입력된 문자열이 null 이면 ""으로 처리
	* @param  문자열
	* @return  치환결과
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	*/
	public static String null2str(String s)
	{
		if (s==null || s.equalsIgnoreCase("null"))
			s = "";
		return s;
	}
	public static String null2zero(String s)
	{
		if (s==null || s.equalsIgnoreCase("null") ){
			s = "0";
		} else if("".equals(s)){
			s = "0";
			
		}
		return s;
	}

	/**
	 * HttpServletRequest으로 파라메터명에 해당하는 값 구함. null 치환 기능 포함
	 * @param req
	 * @param param
	 * @return
	 */
	public static String requestParamter(HttpServletRequest request, String param){
		String result = "";
		result = null2str(request.getParameter(param));
		return result;
	}
	
	 /**
	* xss(크로스 사이트 스크립트 문자 치환
	* @param  입력문자열
	* @return  치환결과
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	*/
	public static String xssReplace(String s)
	{
		if (s==null || s.equalsIgnoreCase("null")){
			s = "";
		} else {
			s = maya.util.Util.rplc(s, "<","&lt;");
			s = maya.util.Util.rplc(s, ">","&gt;");
			s = maya.util.Util.rplc(s, "=","&;");
			s = maya.util.Util.rplc(s, "!","&;");
			s = maya.util.Util.rplc(s, "&","&;");
			s = maya.util.Util.rplc(s, "'","&;");
			s = maya.util.Util.rplc(s, "(","&;");
			s = maya.util.Util.rplc(s, ")","&;");
		
		}
		return s;
	}
	
	 /**
	*  xss(크로스 사이트 스크립트 문자 제거
	* @param  입력문자열
	* @return  치환결과
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	*/
	public static String xssRemove(String s)
	{
		if (s==null || s.equalsIgnoreCase("null")){
			s = "";
		} else {
			s = maya.util.Util.rplc(s, "<","");
			s = maya.util.Util.rplc(s, ">","");
			s = maya.util.Util.rplc(s, "\"","");
			s = maya.util.Util.rplc(s, "+","");
			s = maya.util.Util.rplc(s, "-","");
			s = maya.util.Util.rplc(s, "&","");
			s = maya.util.Util.rplc(s, "(","");
			s = maya.util.Util.rplc(s, ")","");
		}
		return s;
	}

	
	 /**
	*  글자 잘라오기 (문자열, 시작인덱스, 글자수)
	* @param  입력문자열
	* @return  치환결과
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	*/
	public static String subString(String s, int i, int j)
	{
		String resultStr = s;
		try {
			if(s==null){
				return "";
			} else if(s.getBytes().length > i){
				if(s.getBytes().length > (i+j)){
					resultStr = s.substring(i,j);
				} else {
					resultStr = s.substring(i);
				}
			}
		} catch(StringIndexOutOfBoundsException e) {
						
		}
		return resultStr;
	}

	private String textArray[];
	private int textCount;

	/**
	* 문자열을 받아 사이즈대로 잘라낸 문자열의 배열을 받는다.
	* @param  문자열 배열
	* @return  없음, 내부변수 textArray[] 에 저장
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	*/

	public void setTextArray(String[] arr) {
    textArray = new String[textCount];
    textArray = arr;
  }

	/**
   * 문자열을 받아 사이즈대로 잘라낸 문자열의 배열에 값을 반환
	* @param  반환받고자 하는 문자열의 index 값
	* @return index에 해당하는 배열의 문자열을 반환
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	*/
	  public String getTextArray(int i) {
	    String val=textArray[i];
	    return val;
	  }

	/**
	* 문자열을 받아 사이즈대로 잘라낸 문자열의 개수를 저장
	* @param  문자열의 개수
	* @return index에 해당하는 배열의 문자열을 반환
	*/
	public void setTextCount(int count) {
		textCount = count;
	}

	/**
   * 문자열을 받아 사이즈대로 잘라낸 문자열의 개수를 반환
   * @param  문자열의 개수
   * @return 문자열의 개수를 반환
   */
  public int getTextCount() {
	  return textCount;
  }

	/**
   * String 타입을 int 타입으로 변환
   * @param val int 형으로 변환하고자 하는 문자열
   * @return int 문자열을 int형으로 변환한 값
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
   */

  public static int toInt(String val) {
	  if(val==null || "".equals(val))return 0;
	  
	  return Integer.parseInt(val) ;  
  }

  
  /**
   * Object 타입을 int 타입으로 변환
   *
   * @param obj int 형으로 변환하고자 하는 문자열
   * @return int Object를 int형으로 변환한 값
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
   */
  public static int toInt(Object obj) { return Integer.parseInt(String.valueOf(obj)) ; }


  
  /**
   * int 타입을 String 타입으로 변환
   *
   * @param val String 형으로 변환하고자 하는 int형
   * @return String int형을 문자열로 변환한 값
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
   */
  public static String toString(int val) { return String.valueOf(val) ; }


  /**
   * Object 타입을 String 타입으로 변환
   *
   * @param obj String 형으로 변환하고자 하는 Object
   * @return String Object를 문자열로 변환한 값
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
   */
  public static String toString(Object obj) { return String.valueOf(obj) ; }

  
  /**
   * 파라미터로 받은 Object 타입이 null인지 아닌지 체크
   *
   * @param obj 체크하고자 하는 Object
   * @return Object가 null이면 true, null이 아니면 false
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
   */
  public static boolean isNull(Object obj) {
      if (obj != null) return false ;
      else return true ;
  }

  /**
   * 파라미터로 받은 String에 데이터가 있는지 없는지 체크
   *
   * @param str 체크하고자 하는 String
   * @return 데이터가 있으면 false, 없으면 true
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
   */
  public static boolean isEmpty (String str) {
      if (str.trim().length() <= 0) return true ;
      else return false ;
  }

  /**
   * 문자열 중에서 특정한 문자를 치환
   *
   * @param str 원 문자열
   * @param n1 치환하고자 하는 문자
   * @param n2 대체 문자
   * @return 문자를 치환한 문자열
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
   */
  public static String replace(String str, String n1, String n2) {
	  int yes = 0;
	  if (str == null) return "";

	  StringTokenizer st = new StringTokenizer(str,n1);
	  StringBuffer sb = new StringBuffer();

	  while (st.hasMoreTokens()) {
		sb.append(st.nextElement());
		sb.append(n2);
		yes = 1;
	  }
	  return sb.substring(0,sb.length()-n2.length()*yes).toString();
	}

	/**
	 * \n 을 <BR> 테그로 변환
	 *
	 * @param str 원 문자열
	 * @return <BR>문자를 치환한 문자열
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 */
	public static String nl2br(String str) {
		return replace(str, "\n","<BR>");
	  }

   /**
   * 문자열을 받아 한글로 디코딩
   *
   * @param s 디코딩할 문자열
   * @return 디코딩한 문자열을 리턴한다.
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
   */
    public static String con(String s)
    {
        String s1 = null;
        try
        {
            s1 = new String(s.getBytes("8859_1"), "ksc5601");
        }
        catch(Exception _ex) { }
        return s1;
    }

   /**
   * 문자열을 받아 크기만큼 잘라내어 리턴한다.
   *
   * @param trail 마지막에 붙일 문자열
   * @return 문자열을 자른 문자열
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
   */
    public static String cropByte(String str, int i, String trail) {
      if (str==null) return "";
      String tmp = str;
      int slen = 0, blen = 0;
      char c;
      try {
          if(tmp.getBytes().length>i) {
              while (blen+1 < i) {
                c = tmp.charAt(slen);
                blen++;
                slen++;
                if ( c > 127 ) blen++; //2-byte character..
              }
              tmp=tmp.substring(0,slen)+trail;
            }
    	  
      } catch (Exception e) {
    	  return str;
      }
      return tmp;
    }

   /**
   * 문자열을 받아 길이에 맞게 잘라낸 문자열을 배열로 리턴한다.
   *
   * @str 잘라낼 문자열
   * @len 잘라낼 길이
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
   */
    public void cropBetweenByte(String str, int string_len) {
      String imsi01[] = new String[1];
      String imsi02[] = new String[1];
      String tmp = str;                          // 전달받은 문자열
      int str_len = string_len;
      int total_len=tmp.getBytes().length;       // 문자열을 byte 단위의 길이
      int slen = 0, blen = 0, start_num = 0;     // slen = 문자열을 자를 마지막 index
                                                 // blen = 문자열중 잘라진 문자열의 길이
                                                 // 문자열을 자를 시작 index

      int arr_count=0;                           // 문자열이 잘라진 개수
      char c;
      imsi02[0]="";

      while (total_len > blen) {
        start_num=slen;

        if(total_len > string_len) {
          str_len = blen+string_len;
          while (blen < str_len) {
            if (total_len <= blen) break;
            c = tmp.charAt(slen);
            blen++;
            slen++;
            if ( c > 127 ) blen++; //2-byte character..
          }

          if (total_len > blen) imsi02[arr_count] = tmp.substring(start_num,slen);
          else imsi02[arr_count] = tmp.substring(start_num);
          arr_count++;

          imsi01 = new String[arr_count+1];
          for (int i=0;i < imsi02.length ;i++ ) imsi01[i] = imsi02[i];
          imsi02 = new String[arr_count+1];
          for (int i=0;i < imsi01.length ;i++ ) imsi02[i] = imsi01[i];
        } else {
          imsi01[arr_count] = tmp;
          blen += string_len;
          arr_count++;
        }
      }
      setTextCount(arr_count);
      setTextArray(imsi01);
   }

   /**
   * 빈 문자열을 HTML 태그의 &nbsp로 변환
   *
   * @param n2 대체 문자
   * @return 문자를 치환한 문자열
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
   */
  public static String replaceBlankStrToNBSP(String str) {
      if (str == "" || str.equals("")) return "&nbsp;" ;
      return str ;
    }

    /**
   * 한글을 파라미터로 넘길경우에 사용
   *
   * @param 문자열
   * @return 치환된 문자열
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
   */
  public static String ToUrlEncode(String url_str)
  {
             String encode_str = "";
             if (url_str != null)
                             encode_str = URLEncoder.encode(url_str);
             return encode_str;
  }

  /**
   * 한글을 파라미터로 넘길경우에 사용
   *
   * @param 문자열
   * @return 치환된 문자열
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
   */

  public static String ToUrlDecode(String url_str)
  {
             String decode_str = "";

             try {
              if (url_str != null)
                              decode_str = URLDecoder.decode(url_str);
             }
             catch (Exception e){}

             return decode_str;
  }

  /**
   * 지정된 파일의 위치를 받아옴
   *
   * @param 파일명
   * @return 파일위치
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
   */
  public static String getFilePath(String file) {
      return file.substring(0, file.lastIndexOf("/")+1) ;
  }

  /**
   *지정된 파일(풀패스)의 파일명만을 추출
   *
   * @param 파일패스
   * @return 파일명
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
   */
  public static String getFileName(String file) {
      return file.substring(file.lastIndexOf("/")+1, file.length()) ;
  }

  static final String NBSP = "&nbsp;" ;
  static final String DASH = "<CENTER>-</CENTER>";
  static final String EMPTY = "" ;
  static final String ZERO = "0" ;
  static final String ONE = "1" ;

	/**
	 * 새 파일이름 만들기 
	 * @param dir
	 * @param fileName
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 */
	public static String getNewFileName(String dir, String fileName)
	{
		
		String newFileName = fileName;
		java.util.Date date = new java.util.Date(); 
		java.sql.Timestamp t_cre_dt = new java.sql.Timestamp(date.getTime());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		//fileName = t_cre_dt.toString();
		newFileName = sdf.format(t_cre_dt)+"."+maya.util.Util.getFileExt(fileName);

		String newPathName = dir+"/"+newFileName;
		while(!isNewPathName(newPathName)){
			//logger.debug("oldFile : "+newFileName);
			newFileName = "0"+newFileName;
			newPathName = dir+"/"+newFileName;
			//logger.debug("newFile : "+newFileName);
		}
		return newFileName;
	}  

	/**
	 * 파일 존재 여부 체크 
	 * @param filePathName
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 */
	public static boolean isNewPathName(String filePathName)
	{
		File checkFile = new File(filePathName);
		if(checkFile.exists()) {
			return false;
		} else {
			return true;
		}
	}	

	/**
	 * 파일저장 처리 
	 * @param file
	 * @param filePath
	 * @param filename
	 * @return
	 * @throws Exception
	 */
	public static boolean saveFile(byte[] file, String filePath, String filename) throws Exception {
		boolean result=false;
		//String filename;
		//Variant aa;
		//System.out.println("filename:" + inds.getColumn(i, "file_name") + ", path:" + inds.getColumn(i, "path") + 	", filesize:" + inds.getColumn(i, "filesize"));
		//byte[] file = inds.getColumn(i, "content").getBinary();
		//filename = inds.getColumn(i, "file_name").toString();
		//System.out.println(file);		
		ByteArrayInputStream is = new ByteArrayInputStream(file);
		//String filePath = "C:/upfile/web";
		filePath = filePath + "/" + filename;
		FileOutputStream s = new FileOutputStream(filePath);

		byte[] in = new byte[(int)file.length];  
		int len = 0;  
		int byteData = 0;
		int offset = 0;
		int ch;
		while ((len = is.read(in, offset, in.length)) != -1 );
		len = is.read(in, offset, in.length);
		s.write(in);			//서버로 파일 write
		is.close();
		s.close();	
		return result;
	}	
  
  /**
   * null 값을 해당 문자열 값으로 대체
   *
   * @param 문자열, 대체키
   * @return 문자열
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
   */
  public static String Isnull(String getData, char flg){
    String setData = "";

    switch (flg) {
      case 'N': setData = NBSP;
        break;
      case 'D': setData = DASH;
        break;
      case 'E' : setData = EMPTY ;
        break;
      case 'Z' : setData = ZERO ;
        break;
      case 'O' : setData = ONE ;
        break;
    }

    if (!(getData == null || getData.trim().equals(""))) {
      setData = getData;
    }
    return setData;
  }
  
  /**
   *  전달받은 이미지명을 해당 디렉토리의 해달 파일명으로 복사한다.
   *  @param FileFullName : 원본 이미지 Full Path
   *  @param FileTargetName : 변경 이미지 명
   *  @param FileTargetPath : 변경 디렉토리명
   *  @return rtn_file_name : DB에 반영할 파일명의 중복성을 체크후 파일명을 리턴한다.
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
   */
  public static String Change_filepath(String FileFullName, String FileTargetName, String FileTargetPath ) {
  	String rtn_file_name = "";
  	
  	try {
		// 파일 정보를 생성한다.
		rtn_file_name = FileTargetName;
		String new_file_path = FileTargetPath+FileTargetName;	    // 새로 복사될 파일

		 // 새로생성될 파일명이 존재하는지 체크 존재할 경우 '_01' 의 내용을 추가
		 File nowf = new File(FileFullName);		// 원본파일 객체
		 File chkf = new File(new_file_path);	// 중복파일 체크 객체
		 
		 boolean check = true;
		 int i=1;
		 String num = "";
		 while (check) {
		 	if (i < 10) num = "0"+Integer.toString(i);
		 	else num = Integer.toString(i);
		 	
		 	chkf = new File(new_file_path);
		 	if (chkf.exists()) {
		 		rtn_file_name = FileTargetName.substring(0,FileTargetName.lastIndexOf("."))+"_"+num+FileTargetName.substring(FileTargetName.lastIndexOf("."));
				new_file_path = FileTargetPath+rtn_file_name;
		 	} else {
		 	  check = false;
		 	}
		 	i++;
		 }

		  // 파일을 해당 폴더로 복사한다.
		  FileInputStream ins = new FileInputStream(FileFullName);
		  FileOutputStream outs = new FileOutputStream(new_file_path);

		  long filesize = nowf.length();
		  byte buffer[] = new byte[(int)filesize];
		  int read = ins.read(buffer);
		  outs.write(buffer);
	
		  ins.close();
		  outs.close();
		  
  	} catch (IOException eq) {
	}
	
	return rtn_file_name;
  }

  /**
   *  년월일로 구성된 스트링을 반환한다.
   *  @return YYYYMMDDHHMMSS
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
   */
  public static String makeDate() {
  	String year;
  	String month;
  	String day;
  	String hour;
  	String minute;
  	String second;
  	String full_name;
  	
	Date date = new Date();
	
	year = Integer.toString(date.getYear()+1900);
	month = (date.getMonth()+1) < 10?"0"+Integer.toString(date.getMonth()+1):Integer.toString(date.getMonth()+1);
	day = (date.getDate()) < 10?"0"+Integer.toString(date.getDate()):Integer.toString(date.getDate());
	hour = (date.getHours()) < 10?"0"+Integer.toString(date.getHours()):Integer.toString(date.getHours());
	minute = (date.getMinutes()) < 10?"0"+Integer.toString(date.getMinutes()):Integer.toString(date.getMinutes());
	second = (date.getSeconds()) < 10?"0"+Integer.toString(date.getSeconds()):Integer.toString(date.getSeconds());
	full_name = year+month+day+hour+minute+second;

	return full_name;
  }
  
  /**
   *  해당파일을 삭제한다.
	*
   *  @param 파일경로
   *  @param 파일명
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
   */
  public static void DeleteImg(String path, String img_name) {
    if (!img_name.equals("")) {
       FileSaverMng filemng = new FileSaverMng();
       
       filemng.delete(path, img_name);
    }
  }

  
  /**
   * BASE64 방식으로  encoding 한다.
	*
   *  @param 입력문자열
   *  @param 처리결과문자열
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
   */
  /*
  public static String base64_encode(String src){
  	  String s = null;
	  try { 
		  byte[] buf = src.getBytes(); 
		  s = new sun.misc.BASE64Encoder().encode(buf);
	  } catch (Exception e) {} 
	  return s;
  } 
  public static String base64_decode(String src){
  	byte[] buf = null;
	try { 
		buf = new sun.misc.BASE64Decoder().decodeBuffer(src); 
	} catch (IOException e) {}
	return new String(buf);
  }
  */
  
  
  
  	/**
   * split함수 정의(Begin)
   *
   *  @param 입력문자열
   *  @param 토큰
   *  @return 문자열 배열
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
   */
	public static String[] StrSplit(String str, String token) {
		String [] valueStr = new String[100];
		String tempStr = "";
		int i=0, j=0;
		int strLength = 0;

		while( (j=str.indexOf(token)) > -1 ) {
			valueStr[i] = str.substring(0, j);
			tempStr = str.substring(j+1);
			i++;
			str = tempStr;
		}
		valueStr[i] = str;
		i++;

		String [] orgStr = new String[i];
		for( j=0; j<i; j++ ) {
			orgStr[j] = valueStr[j];
		}
  
		return orgStr;
	}
  
  
  		/**
	   * split함수 정의(Begin)
	   *
	   * 지정한 사이즈로 문자열을 나누어 문자열 배열로 반환
	   * @param str, strSize
	   * @return String []
	   */
  public static String[] StrSubString(String str, int strSize) {
	  int i = 0;
	  int j = 0;
	  int k = 0;
	  int startNo = 0;
	  int endNo = 0;
	  int arrayCount = 0;
	  int strLen = 0;
	  int emptyCount = 0;
	  String tokenStr[] = null;
	  String temp = null;

	  strLen = str.length();
	  if(strLen > strSize) {
		  arrayCount = strLen/strSize;
		  if((strLen%strSize) > 0)
			  arrayCount = arrayCount + 1;
		  tokenStr = new String[arrayCount+1];

		  for(i=0; i<arrayCount; i++) {
			  temp = null;
			  k = 0;

			  if((i+1)==arrayCount && strLen>((i*strSize)+strSize-emptyCount))
				  arrayCount++;

			  if(strLen < ((i*strSize)+strSize-emptyCount)) {
				  startNo = i * strSize - emptyCount;
				  endNo = strLen - 1;
			  }
			  else {
				  startNo = i * strSize - emptyCount;
				  endNo = (i * strSize) + strSize - emptyCount;
			  }

			  for(j=endNo-1; j>=startNo; j--) {
				  if((int)str.charAt(j) > 127) {
					  k++;
				  }
				  else {
					  if(k%2 == 1) {
						  temp = str.substring(i*strSize-emptyCount, (i*strSize)+(strSize-(emptyCount+1)));
						  emptyCount++;
					  }
					  break;
				  }
			  }
			  if(temp == null) {
				  if(strLen < ((i*strSize)+strSize-emptyCount)) {
					  temp = str.substring(i*strSize-emptyCount, strLen);
				  }
				  else {
					  temp = str.substring(i*strSize-emptyCount, (i*strSize)+strSize-emptyCount);
				  }
			  }

			  try {
				  tokenStr[i] = temp;
			  }
			  catch(NullPointerException e) {}
		  }
	  }
	  return tokenStr;
  }
  
 	/**
  	* 전달받은 문자열이 숫자인지 아닌지를 체크
  	* 
  	* @param str - 숫자인지 아닌지 체크하고자 하는 문자열
  	* @return boolean - true : str은 숫자,  false : str은 숫자가 아님 
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
  	*/
  
  	public static boolean isNumber(String str) {
  		char c;
  		boolean flgResult = true;
		String tmp = str;
	
		if(tmp == null || tmp.equals("")){
			return false;
		} 

		for(int i = 0; i < tmp.length(); i++){
			c = tmp.charAt(i);
			if(c < 48 || c > 57){
				flgResult = false;
				break;
			}
		}
	  	return flgResult;
  	}
  	
	/**
	 * 윤년인지 아닌지 계산하는 함수
	 *
	 *	1. 4의 배수의 해는 윤년으로 정한다.
	 *	2. 이중 100의 배수의 해는 윤년이 아니다.
	 *	3. 2에 의해 윤년이 아니라 해도 400의 배수의 해는 윤년이다.
	 *
	 * @param int - 계산하고자 하는 년도
	 * @return boolean - 윤년인지 아닌지 체크한 결과
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 */
	public static boolean getLeapYear(int year){
		boolean flgResult = false;
		if (year % 4 == 0){
			flgResult = true ;
			if(year % 100 == 0){
				flgResult = false;
				if(year % 400 == 0){
					flgResult = true;					
				}
			}
			return flgResult;
		}
		else return false;
	}

	/**
	 * 해당 년, 월, 일의 요일을 구하는 함수
	 *
	 * @param year, month, date - 계산하고자 하는 년, 월, 일
	 * @return int - 해당 년, 월, 일의 요일 (1:일요일, 2:월요일, ... , 7:토요일)
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 */
	public static int getDayOfTheWeek(int year, int month, int date){
		Calendar cal = Calendar.getInstance();

		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month-1);
		cal.set(Calendar.DATE, date);
 
		return cal.get(Calendar.DAY_OF_WEEK);
	}	

	// 
	/**
	 * 입력받은 int 형을 문자열로 바꾸고 입력받은 자리수만큼 "0"을 붙여서 리턴해 줍니다.
	 *
	 * @param 입력수치, 지정길이
	 * @return 문자열
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 */
    public static String addZero(int val, int size) {
        String str = String.valueOf(val);       // int 형 값을 문자열로 변환합니다.
        int istrSize = 0;                       // 변환된 문자열의 길이를 담을 변수 입니다.
        StringBuffer sb = new StringBuffer();   // 앞에 붙일 문자열 0 들을 넣을 StringBuffer 입니다.
        istrSize = str.getBytes().length;       // 문자열의 길이를 구합니다.
        if(size < istrSize) {                   // 맟출려는 자리수보다 문자열의 길이가 더 크면 에러를 출력합니다.
            //System.out.println("맞추고자 하는 길이보다 변환한 문자열의 길이가 더 커요~");
            return "Error";
        }
        // size는 내가 맞추고자할 길이이고 istrSize는 현재 문자열의 길이 이므로
        // 맞추고자할 길이에서 문자열 길이를 뺀 만큼 앞에다 붙일 문자열 "0" 을 만듭니다.
        for(int i=0; i<(size-istrSize); i++) {
            sb.append("0");         // "0" 을 하나 붙입니다.
        }
        str = sb.toString() + str;  // 만들어진 문자열 "0..."과 int형을 변환한 문자열을 합쳐서 리턴합니다.
        return str; 
    }
    
}


