package maya.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

public class DBUtil {
	
	private String db_name = "";
	private String db_user = "";
	private String db_passwd = "";
	private String back_filePath = "";
	
	public DBUtil(){
		
	} 
	
	public void setDBInfo(String db_name, String user, String passwd){
		this.db_name = db_name;
		this.db_user = user;
		this.db_passwd = passwd; 
	}
	 
	public void tagBackUp(String filePath) {
		this.back_filePath = filePath;
        try {
        	 
            Runtime runtime = Runtime.getRuntime(); 
            File backupFile = new File(back_filePath);
            FileWriter fw = new FileWriter(backupFile);
            String runtime_str = "mysqldump --user="+db_user+" --password="+db_passwd+" --databases "+db_name+" --default-character-set=euckr --opt TS";
            //String runtime_str = "mysqldump --user="+db_user+" --password="+db_passwd+" --databases "+db_name+"  --opt TS";
            System.out.println(" backup DB : "+runtime_str);
            Process child = runtime.exec(runtime_str);
            InputStreamReader irs = new InputStreamReader(child.getInputStream());
            BufferedReader br = new BufferedReader(irs);

            String line;
            while ((line = br.readLine()) != null) {
            	//line = new String(line.getBytes("iso-8859-1"), "UTF-8"  );//한글처리
                fw.write(line + "\n");
            }
            fw.close();
            irs.close();
            br.close(); 
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
