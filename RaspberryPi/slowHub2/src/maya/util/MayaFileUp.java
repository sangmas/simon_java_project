package maya.util;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.oreilly.servlet.*;
//import maya.db.MysqlBean;
import org.apache.log4j.*;  
 
/***************************************************************************************
 * <pre>
 * <BR\> 프로젝트 : 연구문서관리(2011) 
 * <BR\> 소스 :   파일 업로드 처리
 * <BR\> 설명 : 기존 MayaFileUp 버전이  Resin 에 맞게
 * <BR\> -----------------------------------------------------------------------------------
 * <BR\> VER      DATE       AUTHOR    DESCRIPTION
 * <BR\> ---   ----------  ----------  -----------------------------------------------------
 * <BR\> 1.0   2010.08.18    김영돈           최초 프로그램 작성    
 * <BR\> -----------------------------------------------------------------------------------
 * <BR\> Copyright(c) 2010 ADDLAB ,  All rights reserved.
 ***************************************************************************************
 * </pre>
**/

public class MayaFileUp {
	private String saveDir = "D:/pot";//저장 경로
	private int maxFileSize = 1024*1024*50;//기본업로드 용량(초기값 3M)
	private String strEncode = "utf-8";//한글파일 업로드 설정 
	MultipartRequest multi = null;
	static Logger logger = Logger.getLogger(MayaFileUp.class);
	
	HashMap savedFileHm = new HashMap();//1단계메뉴
	
	/**
	 * 저장 디렉토리 설정
	 * @param dir 
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public void setSaveDir(String dir) {saveDir = dir;}

	/**
	 * 저장 디렉토리 가져오기 
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 */
	public String getSaveDir(){return saveDir;}

	/**
	 * 디렉토리 존재여부 체크 후 생성 
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public void checkSaveDir()
	{
		File createDir = new File(saveDir);
		if(!createDir.exists()) {
			//logger.debug("디렉토리 생성중");
			try
			{
				createDir.mkdirs();//폴더 없을시 생성			
			}
			catch (Exception e)
			{
				 logger.error(e.getMessage());
			}
		}
	}

	/**
	 * 파일 존재 여부 체크 
	 * @param filePathName
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 */
	public boolean isNewPathName(String filePathName)
	{
		File checkFile = new File(filePathName);
		if(checkFile.exists()) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * 최대  업로드 사이즈 지정 
	 * @param size
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 */
	public void setMaxFileSize(int size)
	{
		maxFileSize = 1024*1024*size;
	}

	/**
	 * 새 파일이름 만들기 
	 * @param dir
	 * @param fileName
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 */
	public String getNewFileName(String dir, String fileName)
	{
		
		String newFileName = fileName;
		java.util.Date date = new java.util.Date(); 
		java.sql.Timestamp t_cre_dt = new java.sql.Timestamp(date.getTime());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		//fileName = t_cre_dt.toString();
		newFileName = sdf.format(t_cre_dt)+"."+maya.util.Util.getFileExt(fileName);

		String newPathName = dir+"/"+newFileName;
		logger.debug("oldFile : "+newFileName);
		while(!isNewPathName(newPathName)){
			//logger.debug("oldFile : "+newFileName);
			newFileName = "0"+newFileName;
			newPathName = dir+"/"+newFileName;
			//logger.debug("newFile : "+newFileName);
		}
		return newFileName;
	}
	
	/**
	 * 파일저장 해쉬 구하기
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 */
	public HashMap getSavedFileHm(){
		return savedFileHm;
	}

	/**
	 * request 파라메터 받아오기 
	 * @param para
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 */
	public String getParameter(String para)
	{
		//return parser.getURLParameter(para);
		return multi.getParameter(para);
	}
	
	/**
	 * 파일 파라메터 받아오기
	 * @param para
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 */
	public String getFileParameter(String para)
	{
		if (savedFileHm.get(para)!=null){
			return savedFileHm.get(para).toString();
		} else {
			return null;
		}
	}
	
	/**
	 * 파일 삭제 (실제 파일 삭제)
	 * @param fileName
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 */
	public boolean delFileReal(String fileName) {
		boolean result = false;
		java.io.File f = new java.io.File(fileName);
		if (f.exists()) {
			logger.debug("파일존재 " + fileName);
			f.delete();
		} else {
			logger.debug("파일이 없습니다. " + fileName);
			result = false;
		}
		return result;
	}	
	
	
	/**
	 * request 로 부터 받아 처리
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{ 
		this.multi = 
			new MultipartRequest(request, this.saveDir, this.maxFileSize, strEncode, new com.oreilly.servlet.multipart.DefaultFileRenamePolicy());
		for(int i=1; i<=5; i++){
		   String paraname = "upfile"+i;
		   logger.debug("폼속성명 :" + paraname);
		   String fileName = multi.getFilesystemName(paraname);
		   if(fileName!=null){
			   File f = multi.getFile(paraname);
			   logger.debug(">>>>>업로드된 파일명 : "+fileName);
			   logger.debug(">>>>>파일 사이즈 :"+f.length()+ " bytes : ");
 
			   savedFileHm.put(paraname+"_name",fileName);
			   savedFileHm.put(paraname+"",fileName);
			   savedFileHm.put(paraname+"_size",Long.toString(f.length()));
		   }
	   }
	}

	/**
	 * 기존 dopost()메서드에 파일파라미터 폼속성명 및 폼속성명의 숫자를 추가인자로 받는다.
	 * by JKH
	 * 	 * @param request
	 * @param response
	 * @param fileParam
	 * @param startFileParamNum
	 * @param endFileParamNum
	 * @throws ServletException
	 * @throws IOException
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 */
	public void doPost2(HttpServletRequest request, HttpServletResponse response, String fileParam, int startFileParamNum, int endFileParamNum) throws ServletException, IOException
	{
		this.multi = 
			new MultipartRequest(request, this.saveDir, this.maxFileSize, strEncode, new com.oreilly.servlet.multipart.DefaultFileRenamePolicy());
		for(int i=startFileParamNum; i<=endFileParamNum; i++){
			String paraname = "";
			if(startFileParamNum == endFileParamNum || (startFileParamNum==0 && endFileParamNum==0)){
			   paraname = fileParam;
		   }
		   else{
			   paraname = fileParam+i;	   
		   }
		   logger.debug("폼속성명 :" + paraname);
		   String fileName = multi.getFilesystemName(paraname);
		   if(fileName!=null){
			   File f = multi.getFile(paraname);
			   logger.debug(">>>>>업로드된 파일명 : "+fileName);
			   logger.debug(">>>>>파일 사이즈 :"+f.length()+ " bytes : ");

			   savedFileHm.put(paraname+"_name",fileName);
			   savedFileHm.put(paraname+"",fileName);
			   savedFileHm.put(paraname+"_size",Long.toString(f.length()));
		   }
	   }
	}
	
	/**
	 * 업로드 된 파일명 가져오기 
	 * @param fileParam
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 */
	public String getUploadFileName(String fileParam){
		String resultStr = "";
		if(this.multi!=null){
			resultStr = multi.getFilesystemName(fileParam);	
		} 
		return resultStr;
	}
	
}
