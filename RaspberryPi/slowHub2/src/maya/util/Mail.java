package maya.util;
import java.util.Properties;  
import javax.mail.Authenticator;
import javax.mail.Message;  
import javax.mail.MessagingException;  
import javax.mail.PasswordAuthentication;  
import javax.mail.Session;  
import javax.mail.Transport;  
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;   
import javax.mail.internet.MimeUtility;


/***************************************************************************************
 * <pre>
 * <BR\> 프로젝트 : 연구문서관리(2011) 
 * <BR\> 소스 :  메일발송
 * <BR\> -----------------------------------------------------------------------------------
 * <BR\> VER      DATE       AUTHOR    DESCRIPTION
 * <BR\> ---   ----------  ----------  -----------------------------------------------------
 * <BR\> 1.0   2010.08.18    김영돈           최초 프로그램 작성    
 * <BR\> -----------------------------------------------------------------------------------
 * <BR\> Copyright(c) 2010 ADDLAB ,  All rights reserved.
 ***************************************************************************************
 * </pre>
**/
  
public class Mail {  

	public void MailSend(String fromMail, String title, String content, String recevs[]){ 
		
		try {  
			String[] emailList = recevs ; //{ "jinhi78i@hanmail.net" };// 받는사람 메일   
			String emailFromAddress = fromMail ;// 메일 보내는 사람  
			String emailMsgTxt = content; // 내용  
			String emailSubjectTxt = title;// 제목  
			// 메일보내기   
			postMail(emailList, emailSubjectTxt, emailMsgTxt, emailFromAddress);  
			System.out.println("모든 사용자에게 메일이 성공적으로 보내졌음~~");  
		} catch (MessagingException e) {  
			e.printStackTrace();  
		}  
	}  
	
	
	/**
	 * 메일 보내기
	 * @param recipients
	 * @param subject
	 * @param message
	 * @param from
	 * @throws MessagingException
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	private void postMail(String recipients[], String subject, String message, String from) throws MessagingException {  
		boolean debug = false;  
		//java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
		//상기라인  에러때문에 임시로 막아놈 2011-09-06
		String SMTP_HOST_NAME = "gmail-smtp.l.google.com";
		//String SMTP_HOST_NAME = "mail.kisoe.net";
		
		// Properties 설정  
		Properties props = new Properties();  
		props.put("mail.transport.protocol", "smtp");  
		props.put("mail.smtp.starttls.enable","true");  
		props.put("mail.smtp.host", SMTP_HOST_NAME);  
		props.put("mail.smtp.auth", "true");  
		
		Authenticator auth = new SMTPAuthenticator();   
		Session session = Session.getDefaultInstance(props, auth);  
		session.setDebug(debug);  
	
		// create a message  
		Message msg = new MimeMessage(session);  
		// set the from and to address  
		InternetAddress addressFrom = new InternetAddress(from);  
		msg.setFrom(addressFrom); 
		msg.setContent(message, "text/html; charset=euc-kr"); //
		InternetAddress[] addressTo = new InternetAddress[recipients.length];  
		for (int i = 0; i < recipients.length; i++) {  
			addressTo[i] = new InternetAddress(recipients[i]);  
		}  
		msg.setRecipients(Message.RecipientType.TO, addressTo);  
		// Setting the Subject and Content Type  
		msg.setSubject(subject);  
		//msg.setContent(message, "text/plain");  
		Transport.send(msg);  
	}  
	
	/**
	 * 구글 사용자 메일 계정 아이디/패스 정보  
	 * @author 김영돈
	 * @version 	: 1.0
	 * @date	 	: 2010. 8. 18.
	 */
	private class SMTPAuthenticator extends javax.mail.Authenticator {  
		public PasswordAuthentication getPasswordAuthentication() {  
			String username =  "leejinhi@addlab.co.kr"; // gmail 사용자;  
			String password = "leejinhi78"; // 패스워드;  
			//String username =  "kisoe19@kisoe.net"; // gmail 사용자;  
			//String password = "kisoe"; // 패스워드;  
			return new PasswordAuthentication(username, password);  
		}  
	}  
} 
