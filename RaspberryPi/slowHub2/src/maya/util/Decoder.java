package maya.util;
/***************************************************************************************
 * <pre>
 * <BR\> 프로젝트 : 연구문서관리(2011) 
 * <BR\> 소스 :    Encoding 을 Decode 화 하는 함수
 * <BR\> -----------------------------------------------------------------------------------
 * <BR\> VER      DATE       AUTHOR    DESCRIPTION
 * <BR\> ---   ----------  ----------  -----------------------------------------------------
 * <BR\> 1.0   2010.08.18    김영돈           최초 프로그램 작성    
 * <BR\> -----------------------------------------------------------------------------------
 * <BR\> Copyright(c) 2010 ADDLAB ,  All rights reserved.
 ***************************************************************************************
 * </pre>
**/

public class Decoder
{
	/**
	 * 문자열 디코딩
	 * @param s
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
    public static String decode(String s)
    {
		if(s==null ){
			return null;
		}

        byte abyte0[] = null;
        try
        {
            StringBuffer stringbuffer = new StringBuffer();
            for(int i = 0; i < s.length(); i += 2){
            	String tmp ="";
            	try{
            		tmp = s.substring(i, i + 2);
                    stringbuffer.append((char)Integer.parseInt(tmp, 16));
            	}catch(java.lang.StringIndexOutOfBoundsException e){
            	//	tmp = s.substring(i, i + 1);
            	}
                //stringbuffer.append((char)Integer.parseInt(s.substring(i, i + 2), 16));

            }
            //System.out.println("문자열0 : "+stringbuffer);	
            String s1 = stringbuffer.toString();
            //System.out.println("문자열 : "+s1);	
            abyte0 = s1.getBytes("8859_1");
        }
        catch(Exception exception)
        {
        	System.out.println("Decoder.decode() : "+ exception.toString());
        }
        return new String(abyte0);
    }

    /**
     * 생성자
     */
    public Decoder()
    {
    }
}
