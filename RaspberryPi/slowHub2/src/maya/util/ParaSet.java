package maya.util;

/***************************************************************************************
 * <pre>
 * <BR\> 프로젝트 : 연구문서관리(2011) 
 * <BR\> 소스 :   파라메터 변수, 값을 갖는 클래스 셋
 * <BR\>  설명 : 파라메터 Vector를 만들때 사용 
 * <BR\> -----------------------------------------------------------------------------------
 * <BR\> VER      DATE       AUTHOR    DESCRIPTION
 * <BR\> ---   ----------  ----------  -----------------------------------------------------
 * <BR\> 1.0   2010.08.18    김영돈           최초 프로그램 작성    
 * <BR\> -----------------------------------------------------------------------------------
 * <BR\> Copyright(c) 2010 ADDLAB ,  All rights reserved.
 ***************************************************************************************
 * </pre>
**/
public class  ParaSet
{
	public String key = "";
	public String value = "";

	/**
	 * 값 생성
	 * @param key
	 * @param value
	 */
	public ParaSet(String key, String value){
		this.key = key;
		this.value = value;
	}
}


