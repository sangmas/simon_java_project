package maya.util;

import java.io.*;   
import java.util.*;   
 

/***************************************************************************************
 * <pre>
 * <BR\> 프로젝트 :연구문서관리(2011) 
 * <BR\>  소스 : 프로퍼티 유틸리티
 * <BR\> -----------------------------------------------------------------------------------
 * <BR\> VER      DATE       AUTHOR    DESCRIPTION
 * <BR\> ---   ----------  ----------  -----------------------------------------------------
 * <BR\> 1.0   2010.08.18    김영돈           최초 프로그램 작성    
 * <BR\> -----------------------------------------------------------------------------------
 * <BR\> Copyright(c) 2010 ADDLAB ,  All rights reserved.
 ***************************************************************************************
 * </pre>
**/

public class PropertyUtil2{
    private String PROPERTIES_FILE = "C:\\globals.properties";
    

    /**
     * 생성자
     */
    public PropertyUtil2(){
    }
    
    /**
     *파일지정 생성자
     * @param path
     */
    public PropertyUtil2(String path){
    	PROPERTIES_FILE = path;
    }
    
    /**
     * 특정 키값을 반환한다.
     * @param keyName
     * @return
     * @version : 1.0
     * @date : 2010. 8. 18.
     * @Author : 김영돈
     *
     */
    public  String getProperty(String keyName) {
         String value = null;
  
         try {
            Properties props = new Properties();
            FileInputStream  fis = new FileInputStream(PROPERTIES_FILE);
            props.load(new java.io.BufferedInputStream(fis)); 
            value = props.getProperty(keyName).trim();
             fis.close();
        } catch(java.lang.Exception e) {
            System.out.println(e.toString());
         }
         return value;
    }
 
 
    /**
     * 특정 키 이름으로 값을 설정한다.
     * @version : 1.0
     * @date : 2010. 8. 18.
     * @Author : 김영돈
     */
    public void setProperty(String keyName, String value) {
         try {
             Properties props = new Properties();
             FileInputStream fis  = new FileInputStream(PROPERTIES_FILE);
             props.load(new java.io.BufferedInputStream(fis));
            props.setProperty(keyName, value); 
            props.store(new FileOutputStream(PROPERTIES_FILE), "");
            fis.close();
         } catch(java.lang.Exception e) {
            System.out.println(e.toString()); System.out.println(e.toString()); 
        }
    }
}
