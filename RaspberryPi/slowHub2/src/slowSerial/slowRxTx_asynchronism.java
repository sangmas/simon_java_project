package slowSerial;




import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import gnu.io.UnsupportedCommOperationException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Enumeration;
import java.util.TooManyListenersException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public class slowRxTx_asynchronism implements Runnable, SerialPortEventListener {
	static CommPortIdentifier portId;
	static Enumeration<CommPortIdentifier> portList;
	static CommPort commPort;
	static InputStream inputStream;
	static OutputStream outputStream;
	static SerialPort serialPort;
	Thread readThread;//////////////////////////////////////////////////////////////////////////주석처리해도 잘됨
	static String targetPort = "COM7";                                     //기본시리얼포트
	//static String targetPort = "COM7";                                     //기본시리얼포트
	static private int maxMsgBufferSize =500;                              //메시지크기의 2배 -1
	static String saveRecivePacket = "";                                   //수신 메시지 패킷
	static String switchCommand = "";
	public String DemonStr ="";
	
	public void setSwitchCommand(String cmm){
		switchCommand = cmm;		
	}
	
	public void init(String targetPort1) throws Exception{
		System.out.println(" targetPort is : "+targetPort1);
		CommPortIdentifier portIdentifier/*포트 식별자*/ = CommPortIdentifier.getPortIdentifier( targetPort1 );
		int timeout = 2000;
		if( portIdentifier.isCurrentlyOwned() ) {                                                              //포트가 사용중일때
			System.out.println( "Error: Port is currently in use" );  
			//commPort = portIdentifier.open(this.getClass().getName(), timeout );
		} else {                                                                                                       //포트가 사용중이지 않을 때 포트연결생성
			commPort/*연결된 포트*/ = portIdentifier/*포트연결생성*/.open(this.getClass().getName(), timeout );
		}

			if(commPort instanceof SerialPort ) {   /*연결된 포트가 시리얼 포트로 형변환 가능하다면*/
				serialPort = (SerialPort)commPort;        //형변환시켜서 위에 스태틱으로 설정한 serialPort에 넣는다
				serialPort.setSerialPortParams( 9600, //속도설정
						SerialPort.DATABITS_8,
						SerialPort.STOPBITS_1,
						SerialPort.PARITY_NONE );

				inputStream = serialPort.getInputStream();  // 위의 스태틱으로 설정한 인풋스트림과 아웃풋스트림에 시리얼통신의 getInputStream과 getOutputStream을 넣는다
				outputStream = serialPort.getOutputStream();
				this.rx();                                           //수신
				this.tx(switchCommand);                       //송신
			} else {
				System.out.println( "Error: Only serial ports are handled by this example." );
			}
		}
		
	public void rx(){
		try{
			serialPort.addEventListener(this);
		}catch(TooManyListenersException e) {
		}
		serialPort.notifyOnDataAvailable(true);
		try {
			serialPort.setSerialPortParams(9600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
		}catch(UnsupportedCommOperationException e1){
		}
	}
	public void tx(String messageString){
		//String messageString = "Hello, world! \n";
			try {
				if(messageString.equals("k")){
				//outputStream.write(messageString.getBytes());
					outputStream.write('k');
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Sending message : '"+messageString+"'");
	}
	
	public void run(){
		System.out.println("\nHere is ComRxTx Thread");
		try {
			//this.init(targetPort);
				this.init("COM7");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				this.init("ttyACM1");  
			}catch (Exception _e) {}
		}
	}
	
	public void serialEvent(SerialPortEvent event){
		switch(event.getEventType()){
		case SerialPortEvent.BI:
		case SerialPortEvent.OE:
		case SerialPortEvent.FE:
		case SerialPortEvent.PE:
		case SerialPortEvent.CD:
		case SerialPortEvent.CTS:
		case SerialPortEvent.DSR:			
		case SerialPortEvent.RI:
		case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
			break;
		case SerialPortEvent.DATA_AVAILABLE:
			
			
			
			byte[] readBuffer = new byte[1024];
			int numBytes = -1;
			try{
				while(inputStream.available()>30){
					numBytes = inputStream.read(readBuffer);
				}
				
				//System.out.print("\n[size]"+numBytes);
				String tmpMsg = new String(readBuffer, 0, numBytes);
				saveRecivePacket(tmpMsg);
				//System.out.println("test!!!!!!!!!!! : "+tmpMsg);
				
				StringBuffer sb = new StringBuffer();
				
				//DemonStr은 사용안하고 있었음.
				//System.out.println("****test3333333 : "+DemonStr);
				//DemonStr=sb.append(DemonStr).append(tmpMsg).toString();
				//System.out.println("****test4444444 : "+DemonStr);
				
				
				
				String strVO = getReciveMsg();
				String resultVO="";
				System.out.println("************serial 수신시작***********");
				System.out.println(getReciveMsg());
				System.out.println("@@@@@@@@@@@@serial 수신종료@@@@@@@@@@@@@");
				resultVO = strVO; 
				int Start_STR_Index = resultVO.indexOf("<@@["); 
				String s1 = resultVO.substring(Start_STR_Index);
				
				int End_STR_Index = s1.indexOf("]@@>"); 
				End_STR_Index=End_STR_Index + 4;
				//시작값위치부터 끝값-1 위치의 인덱스까지 추출
				//substring()
				//"unhappy". substring(2)에서는 "happy" 가 돌려주어진다
				//"hamburger". substring(4, 8)에서는 "urge" 가 돌려주어진다 
				String s2 = s1.substring(0, End_STR_Index);				
				System.out.println("");
				
				if(s2.endsWith("]@@>")){
					resultVO=s2;
					System.out.println("올바른 값 추출완료!");		        	
					resultVO=resultVO.replace("<@@[", "");
					resultVO=resultVO.replace("]@@>", "");
					String[] sensorVoArr = resultVO.split(",");
					System.out.println("다음의 10개 값이 DB에 insert됩니다. "+sensorVoArr[0]+" / "+sensorVoArr[1]+" / "+sensorVoArr[2]+" / "+sensorVoArr[3]+" / "+sensorVoArr[4]+" / "+sensorVoArr[5]+" / "+sensorVoArr[6]+" / "+sensorVoArr[7]+" / "+sensorVoArr[8]+" / "+sensorVoArr[9]);
					sensorVoArr[2]=sensorVoArr[2].replace(" ", "");
					saveSensingData_new54(sensorVoArr[0], sensorVoArr[1], sensorVoArr[2], sensorVoArr[3], sensorVoArr[4], sensorVoArr[5], sensorVoArr[6], sensorVoArr[7], sensorVoArr[8], sensorVoArr[9]);
					System.out.println("");System.out.println("");System.out.println("");
					
					//alter_saveRecivePacket()는 DB에 저장된 스트링을 잘라내기 위한 메소드입니다.
					alter_saveRecivePacket(Start_STR_Index+End_STR_Index+1);
				}
				else{
					System.out.println("값을 추출하지 못했습니다.");	
					System.out.println("");System.out.println("");System.out.println("");
				}
				
			}catch (IOException e){}
			
		} 
	}
	
	public static void saveSensingData_new54(String D0, String D1, String D2, String D3, String D4, String D5, String D6, String D7, String D8, String D9){
		//System.out.println("센싱정보 기록 : "+i);
		//if(sensorVoArr.length>2){
		
		//http://203.230.100.54:8080/slowHub/ksh_ui_ver3/proc/deviceExec.jsp?pMode=insert&node_name=Air_1&co2=7&=7&temp=7&humid=7&lpg=7&voc=7&cds=7&uv=7&dust=7&ozone=7&communication_type=ZigBee
			URI uri = null;
			
			//if(B.equals("04")){
			try {
					uri = new URIBuilder()
					.setScheme("http")
					.setHost("203.230.100.62:8080")
					.setPath("/slowHub/ksh_ui_ver3/proc/deviceExec.jsp")
					.setParameter("pMode", "insert")
					.setParameter("node_name",D0)
					//co2=7&=7&temp=7&humid=7&lpg=7&voc=7&cds=7&uv=7&dust=7&ozone=7&communication_type=ZigBee
					.setParameter("co2",D1)
					.setParameter("temp",D2)
					.setParameter("humid",D3)
					.setParameter("lpg",D4)
					.setParameter("voc",D5)
					.setParameter("cds",D6)
					.setParameter("uv",D7)
					.setParameter("dust",D8)
					.setParameter("ozone",D9)
					.setParameter("communication_type","ZigBee")
					.build();
				} catch (URISyntaxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			CloseableHttpClient httpclient = HttpClients.createDefault(); 
			HttpGet httpget = new HttpGet(uri);
			try {
				CloseableHttpResponse response = httpclient.execute(httpget);
				try {
				} finally {
				    response.close();
				}
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
			} catch (HttpHostConnectException e) {
				// TODO Auto-generated catch block
				System.out.println("웹서버가 응답하지 않습니다.");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//System.out.println(httpget.getURI());			
	}
	
	public static void saveSensingData(String A, String B, String C){
		//System.out.println("센싱정보 기록 : "+i);
		//if(sensorVoArr.length>2){
			URI uri = null;
			try {
				uri = new URIBuilder()
				.setScheme("http")
				.setHost("203.230.100.54:8080")
				.setPath("/slowHub/test_ljh/proc/tabExec.jsp")
				.setParameter("pMode", "insert")
				//.setParameter("device_id", "ED00000001")
				//.setParameter("sensor_id", "01")
				//.setParameter("sensor_value", "30.2")
				.setParameter("nodeid",A)
				.setParameter("sensortype",B)
				.setParameter("value",C)
				.build();
				
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			CloseableHttpClient httpclient = HttpClients.createDefault(); 
			HttpGet httpget = new HttpGet(uri);
			try {
				CloseableHttpResponse response = httpclient.execute(httpget);
				try {
				} finally {
				    response.close();
				}
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
			} catch (HttpHostConnectException e) {
				// TODO Auto-generated catch block
				System.out.println("웹서버가 응답하지 않습니다.");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(httpget.getURI());			
	}
	

	
	public static void saveRecivePacket(String tmpMsg){
		if(maxMsgBufferSize<saveRecivePacket.length()){
			saveRecivePacket = "";
		}
		//System.out.println("****test1111111 : "+saveRecivePacket);
		saveRecivePacket +=tmpMsg;
		//System.out.println("****test2222222 : "+saveRecivePacket);
	}
		
	public static String getReciveMsg(){
		return saveRecivePacket;
	}
	private void alter_saveRecivePacket(int i){
		saveRecivePacket = saveRecivePacket.substring(i);
	}
	
}


