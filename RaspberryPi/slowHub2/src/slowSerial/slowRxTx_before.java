package slowSerial;



import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.TooManyListenersException;




import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import gnu.io.UnsupportedCommOperationException;

public class slowRxTx_before implements Runnable, SerialPortEventListener {
	static CommPortIdentifier portId;
	static Enumeration<CommPortIdentifier> portList;
	static CommPort commPort;
	static InputStream inputStream;
	static OutputStream outputStream;
	static SerialPort serialPort;
	Thread readThread;//////////////////////////////////////////////////////////////////////////주석처리해도 잘됨
	static String targetPort = "COM1";                                     //기본시리얼포트
	static private int maxMsgBufferSize = 50;                              //메시지크기의 2배 -1
	static String saveRecivePacket = "";                                   //수신 메시지 패킷
	static String switchCommand = "";
	
	public void setSwitchCommand(String cmm){
		switchCommand = cmm;		
	}
	
	public void init(String targetPort1) throws Exception{
		System.out.println(" targetPort is : "+targetPort1);
		CommPortIdentifier portIdentifier/*포트 식별자*/ = CommPortIdentifier.getPortIdentifier( targetPort1 );
		int timeout = 2000;
		if( portIdentifier.isCurrentlyOwned() ) {                                                              //포트가 사용중일때
			System.out.println( "Error: Port is currently in use" );  
			//commPort = portIdentifier.open(this.getClass().getName(), timeout );
		} else {                                                                                                       //포트가 사용중이지 않을 때 포트연결생성
			commPort/*연결된 포트*/ = portIdentifier/*포트연결생성*/.open(this.getClass().getName(), timeout );
		}

			if(commPort instanceof SerialPort ) {   /*연결된 포트가 시리얼 포트로 형변환 가능하다면*/
				serialPort = (SerialPort)commPort;        //형변환시켜서 위에 스태틱으로 설정한 serialPort에 넣는다
				serialPort.setSerialPortParams( 9600, //속도설정
						SerialPort.DATABITS_8,
						SerialPort.STOPBITS_1,
						SerialPort.PARITY_NONE );

				inputStream = serialPort.getInputStream();  // 위의 스태틱으로 설정한 인풋스트림과 아웃풋스트림에 시리얼통신의 getInputStream과 getOutputStream을 넣는다
				outputStream = serialPort.getOutputStream();
				this.rx();                                           //수신
				this.tx(switchCommand);                       //송신
			} else {
				System.out.println( "Error: Only serial ports are handled by this example." );
			}
		}
		
		  
		
/*
			int timeout = 2000;
			CommPort commPort = portIdentifier.open(this.getClass().getName(), timeout );

			if(commPort instanceof SerialPort ) {
				serialPort = (SerialPort)commPort;
				serialPort.setSerialPortParams( 115200,
						SerialPort.DATABITS_8,
						SerialPort.STOPBITS_1,
						SerialPort.PARITY_NONE );

				inputStream = serialPort.getInputStream();
				outputStream = serialPort.getOutputStream();
				this.rx();
				this.tx(switchCommand);
			} else {
				System.out.println( "Error: Only serial ports are handled by this example." );
			}
*/

	
	/*
	public void init(String targetPort1){
		System.out.println(" ComRxTx.java init()");
		portList = CommPortIdentifier.getPortIdentifiers();    //�ы듃紐⑸줉 媛�졇�ㅺ린
		if(portList.hasMoreElements()){
			System.out.println("Found Port(s)!! ");
		} else {
			System.out.println("Not found Port!! ");
		}
		while(portList.hasMoreElements()){
			portId = (CommPortIdentifier)portList.nextElement();
			System.out.println(" Found port: "+portId.getName());
			if(portId.getPortType() == CommPortIdentifier.PORT_SERIAL){
				//System.out.println(" Found Serial : "+portId.getName());
				if(portId.getName().equals(targetPort1)){
					System.out.println(" That's found Target Port : "+targetPort1);
					try{
						serialPort = (SerialPort)portId.open("ComRxTx", 100);						
					}
					catch (PortInUseException e1){
					}
					try{
						if(serialPort != null && outputStream==null)
						outputStream = serialPort.getOutputStream();
						inputStream = serialPort.getInputStream();
					} 
					catch(java.io.IOException e){
					}
					this.rx();
					this.tx(switchCommand);
				} 
			}else{
				
			}
		}
	}
	*/
	public void rx(){
		try{
			serialPort.addEventListener(this);
		}catch(TooManyListenersException e) {
		}
		serialPort.notifyOnDataAvailable(true);
		try {
			serialPort.setSerialPortParams(9600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
		}catch(UnsupportedCommOperationException e1){
		}
	}
	public void tx(String messageString){
		//String messageString = "Hello, world! \n";
			try {
				//1번멀티탭제어관련
				if(messageString.equals("a")){
				//outputStream.write(messageString.getBytes());
					outputStream.write('5');
					outputStream.write('1');
					outputStream.write('1');
					outputStream.write('0');
					outputStream.write('1');
					outputStream.write('\r');
				}
							else if(messageString.equals("a-1")){
							//outputStream.write(messageString.getBytes());
								outputStream.write('5');
								outputStream.write('1');
								outputStream.write('1');
								outputStream.write('1');
								outputStream.write('1');
								outputStream.write('\r');
							}				else if(messageString.equals("a-2")){
							//outputStream.write(messageString.getBytes());
								outputStream.write('5');
								outputStream.write('1');
								outputStream.write('1');
								outputStream.write('2');
								outputStream.write('1');
								outputStream.write('\r');
							}
				else if(messageString.equals("b")){
				//outputStream.write(messageString.getBytes());
					outputStream.write('5');
					outputStream.write('1');
					outputStream.write('0');
					outputStream.write('0');
					outputStream.write('1');
					outputStream.write('\r');
				}
							else if(messageString.equals("b-1")){
							//outputStream.write(messageString.getBytes());
								outputStream.write('5');
								outputStream.write('1');
								outputStream.write('0');
								outputStream.write('1');
								outputStream.write('1');
								outputStream.write('\r');
							}				else if(messageString.equals("b-2")){
							//outputStream.write(messageString.getBytes());
								outputStream.write('5');
								outputStream.write('1');
								outputStream.write('0');
								outputStream.write('2');
								outputStream.write('1');
								outputStream.write('\r');
							}
				else if(messageString.equals("c")){
				//outputStream.write(messageString.getBytes());
					outputStream.write('5');
					outputStream.write('1');
					outputStream.write('1');
					outputStream.write('0');
					outputStream.write('2');
					outputStream.write('\r');
				}
							else if(messageString.equals("c-1")){
							//outputStream.write(messageString.getBytes());
								outputStream.write('5');
								outputStream.write('1');
								outputStream.write('1');
								outputStream.write('1');
								outputStream.write('2');
								outputStream.write('\r');
							}				else if(messageString.equals("c-2")){
							//outputStream.write(messageString.getBytes());
								outputStream.write('5');
								outputStream.write('1');
								outputStream.write('1');
								outputStream.write('2');
								outputStream.write('2');
								outputStream.write('\r');
							}
				else if(messageString.equals("d")){
				//outputStream.write(messageString.getBytes());
					outputStream.write('5');
					outputStream.write('1');
					outputStream.write('0');
					outputStream.write('0');
					outputStream.write('2');
					outputStream.write('\r');
				}
							else if(messageString.equals("d-1")){
							//outputStream.write(messageString.getBytes());
								outputStream.write('5');
								outputStream.write('1');
								outputStream.write('0');
								outputStream.write('1');
								outputStream.write('2');
								outputStream.write('\r');
							}				else if(messageString.equals("d-2")){
							//outputStream.write(messageString.getBytes());
								outputStream.write('5');
								outputStream.write('1');
								outputStream.write('0');
								outputStream.write('2');
								outputStream.write('2');
								outputStream.write('\r');
							}
				else if(messageString.equals("e")){
				//outputStream.write(messageString.getBytes());
					outputStream.write('5');
					outputStream.write('1');
					outputStream.write('1');
					outputStream.write('0');
					outputStream.write('3');
					outputStream.write('\r');
				}
							else if(messageString.equals("e-1")){
							//outputStream.write(messageString.getBytes());
								outputStream.write('5');
								outputStream.write('1');
								outputStream.write('1');
								outputStream.write('1');
								outputStream.write('3');
								outputStream.write('\r');
							}				else if(messageString.equals("e-2")){
							//outputStream.write(messageString.getBytes());
								outputStream.write('5');
								outputStream.write('1');
								outputStream.write('1');
								outputStream.write('2');
								outputStream.write('3');
								outputStream.write('\r');
							}
				else if(messageString.equals("f")){
				//outputStream.write(messageString.getBytes());
					outputStream.write('5');
					outputStream.write('1');
					outputStream.write('0');
					outputStream.write('0');
					outputStream.write('3');
					outputStream.write('\r');
				}
							else if(messageString.equals("f-1")){
							//outputStream.write(messageString.getBytes());
								outputStream.write('5');
								outputStream.write('1');
								outputStream.write('0');
								outputStream.write('1');
								outputStream.write('3');
								outputStream.write('\r');
							}				else if(messageString.equals("f-2")){
							//outputStream.write(messageString.getBytes());
								outputStream.write('5');
								outputStream.write('1');
								outputStream.write('0');
								outputStream.write('2');
								outputStream.write('3');
								outputStream.write('\r');
							}
				else if(messageString.equals("q")){
				//outputStream.write(messageString.getBytes());
					outputStream.write('5');
					outputStream.write('1');
					outputStream.write('1');
					outputStream.write('0');
					outputStream.write('4');
					outputStream.write('\r');
				}
							else if(messageString.equals("q-1")){
							//outputStream.write(messageString.getBytes());
								outputStream.write('5');
								outputStream.write('1');
								outputStream.write('1');
								outputStream.write('1');
								outputStream.write('4');
								outputStream.write('\r');
							}				else if(messageString.equals("q-2")){
							//outputStream.write(messageString.getBytes());
								outputStream.write('5');
								outputStream.write('1');
								outputStream.write('1');
								outputStream.write('2');
								outputStream.write('4');
								outputStream.write('\r');
							}
				else if(messageString.equals("w")){
				//outputStream.write(messageString.getBytes());
					outputStream.write('5');
					outputStream.write('1');
					outputStream.write('0');
					outputStream.write('0');
					outputStream.write('4');
					outputStream.write('\r');
				}
							else if(messageString.equals("w-1")){
							//outputStream.write(messageString.getBytes());
								outputStream.write('5');
								outputStream.write('1');
								outputStream.write('0');
								outputStream.write('1');
								outputStream.write('4');
								outputStream.write('\r');
							}				else if(messageString.equals("w-2")){
							//outputStream.write(messageString.getBytes());
								outputStream.write('5');
								outputStream.write('1');
								outputStream.write('0');
								outputStream.write('2');
								outputStream.write('4');
								outputStream.write('\r');
							}
				
				
				
				
				//2번멀티탭제어관련
				   else if(messageString.equals("g")){
					//outputStream.write(messageString.getBytes());
						outputStream.write('5');
						outputStream.write('2');
						outputStream.write('1');
						outputStream.write('0');
						outputStream.write('1');
						outputStream.write('\r');
					}
								else if(messageString.equals("g-1")){
								//outputStream.write(messageString.getBytes());
									outputStream.write('5');
									outputStream.write('2');
									outputStream.write('1');
									outputStream.write('1');
									outputStream.write('1');
									outputStream.write('\r');
								}				else if(messageString.equals("g-2")){
								//outputStream.write(messageString.getBytes());
									outputStream.write('5');
									outputStream.write('2');
									outputStream.write('1');
									outputStream.write('2');
									outputStream.write('1');
									outputStream.write('\r');
								}				
					else if(messageString.equals("h")){
					//outputStream.write(messageString.getBytes());
						outputStream.write('5');
						outputStream.write('2');
						outputStream.write('0');
						outputStream.write('0');
						outputStream.write('1');
						outputStream.write('\r');
					}
								else if(messageString.equals("h-1")){
								//outputStream.write(messageString.getBytes());
									outputStream.write('5');
									outputStream.write('2');
									outputStream.write('0');
									outputStream.write('1');
									outputStream.write('1');
									outputStream.write('\r');
								}				else if(messageString.equals("h-2")){
								//outputStream.write(messageString.getBytes());
									outputStream.write('5');
									outputStream.write('2');
									outputStream.write('0');
									outputStream.write('2');
									outputStream.write('1');
									outputStream.write('\r');
								}	
					else if(messageString.equals("i")){
					//outputStream.write(messageString.getBytes());
						outputStream.write('5');
						outputStream.write('2');
						outputStream.write('1');
						outputStream.write('0');
						outputStream.write('2');
						outputStream.write('\r');
					}
								else if(messageString.equals("i-1")){
								//outputStream.write(messageString.getBytes());
									outputStream.write('5');
									outputStream.write('2');
									outputStream.write('1');
									outputStream.write('1');
									outputStream.write('2');
									outputStream.write('\r');
								}				else if(messageString.equals("i-2")){
								//outputStream.write(messageString.getBytes());
									outputStream.write('5');
									outputStream.write('2');
									outputStream.write('1');
									outputStream.write('2');
									outputStream.write('2');
									outputStream.write('\r');
								}	
					else if(messageString.equals("j")){
					//outputStream.write(messageString.getBytes());
						outputStream.write('5');
						outputStream.write('2');
						outputStream.write('0');
						outputStream.write('0');
						outputStream.write('2');
						outputStream.write('\r');
					}
								else if(messageString.equals("j-1")){
								//outputStream.write(messageString.getBytes());
									outputStream.write('5');
									outputStream.write('2');
									outputStream.write('0');
									outputStream.write('1');
									outputStream.write('2');
									outputStream.write('\r');
								}				else if(messageString.equals("j-2")){
								//outputStream.write(messageString.getBytes());
									outputStream.write('5');
									outputStream.write('2');
									outputStream.write('0');
									outputStream.write('2');
									outputStream.write('2');
									outputStream.write('\r');
								}	
					else if(messageString.equals("k")){
					//outputStream.write(messageString.getBytes());
						outputStream.write('5');
						outputStream.write('2');
						outputStream.write('1');
						outputStream.write('0');
						outputStream.write('3');
						outputStream.write('\r');
					}
								else if(messageString.equals("k-1")){
								//outputStream.write(messageString.getBytes());
									outputStream.write('5');
									outputStream.write('2');
									outputStream.write('1');
									outputStream.write('1');
									outputStream.write('3');
									outputStream.write('\r');
								}				else if(messageString.equals("k-2")){
								//outputStream.write(messageString.getBytes());
									outputStream.write('5');
									outputStream.write('2');
									outputStream.write('1');
									outputStream.write('2');
									outputStream.write('3');
									outputStream.write('\r');
								}
					else if(messageString.equals("l")){
					//outputStream.write(messageString.getBytes());
						outputStream.write('5');
						outputStream.write('2');
						outputStream.write('0');
						outputStream.write('0');
						outputStream.write('3');
						outputStream.write('\r');
					}
								else if(messageString.equals("l-1")){
								//outputStream.write(messageString.getBytes());
									outputStream.write('5');
									outputStream.write('2');
									outputStream.write('0');
									outputStream.write('1');
									outputStream.write('3');
									outputStream.write('\r');
								}				else if(messageString.equals("l-2")){
								//outputStream.write(messageString.getBytes());
									outputStream.write('5');
									outputStream.write('2');
									outputStream.write('0');
									outputStream.write('2');
									outputStream.write('3');
									outputStream.write('\r');
								}	
					else if(messageString.equals("m")){
					//outputStream.write(messageString.getBytes());
						outputStream.write('5');
						outputStream.write('2');
						outputStream.write('1');
						outputStream.write('0');
						outputStream.write('4');
						outputStream.write('\r');
					}
								else if(messageString.equals("m-1")){
								//outputStream.write(messageString.getBytes());
									outputStream.write('5');
									outputStream.write('2');
									outputStream.write('1');
									outputStream.write('1');
									outputStream.write('4');
									outputStream.write('\r');
								}				else if(messageString.equals("m-2")){
								//outputStream.write(messageString.getBytes());
									outputStream.write('5');
									outputStream.write('2');
									outputStream.write('1');
									outputStream.write('2');
									outputStream.write('4');
									outputStream.write('\r');
								}
					else if(messageString.equals("n")){
					//outputStream.write(messageString.getBytes());
						outputStream.write('5');
						outputStream.write('2');
						outputStream.write('0');
						outputStream.write('0');
						outputStream.write('4');
						outputStream.write('\r');
					}
					else if(messageString.equals("n-1")){
					//outputStream.write(messageString.getBytes());
						outputStream.write('5');
						outputStream.write('2');
						outputStream.write('0');
						outputStream.write('1');
						outputStream.write('4');
						outputStream.write('\r');
					}				else if(messageString.equals("n-2")){
					//outputStream.write(messageString.getBytes());
						outputStream.write('5');
						outputStream.write('2');
						outputStream.write('0');
						outputStream.write('2');
						outputStream.write('4');
						outputStream.write('\r');
					}	
				
				
				
				
				//else if(messageString.equals("inquiry")){}
				else if(messageString.equals("t")){
				//outputStream.write(messageString.getBytes());
					outputStream.write('0');

					outputStream.write('\r');
				}
				else{
					outputStream.write(messageString.getBytes());
					
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Sending message : '"+messageString+"'");
	}
	
	public void run(){
		System.out.println("\nHere is ComRxTx Thread");
		try {
			//this.init(targetPort);
				this.init("/dev/ttyUSB0");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				this.init("/dev/ttyUSB1");  
			}catch (Exception _e) {}
		}
	}
	
	public void serialEvent(SerialPortEvent event){
		switch(event.getEventType()){
		case SerialPortEvent.BI:
		case SerialPortEvent.OE:
		case SerialPortEvent.FE:
		case SerialPortEvent.PE:
		case SerialPortEvent.CD:
		case SerialPortEvent.CTS:
		case SerialPortEvent.DSR:
			
		case SerialPortEvent.RI:
		case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
			break;
		case SerialPortEvent.DATA_AVAILABLE:
			
			byte[] readBuffer = new byte[1024];
			int numBytes = -1;
			try{
				while(inputStream.available()>0){
					numBytes = inputStream.read(readBuffer);
				}
				System.out.print("\n[size]"+numBytes);
				String tmpMsg = new String(readBuffer, 0, numBytes);
				System.out.print(tmpMsg);
				saveRecivePacket(tmpMsg);
			}catch (IOException e){}
			
		} 
	}

	
	public static void saveRecivePacket(String tmpMsg){
		if(maxMsgBufferSize<saveRecivePacket.length()){
			saveRecivePacket = "";
		}
		saveRecivePacket +=tmpMsg;
	} 
		
	public static String getReciveMsg(){
		return saveRecivePacket;
	}

}


