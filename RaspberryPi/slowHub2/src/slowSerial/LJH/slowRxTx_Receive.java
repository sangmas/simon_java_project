package slowSerial.LJH;



import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import gnu.io.UnsupportedCommOperationException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration; 
import java.util.StringTokenizer;
import java.util.TooManyListenersException;

import slowHub.testLjh.TabLjhDao;
import slowHub.testLjh.TabLjhVO;




public class slowRxTx_Receive implements Runnable, SerialPortEventListener {
	static CommPortIdentifier portId;
	static Enumeration<CommPortIdentifier> portList;
	static CommPort commPort;
	static InputStream inputStream;
	static OutputStream outputStream;
	static SerialPort serialPort;
	Thread readThread;//////////////////////////////////////////////////////////////////////////주석처리해도 잘됨
	static String targetPort = "COM3";                                     //기본시리얼포트
	static private int maxMsgBufferSize = 50;                              //메시지크기의 2배 -1
	static String saveRecivePacket = "";                                   //수신 메시지 패킷
	static String switchCommand = "";
	static public int seq_DB=0;												//DB에 들어갈 시퀀스
	
	public void setSwitchCommand(String cmm){
		switchCommand = cmm;	
		System.out.println("Receive 자바테스트입니다"+switchCommand);
	}

	
	
	public void init(String targetPort1) throws Exception{
		System.out.println(" targetPort is : "+targetPort1);
		CommPortIdentifier portIdentifier/*포트 식별자*/ = CommPortIdentifier.getPortIdentifier( targetPort1 );
		int timeout = 2000;
		if( portIdentifier.isCurrentlyOwned() ) {                                                              //포트가 사용중일때
			System.out.println( "Error: Port is currently in use" );  
			//commPort = portIdentifier.open(this.getClass().getName(), timeout );
		} else {                                                                                                       //포트가 사용중이지 않을 때 포트연결생성
			commPort/*연결된 포트*/ = portIdentifier/*포트연결생성*/.open(this.getClass().getName(), timeout );
		}

			if(commPort instanceof SerialPort ) {   /*연결된 포트가 시리얼 포트로 형변환 가능하다면*/
				serialPort = (SerialPort)commPort;        //형변환시켜서 위에 스태틱으로 설정한 serialPort에 넣는다
				serialPort.setSerialPortParams( 9600, //속도설정
						SerialPort.DATABITS_8,
						SerialPort.STOPBITS_1,
						SerialPort.PARITY_NONE );

				inputStream = serialPort.getInputStream();  // 위의 스태틱으로 설정한 인풋스트림과 아웃풋스트림에 시리얼통신의 getInputStream과 getOutputStream을 넣는다
				outputStream = serialPort.getOutputStream();
				this.rx();                                           //수신

			} else {
				System.out.println( "Error: Only serial ports are handled by this example." );
			}
			
			
		}
	
	public void rx(){
		try{
			serialPort.addEventListener(this);
		}catch(TooManyListenersException e) {
		}
		serialPort.notifyOnDataAvailable(true);
		
		try {
			serialPort.setSerialPortParams(9600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
		}catch(UnsupportedCommOperationException e1){
		}
	}

	
	public void run(){
		System.out.println("\nReceive Here is ComRxTx Thread");
		try {
			//this.init(targetPort);
				this.init("COM2");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				this.init("");  
			}catch (Exception _e) {}
		}
	}
	
	public void serialEvent(SerialPortEvent event){
		switch(event.getEventType()){
		case SerialPortEvent.BI:
		case SerialPortEvent.OE:
		case SerialPortEvent.FE:
		case SerialPortEvent.PE:
		case SerialPortEvent.CD:
		case SerialPortEvent.CTS:
		case SerialPortEvent.DSR:
			
		case SerialPortEvent.RI:
		case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
			break;
		case SerialPortEvent.DATA_AVAILABLE:
			
			byte[] readBuffer = new byte[1024];
			int numBytes = -1;
			try{
				while(inputStream.available()>0){
					numBytes = inputStream.read(readBuffer);
				}
				System.out.println("\n[size]"+numBytes);
				String tmpMsg = new String(readBuffer, 0, numBytes);
				System.out.print(tmpMsg);
				saveReceivePacket(tmpMsg);
				
				String check = tmpMsg.substring(0,1) ;
				System.out.println("정혁이의 테스트 : "+tmpMsg);
				System.out.println("$를 체크해봅니다 : "+check);
				String tmpMsg_$ = tmpMsg.substring(1) ;
				System.out.println("$뺀 신호 : "+tmpMsg_$);
				
				StringBuilder strBuf = new StringBuilder(tmpMsg);
				
				if(check.equals("$")){

					StringTokenizer st = new StringTokenizer(tmpMsg_$, "/");
					String [] array = new String[st.countTokens()];                        //토큰의 갯수는 몇개인가?
		  			int i = 0;
		 			while(st.hasMoreElements()){
		   				array[i++] = st.nextToken();
		  			}
		 			for(i=0; i<5; i++){
						System.out.println(i+"배열값 체크해보기 "+array[i]);
						//System.out.print(array[i]);					
					}
					TabLjhVO vo = new TabLjhVO();
					vo.node_id = array[0];
					//vo.패킷타입 = array[1]; 
					vo.sensor_type = array[2];
					vo.value = array[3];
					//seq_DB++;
					
					vo.seq = String.valueOf(seq_DB++);
					vo.time = array[4];
					
					System.out.println("마지막 테스트입니다."+vo.node_id+" / "+vo.sensor_type+" / "+vo.value+" / "+vo.seq+" / "+vo.time); 
					
					try{
						TabLjhDao.insert(vo);
					}catch(Exception e){
						e.printStackTrace(); 
					}
					
				}
				  
			}catch (IOException e){}
			
		} 
	}

	
	public static void saveReceivePacket(String tmpMsg){
		if(maxMsgBufferSize<saveRecivePacket.length()){
			saveRecivePacket = "";
		}
		saveRecivePacket +=tmpMsg;
	} 
		
	public static String getReciveMsg(){
		return saveRecivePacket;
	}

}


