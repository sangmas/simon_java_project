package slowSerial.LJH;



import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.TooManyListenersException;







import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import gnu.io.UnsupportedCommOperationException;

public class slowRxTx_Send implements Runnable, SerialPortEventListener {
	static CommPortIdentifier portId;
	static Enumeration<CommPortIdentifier> portList;
	static CommPort commPort;
	static InputStream inputStream;
	static OutputStream outputStream;
	static SerialPort serialPort;
	Thread readThread;//////////////////////////////////////////////////////////////////////////주석처리해도 잘됨
	static String targetPort = "COM2";                                     //기본시리얼포트
	static private int maxMsgBufferSize = 50;                              //메시지크기의 2배 -1
	static String saveRecivePacket = "";                                   //수신 메시지 패킷
	static String switchCommand = "";
	
	public void setSwitchCommand(String cmm){
		switchCommand = cmm;	
		System.out.println("Send 자바테스트입니다"+switchCommand);
	}

	
	
	public void init(String targetPort1) throws Exception{
		System.out.println(" targetPort is : "+targetPort1);
		CommPortIdentifier portIdentifier/*포트 식별자*/ = CommPortIdentifier.getPortIdentifier( targetPort1 );
		int timeout = 2000;
		if( portIdentifier.isCurrentlyOwned() ) {                                                              //포트가 사용중일때
			System.out.println( "Error: Port is currently in use" );  
			//commPort = portIdentifier.open(this.getClass().getName(), timeout );
		} else {                                                                                                       //포트가 사용중이지 않을 때 포트연결생성
			commPort/*연결된 포트*/ = portIdentifier/*포트연결생성*/.open(this.getClass().getName(), timeout );
		}

		
		System.out.println( "여기까지 됐나요??????" );
			if(commPort instanceof SerialPort ) {   /*연결된 포트가 시리얼 포트로 형변환 가능하다면*/
				serialPort = (SerialPort)commPort;        //형변환시켜서 위에 스태틱으로 설정한 serialPort에 넣는다
				serialPort.setSerialPortParams( 9600, //속도설정
						SerialPort.DATABITS_8,
						SerialPort.STOPBITS_1,
						SerialPort.PARITY_NONE );

				inputStream = serialPort.getInputStream();  // 위의 스태틱으로 설정한 인풋스트림과 아웃풋스트림에 시리얼통신의 getInputStream과 getOutputStream을 넣는다
				outputStream = serialPort.getOutputStream();
                                         //수신
				this.tx(switchCommand);                       //송신
			} else {
				System.out.println( "Error: Only serial ports are handled by this example." );
			}
			
			
		}
	

	public void tx(String messageString){
			//String messageString = "Hello, world! \n";
		System.out.println("Send 자바테스트입니다:tx()잘하고 있니????"+messageString);
			try {
				if(!("".equals(messageString) || "NOcommand".equals(messageString))){
				//outputStream.write(messageString.getBytes());
					//outputStream.write("$60:1:3:50");
					String[] check = new String[messageString.length()];
					
					
					for(int i=0; i<messageString.length(); i++){
					check[i] = messageString.substring(i,i+1);
					System.out.println("배열의 값을 체크해봅니다 : "+check[i]);
					}
					char key[];
					for(int i=0; i<messageString.length(); i++){
						key=check[i].toCharArray();  
						outputStream.write(key[0]);
					}
					outputStream.write('\r');
				}
				//else if(messageString.equals("inquiry")){}
				else if(messageString.equals("t")){
				//outputStream.write(messageString.getBytes());
					outputStream.write('0');

					outputStream.write('\r');
				}
				else{
					outputStream.write(messageString.getBytes());
					
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Sending message : '"+messageString+"'");
	}
	
	public void run(){
		System.out.println("\nSend Here is ComRxTx Thread");
		try {
			//this.init(targetPort);
				this.init("COM3");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				this.init("");  
			}catch (Exception _e) {}
		}
	}
	
	public void serialEvent(SerialPortEvent  event){
		switch(event.getEventType()){
		case SerialPortEvent.BI:
		case SerialPortEvent.OE:
		case SerialPortEvent.FE:
		case SerialPortEvent.PE: 
		case SerialPortEvent.CD:
		case SerialPortEvent.CTS:
		case SerialPortEvent.DSR: 
			
		case SerialPortEvent.RI:
		case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
			break;
		case SerialPortEvent.DATA_AVAILABLE:
			
			byte[] readBuffer = new byte[1024];
			int numBytes = -1;
			try{
				while(inputStream.available()>0){
					numBytes = inputStream.read(readBuffer);
				}
				System.out.print("\n[size]"+numBytes);
				String tmpMsg = new String(readBuffer, 0, numBytes);
				System.out.print(tmpMsg);
				saveRecivePacket(tmpMsg);
			}catch (IOException e){}
			
		} 
	}

	
	public static void saveRecivePacket(String tmpMsg){
		if(maxMsgBufferSize<saveRecivePacket.length()){
			saveRecivePacket = "";
		}
		saveRecivePacket +=tmpMsg;
	} 
		
	public static String getReciveMsg(){
		return saveRecivePacket;
	}

}


