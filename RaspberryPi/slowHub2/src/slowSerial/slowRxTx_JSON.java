package slowSerial;






import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import gnu.io.UnsupportedCommOperationException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.TooManyListenersException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class slowRxTx_JSON implements Runnable, SerialPortEventListener {
	static CommPortIdentifier portId;
	static Enumeration<CommPortIdentifier> portList;
	static CommPort commPort;
	static InputStream inputStream;
	static OutputStream outputStream;
	static SerialPort serialPort;
	Thread readThread;//////////////////////////////////////////////////////////////////////////주석처리해도 잘됨
	static String targetPort = "COM7";                                     //기본시리얼포트
	//static String targetPort = "COM7";                                     //기본시리얼포트
	static private int maxMsgBufferSize = 50;                              //메시지크기의 2배 -1
	static String saveRecivePacket = "";                                   //수신 메시지 패킷
	static String switchCommand = "";
	public String DemonStr ="";
	
	public void setSwitchCommand(String cmm){
		switchCommand = cmm;		
	}
	
	public void init(String targetPort1) throws Exception{
		System.out.println(" targetPort is : "+targetPort1);
		CommPortIdentifier portIdentifier/*포트 식별자*/ = CommPortIdentifier.getPortIdentifier( targetPort1 );
		int timeout = 2000;
		if( portIdentifier.isCurrentlyOwned() ) {                                                              //포트가 사용중일때
			System.out.println( "Error: Port is currently in use" );  
			//commPort = portIdentifier.open(this.getClass().getName(), timeout );
		} else {                                                                                                       //포트가 사용중이지 않을 때 포트연결생성
			commPort/*연결된 포트*/ = portIdentifier/*포트연결생성*/.open(this.getClass().getName(), timeout );
		}

			if(commPort instanceof SerialPort ) {   /*연결된 포트가 시리얼 포트로 형변환 가능하다면*/
				serialPort = (SerialPort)commPort;        //형변환시켜서 위에 스태틱으로 설정한 serialPort에 넣는다
				serialPort.setSerialPortParams( 9600, //속도설정
						SerialPort.DATABITS_8,
						SerialPort.STOPBITS_1,
						SerialPort.PARITY_NONE );

				inputStream = serialPort.getInputStream();  // 위의 스태틱으로 설정한 인풋스트림과 아웃풋스트림에 시리얼통신의 getInputStream과 getOutputStream을 넣는다
				outputStream = serialPort.getOutputStream();
				this.rx();                                           //수신
				this.tx(switchCommand);                       //송신
			} else {
				System.out.println( "Error: Only serial ports are handled by this example." );
			}
		}
		
	public void rx(){
		try{
			serialPort.addEventListener(this);
		}catch(TooManyListenersException e) {
		}
		serialPort.notifyOnDataAvailable(true);
		try {
			serialPort.setSerialPortParams(9600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
		}catch(UnsupportedCommOperationException e1){
		}
	}
	public void tx(String messageString){
		//String messageString = "Hello, world! \n";
			try {
				if(messageString.equals("k")){
				//outputStream.write(messageString.getBytes());
					outputStream.write('k');
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Sending message : '"+messageString+"'");
	}
	
	public void run(){
		System.out.println("\nHere is ComRxTx Thread");
		try {
			//this.init(targetPort);
				this.init("COM7");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				this.init("COM7");  
			}catch (Exception _e) {}
		}
	}
	
	public void serialEvent(SerialPortEvent event){
		switch(event.getEventType()){
		case SerialPortEvent.BI:
		case SerialPortEvent.OE:
		case SerialPortEvent.FE:
		case SerialPortEvent.PE:
		case SerialPortEvent.CD:
		case SerialPortEvent.CTS:
		case SerialPortEvent.DSR:			
		case SerialPortEvent.RI:
		case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
			break;
		case SerialPortEvent.DATA_AVAILABLE:
			
			
			
			byte[] readBuffer = new byte[1024];
			int numBytes = -1;
			try{
				while(inputStream.available()>100){
					numBytes = inputStream.read(readBuffer);
				}

				String tmpMsg = new String(readBuffer, 0, numBytes);
				saveRecivePacket(tmpMsg);
		
				StringBuffer sb = new StringBuffer();
				DemonStr=sb.append(DemonStr).append(tmpMsg).toString();//
				

				//String strVO = getReciveMsg();
				String strVO = DemonStr;
				
				System.out.println("Start!!test DemonStr : "+strVO);
				System.out.println("");
				
				//문자열의 시작이 'S'가 아니면 중간에서 끊긴 형태일테니까 EEEEEEEEEE까지의 문자열 다 버리고 
				//데이터 부분의 문자열에서부터 "S"붙여서 거기서부터 더하게 한다.
				if(!(strVO.startsWith("S"))){
					String[] sensorVoArr = strVO.split("SSSSSSSSSS");
					System.out.println("&&&&&test sensorVoArr[1] : "+sensorVoArr[1]);
					DemonStr="S"+sensorVoArr[1];
					strVO="";
					tmpMsg="";
				}
				
				//DemonStr에서 EEEEEEEEEE 이전의 데이터는 잘라서 TO_JSON(데이터)를 호출.
				//그후에는 데이터 초기화해서 문자열을 새로 받는 형태로 진행한다. 
				if(strVO.length()>240){

					String[] sensorVoArr = strVO.split("E");
					System.out.println("*****test sensorVoArr[0] : "+sensorVoArr[0]);
					TO_JSON(sensorVoArr[0]);
					DemonStr="";
					tmpMsg="";
					strVO="";
				}
				
				// 너무 많은 문자열 받았을 시 초기화
				if(DemonStr.length()>500){
					DemonStr="";
					tmpMsg="";
					strVO="";
				}
			}catch (IOException e){}
			
		} 
	}

	public static void TO_JSON(String strVO){
		
		strVO=strVO.replace("S", "");
		strVO=strVO.replace("E", "");
		strVO=strVO.replace("\n", "");
		strVO=strVO.replace(" ", "");
		//strVO=strVO.replace("^", "\"");
		
		String[] DataArr = new String[30];
		for(int i=0;i<30;i++){					
			DataArr[i]=new String();
			DataArr[i]="-1";
		}				
		
		Object obj = JSONValue.parse(strVO);
		JSONObject JSONobj=(JSONObject)obj;
		
		JSONArray SM_array = (JSONArray) JSONobj.get("sensing_metadata");
		JSONObject sensing_metadata_0 = (JSONObject) SM_array.get(0);
		
		String A = (String)sensing_metadata_0.get("node_id");
		String B = (String)sensing_metadata_0.get("RxTx");
		
		ArrayList<JSONObject> SD_List = new ArrayList<JSONObject>();
		JSONArray SD_array = (JSONArray) JSONobj.get("sensor_data");
		
		for(int i=0;i<SD_array.size();i++){
			SD_List.add((JSONObject) SD_array.get(i));
		}
		
		for(int i=0;i<SD_List.size();i++){					
			DataArr[Integer.parseInt((String)SD_List.get(i).get("sensor_type"))] = (String)SD_List.get(i).get("value");				
		}
		
		saveSensingData_new54(A, B, DataArr);
	}
	
	
	public static void saveSensingData_new54(String A, String B, String[] C){
		//System.out.println("센싱정보 기록 : "+i);
		//if(sensorVoArr.length>2){
		
		//http://203.230.100.54:8080/slowHub/ksh_ui_ver3/proc/deviceExec.jsp?pMode=insert&node_name=Air_1&co2=7&=7&temp=7&humid=7&lpg=7&voc=7&cds=7&uv=7&dust=7&ozone=7&communication_type=ZigBee
			URI uri = null;
			
			try {
					uri = new URIBuilder()
					.setScheme("http")
					.setHost("203.230.100.54:8080")
					.setPath("/slowHub/ksh_ui_ver3/proc/deviceExec.jsp")
					.setParameter("pMode", "insert")
					.setParameter("node_name",A)
					//co2=7&=7&temp=7&humid=7&lpg=7&voc=7&cds=7&uv=7&dust=7&ozone=7&communication_type=ZigBee
					.setParameter("co2",C[3])
					.setParameter("temp",C[4])
					.setParameter("humid",C[5])
					.setParameter("lpg",C[6])
					.setParameter("voc",C[7])
					.setParameter("cds",C[8])
					.setParameter("uv",C[9])
					.setParameter("dust",C[10])
					.setParameter("ozone",C[11])
					.setParameter("communication_type",C[12])
					.build();
				} catch (URISyntaxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

		
			CloseableHttpClient httpclient = HttpClients.createDefault(); 
			HttpGet httpget = new HttpGet(uri);
			try {
				CloseableHttpResponse response = httpclient.execute(httpget);
				try {
				} finally {
				    response.close();
				}
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
			} catch (HttpHostConnectException e) {
				// TODO Auto-generated catch block
				System.out.println("웹서버가 응답하지 않습니다.");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(httpget.getURI());			
	}
	
	public static void saveSensingData(String A, String B, String C){
		//System.out.println("센싱정보 기록 : "+i);
		//if(sensorVoArr.length>2){
			URI uri = null;
			try {
				uri = new URIBuilder()
				.setScheme("http")
				.setHost("203.230.100.54:8080")
				.setPath("/slowHub/test_ljh/proc/tabExec.jsp")
				.setParameter("pMode", "insert")
				//.setParameter("device_id", "ED00000001")
				//.setParameter("sensor_id", "01")
				//.setParameter("sensor_value", "30.2")
				.setParameter("nodeid",A)
				.setParameter("sensortype",B)
				.setParameter("value",C)
				.build();
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			CloseableHttpClient httpclient = HttpClients.createDefault(); 
			HttpGet httpget = new HttpGet(uri);
			try {
				CloseableHttpResponse response = httpclient.execute(httpget);
				try {
				} finally {
				    response.close();
				}
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
			} catch (HttpHostConnectException e) {
				// TODO Auto-generated catch block
				System.out.println("웹서버가 응답하지 않습니다.");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(httpget.getURI());			
	}
	

	
	public static void saveRecivePacket(String tmpMsg){
		if(maxMsgBufferSize<saveRecivePacket.length()){
			saveRecivePacket = "";
		}
		saveRecivePacket +=tmpMsg;
	}
		
	public static String getReciveMsg(){
		return saveRecivePacket;
	}
}


