package slowHub.OCS_db;

import java.sql.SQLException;
import java.util.List;

import maya.util.PublicMaxSeq;
import slowHub.testTab.TabVO;

import com.ibatis.sqlmap.client.SqlMapClient;

public class AtmosphereDao{

	static SqlMapClient sqlMap = mapper.CommIbatisClient.getSqlMapInstance();
	
	public static TabAtmosphereVO getRow(String node_seq) throws SQLException {
		return (TabAtmosphereVO)sqlMap.queryForObject("atmosphere.getTabAtmosphereRow",node_seq);//??
	}
	
	public static int insert(TabAtmosphereVO vo) throws Exception {
		//eventVO.event_seq =sqlMap.queryForObject("event.getMaxEvent").toString(); 
		vo.node_seq = Integer.toString(PublicMaxSeq.getNext("atmosphere.getMaxTab"));
		
		return  sqlMap.update("atmosphere.insertTab", vo); //sql 파라미터 받아옴
								//앞에 testksh는 sql 네임스페이스				
	}
	
	public static int update(TabAtmosphereVO vo) throws Exception {
		//eventVO.event_seq =sqlMap.queryForObject("event.getMaxEvent").toString(); 
		return  sqlMap.update("atmosphere.updateTab", vo);
	}
	
	public static int delete(String seq) throws Exception {
		//eventVO.event_seq =sqlMap.queryForObject("event.getMaxEvent").toString(); 
		return  sqlMap.delete("atmosphere.deleteTab", seq);
	}
	
	public static List<TabAtmosphereVO> getList() throws SQLException {
		return sqlMap.queryForList("atmosphere.getTabList");
	}//method overiding
	
	public static List<TabAtmosphereVO> getList(TabAtmosphereVO vo, int startIdx, int ipp) throws SQLException {
		return sqlMap.queryForList("atmosphere.getTabList", vo, startIdx-1, ipp);
	}
	
	public  static int getListCnt(TabAtmosphereVO vo) throws SQLException {
		return (Integer)sqlMap.queryForObject("atmosphere.getTabListCnt", vo);
	}
	
}
