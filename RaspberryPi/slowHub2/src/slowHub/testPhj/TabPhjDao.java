package slowHub.testPhj;

import java.sql.SQLException;

import com.ibatis.sqlmap.client.SqlMapClient;

public class TabPhjDao{

	static SqlMapClient sqlMap = mapper.CommIbatisClient.getSqlMapInstance();
	
	public static TabPhjVO getRow(String seq) throws SQLException {
		return (TabPhjVO)sqlMap.queryForObject("tabphj.getTabRow",seq);
	}
	  
	public static int insert(TabPhjVO vo) throws Exception {
		//eventVO.event_seq =sqlMap.queryForObject("event.getMaxEvent").toString(); 
		return  sqlMap.update("tabphj.insertTab", vo);
	}
	
}
