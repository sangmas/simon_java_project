package slowHub.user;

import java.sql.SQLException;
import java.util.List;

import maya.util.PublicMaxSeq;

import com.ibatis.sqlmap.client.SqlMapClient;

public class userDao{

	static SqlMapClient sqlMap = mapper.CommIbatisClient.getSqlMapInstance();
	
	public static userVO getRow(String id) throws SQLException {
		return (userVO)sqlMap.queryForObject("user.getTabuserRow",id);//??
	}
	
	public static int insert(userVO vo) throws Exception {
		//eventVO.event_seq =sqlMap.queryForObject("event.getMaxEvent").toString(); 
		//vo.id = Integer.toString(PublicMaxSeq.getNext("user.getMaxTab"));
		
		return  sqlMap.update("user.insertTab", vo); //sql 파라미터 받아옴
								//앞에 testksh는 sql 네임스페이스				
	}
	
	public static int update(userVO vo) throws Exception {
		//eventVO.event_seq =sqlMap.queryForObject("event.getMaxEvent").toString(); 
		return  sqlMap.update("user.updateTab", vo);
	}
	
	public static int delete(String id) throws Exception {
		//eventVO.event_seq =sqlMap.queryForObject("event.getMaxEvent").toString(); 
		return  sqlMap.delete("user.deleteTab", id);
	}
	
	public static List<userVO> getList() throws SQLException {
		return sqlMap.queryForList("user.getTabList");
	}//method overiding
	
	public static List<userVO> getList(userVO vo, int startIdx, int ipp) throws SQLException {
		return sqlMap.queryForList("user.getTabList", vo, startIdx-1, ipp);
	}
	
	public  static int getListCnt(userVO vo) throws SQLException {
		return (Integer)sqlMap.queryForObject("user.getTabListCnt", vo);
	}
	
}
