package slowHub.sensor;

import java.sql.SQLException;
import java.util.List;

import maya.util.PublicMaxSeq;

import com.ibatis.sqlmap.client.SqlMapClient;

public class sensorDao{

	static SqlMapClient sqlMap = mapper.CommIbatisClient.getSqlMapInstance();
	
	public static sensorVO getRow(String sensor_seq) throws SQLException {
		return (sensorVO)sqlMap.queryForObject("sensor.getTabsensorRow",sensor_seq);//??
	}
	
	public static int insert(sensorVO vo) throws Exception {
		//eventVO.event_seq =sqlMap.queryForObject("event.getMaxEvent").toString(); 
		vo.sensor_seq = PublicMaxSeq.getNext("sensor.getMaxTab");
		//System.out.println("test!! : ");
		return  sqlMap.update("sensor.insertTab", vo); //sql 파라미터 받아옴
								//앞에 testksh는 sql 네임스페이스				
	}
	
	public static int update(sensorVO vo) throws Exception {
		//eventVO.event_seq =sqlMap.queryForObject("event.getMaxEvent").toString(); 
		return  sqlMap.update("sensor.updateTab", vo);
	}
	
	public static int delete(int sensor_seq) throws Exception {
		//eventVO.event_seq =sqlMap.queryForObject("event.getMaxEvent").toString(); 
		return  sqlMap.delete("sensor.deleteTab", sensor_seq);
	}
	
	public static List<sensorVO> getList() throws SQLException {
		return sqlMap.queryForList("sensor.getTabList");
	}//method overiding
	
	public static List<sensorVO> getList(sensorVO vo, int startIdx, int ipp) throws SQLException {
		return sqlMap.queryForList("sensor.getTabList", vo, startIdx-1, ipp);
	}
	
	public  static int getListCnt(sensorVO vo) throws SQLException {
		return (Integer)sqlMap.queryForObject("sensor.getTabListCnt", vo);
	}
	
}
