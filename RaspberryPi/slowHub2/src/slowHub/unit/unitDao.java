package slowHub.unit;

import java.sql.SQLException;
import java.util.List;

import maya.util.PublicMaxSeq;

import com.ibatis.sqlmap.client.SqlMapClient;

public class unitDao{

	static SqlMapClient sqlMap = mapper.CommIbatisClient.getSqlMapInstance();
	
	public static unitVO getRow(String unit_seq) throws SQLException {
		return (unitVO)sqlMap.queryForObject("unit.getTabunitRow",unit_seq);//??
	}
	
	public static int insert(unitVO vo) throws Exception {
		//eventVO.event_seq =sqlMap.queryForObject("event.getMaxEvent").toString(); 
		vo.unit_seq = PublicMaxSeq.getNext("unit.getMaxTab");
		System.out.println("test!! : ");
		return  sqlMap.update("unit.insertTab", vo); //sql 파라미터 받아옴
								//앞에 testksh는 sql 네임스페이스				
	}
	
	public static int update(unitVO vo) throws Exception {
		//eventVO.event_seq =sqlMap.queryForObject("event.getMaxEvent").toString(); 
		return  sqlMap.update("unit.updateTab", vo);
	}
	
	public static int delete(int unit_seq) throws Exception {
		//eventVO.event_seq =sqlMap.queryForObject("event.getMaxEvent").toString(); 
		return  sqlMap.delete("unit.deleteTab", unit_seq);
	}
	
	public static List<unitVO> getList() throws SQLException {
		return sqlMap.queryForList("unit.getTabList");
	}//method overiding
	
	public static List<unitVO> getList(unitVO vo, int startIdx, int ipp) throws SQLException {
		return sqlMap.queryForList("unit.getTabList", vo, startIdx-1, ipp);
	}
	
	public  static int getListCnt(unitVO vo) throws SQLException {
		return (Integer)sqlMap.queryForObject("unit.getTabListCnt", vo);
	}
	
}
