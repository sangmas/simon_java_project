package slowHub.atmosphere;
/***************************************************************************************
 * <pre>
 *  프로젝트 : 이벤트 플레이스(2013)
 *  소스 :  Article
 * -----------------------------------------------------------------------------------
 * VER      DATE       AUTHOR    DESCRIPTION
 * ---   ----------  ----------  -----------------------------------------------------
 * 1.0   2013.07.22    김영돈           최초 프로그램 작성    
 * -----------------------------------------------------------------------------------
 * Copyright(c) 2010  maya,  All rights reserved.
 ***************************************************************************************
  * </pre>
 */

public class TabAtmosphereVO  {
	public String node_seq		= "";
	public String node_name		= "";
	public String co2		= "";
	public String temp		= "";
	public String humid		= "";
	public String lpg		= "";
	public String voc		= "";
	public String cds		= "";
	public String uv		= "";
	public String dust		= "";
	public String ozone		= "";
	public String connect_dt		= "";
	public String communication_type		= "";	
} 

