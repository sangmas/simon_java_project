package slowHub.testDevice;
/***************************************************************************************
 * <pre>
 *  프로젝트 : 이벤트 플레이스(2013)
 *  소스 :  Article
 * -----------------------------------------------------------------------------------
 * VER      DATE       AUTHOR    DESCRIPTION
 * ---   ----------  ----------  -----------------------------------------------------
 * 1.0   2013.07.22    김영돈           최초 프로그램 작성    
 * -----------------------------------------------------------------------------------
 * Copyright(c) 2010  maya,  All rights reserved.
 ***************************************************************************************
  * </pre>
 */

public class TabDeviceVO  {
	public String unit_seq		= "";
	public String unit_name		= "";
	public String reg_dt		= "";
	public String connect_state		= "";
	public String connect_dt		= "";
	public String unit_identifier		= "";
	public String product_code		= "";
	public String compatible_device_code		= "";
	public String manufacture_company_name		= "";
	public String product_name		= "";
	public String communication_type		= "";
	public String cert_key		= "";
	public String external_open_flg		= "";
	public String install_postion_coord		= "";
	public String install_inout_type		= "";
	public String id		= "";
	public String external_cert_key		= "";	
} 

