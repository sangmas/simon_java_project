

-- 단말이 갖고 있는 센서 조회
SELECT * 
FROM  `slow_sensor` 
WHERE  `unit_seq` =  '9'
LIMIT 0 , 30

-- 단말 아이디가 ED00000001 인 01번째 센서 정보 조회
SELECT su.unit_name, ss . * 
FROM slow_sensor ss, slow_unit su
WHERE 1 =1
AND su.unit_seq = ss.unit_seq
AND product_code =  'ED00000001'
AND sensor_seq =1
LIMIT 0 , 30