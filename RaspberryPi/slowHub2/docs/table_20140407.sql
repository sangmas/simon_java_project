CREATE TABLE slow_unit
(
	unit_seq              INTEGER NULL,
	unit_name             VARCHAR(100) NULL,
	reg_dt                DATETIME NULL,
	connect_state         VARCHAR(20) NULL,
	connect_dt            DATETIME NULL,
	unit_identifier       VARCHAR(100) NULL,
	product_code          VARCHAR(100) NULL,
	compatible_device_code  VARCHAR(100) NULL,
	manufacture_company_name  VARCHAR(100) NULL,
	product_name          VARCHAR(100) NULL,
	communication_type    VARCHAR(20) NULL,
	cert_key              VARCHAR(100) NULL,
	external_open_flg     CHAR(1) NULL,
	install_postion_coord  VARCHAR(100) NULL,
	install_inout_type    VARCHAR(20) NULL,
	id                    VARCHAR(100) NULL,
	external_cert_key     VARCHAR(100) NULL
)
;


ALTER TABLE slow_unit
	ADD  PRIMARY KEY (unit_seq)
;


CREATE TABLE slow_sensor
(
	unit_seq              INTEGER NULL,
	sensor_seq            INTEGER NULL,
	sensor_type           VARCHAR(20) NULL,
	run_state             VARCHAR(20) NULL,
	external_open_flg     CHAR(1) NULL
)
;



ALTER TABLE slow_sensor
	ADD  PRIMARY KEY (sensor_seq)
;

CREATE TABLE slow_actuator
(
	actuator_seq          INTEGER NULL,
	unit_seq              INTEGER NULL,
	run_state             VARCHAR(20) NULL,
	external_open_flg     CHAR(1) NULL,
	actuator_type         VARCHAR(20) NULL
)
;



ALTER TABLE slow_actuator
	ADD  PRIMARY KEY (actuator_seq)
;


CREATE TABLE slow_sensing_value
(
	sensing_dt            DATETIME NULL,
	sensing_value         FLOAT NULL,
	sensor_seq            INTEGER NULL,
	seq                   INTEGER NULL
)
;



ALTER TABLE slow_sensing_value
	ADD  PRIMARY KEY (seq)
;


CREATE TABLE slow_action
(
	id                    VARCHAR(100) NULL,
	actuator_seq          INTEGER NULL,
	sensor_seq            INTEGER NULL,
	action_dt             DATETIME NULL,
	execution_result      VARCHAR(100) NULL,
	reply_dt              DATETIME NULL,
	reservation_dt        DATETIME NULL,
	action_seq            INTEGER NULL
)
;



ALTER TABLE slow_action
	ADD  PRIMARY KEY (action_seq)
;

