package KshTest;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

/**
 * This example code demonstrates how to setup a listener
 * for GPIO pin state changes on the Raspberry Pi.  
 * 
 * @author Robert Savage
 */
public class KshTest {
	
	//public static int dht[] = {0}; unsigned char .. counter, checksum .. for..
	
	public static void main(String args[]) throws InterruptedException {
				
		int rtCode;
		
		System.out.println("ksh DHT22 test ... started.");
		//PinCreate kind = new PinCreate();
		
        StartSignalDHT startDHT = new StartSignalDHT();
        startDHT.SignalSend();  
        ReadDHT readDHT = new ReadDHT();        
        rtCode = readDHT.ReadDHT22();
        
        switch(rtCode)
        {
        	case 1:
        		System.out.println("time out 1"); // doesn't use
        		break;
        	case 2:
        		System.out.println("time out 2"); // doesn't use
        		break;
        	case 3:
        		System.out.println("time out 3");
        		break;
        	case 4:
        		System.out.println("time out 4");
        		break;
        	case 5:
        		System.out.println("time out 5");
        		break;
        	case 9:
        		System.out.println("check sum error");
        		break;
        	case 0:
        		System.out.println("@@ RH : "+readDHT.dht_val[0]+"."+readDHT.dht_val[1]+
        				" / $$ T :"+readDHT.dht_val[2]+"."+readDHT.dht_val[3]);
        		break;
        }
        Thread.sleep(1000);
        /*
        for (;;) {
            Thread.sleep(500);
        }        
          */     
    }
}


