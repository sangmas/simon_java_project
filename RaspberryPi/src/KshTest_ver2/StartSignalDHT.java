package KshTest_ver2;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

import java.util.concurrent.TimeUnit;

public class StartSignalDHT {
	static public PinCreate kind;
	public PinState state;
	int cnt;
	
	public StartSignalDHT() throws InterruptedException{
			
		kind = new PinCreate();
		kind.PinCreateOutput(); //because of 1 wire communication, have to make output pin.. first
				
		//kind.pinoutput.high();
		Thread.sleep(18);		
		//kind.pinoutput.low();
		Thread.sleep(1);
		
		//System.out.println("check getState function.. " + kind.pininput.getState());
		
		//cnt = 0;
		
		kind.PinCreateInput(); // change mode for Input
	}
	
	public int SignalSend() {
		while(kind.pinoutput.getState() == PinState.HIGH){
			try {
				TimeUnit.MICROSECONDS.sleep(1);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			cnt++;
			if(cnt >= 255)
				return 1; //time out 1
		}
		
		
		cnt = 0;		
		while(kind.pinoutput.getState() == PinState.LOW){
			try {
				TimeUnit.MICROSECONDS.sleep(1);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			cnt++;
			if(cnt >= 255)
				return 2; //time out 2
		}
		
		cnt = 0;		
		while(kind.pinoutput.getState() == PinState.HIGH){
			try {
				TimeUnit.MICROSECONDS.sleep(1);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			cnt++;
			if(cnt >= 255)
				return 3; //time out 3
		}
		
		
		return 0; // pass
	     
	}
	
}
