package swap;

import java.util.Scanner;

public class TestDrive {
	static int first;
	static int second;
		
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		System.out.println("first number");
		first = scanner.nextInt();
		System.out.println("second number");
		second = scanner.nextInt();
		
		swap();		
		System.out.println("* result = "+first+" "+second);
	}
	
	private static void swap(){
		int temp;
		
		temp = first;
		first = second;
		second = temp;	
	}

}
