<%-- 프런트 컨트롤러 적용 - 링크에 .do 붙임 --%>
<%@ page 
  language="java" 
  contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
  "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>상 등록</title>
</head>
<body>
<jsp:include page="/Header.jsp"/>
<h1>상 등록</h1>
<form action='add.do' method='post'>
수상 등급: <input type='text' name='awardClass'><br>
수상자 소속: <input type='text' name='affiliation'><br>
수상명: <input type='text' name='awardName'><br>
수상 내용: <input type='text' name='awardContent'><br>
수상 날짜: <input type='text' name='receivedDate'><br>
수여 기관: <input type='text' name='organization'><br>

<input type='submit' value='추가'>
<input type='reset' value='취소'>
</form>
<jsp:include page="/Tail.jsp"/>
</body>
</html>
