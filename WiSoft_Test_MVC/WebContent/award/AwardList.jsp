<%-- 2. 프런트 컨트롤러 적용 - 링크에 .do 붙임 --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
  "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>수상 관련 목록</title>
</head>
<body>
	<jsp:include page="/Header.jsp" />

	<h1>수상 관련 목록</h1>
	<p>
		<a href='add.do'>수상 실적 추가하기</a>
	</p>

	<table border="1">
		<tr>
			<th>index</th>
			<th>상이름</th>
			<th>수상 등급</th>
			<th>수상 내용</th>
			<th>수상 날짜</th>
			<th>소속</th>
			<th>수여 기관</th>
			<th>삭제</th>
		</tr>
		<c:forEach var="award" items="${awards}">
			<tr>
				<th>${award.id}</th>
				<th><a href='update.do?id=${award.id}'>${award.awardName}</a></th>
				<th>${award.awardClass}</th>
				<th>${award.awardContent}</th>
				<th>${award.receivedDate}</th>
				<th>${award.affiliation}</th>				
				<th>${award.organization}</th>				
				<th><a href='delete.do?id=${award.id}'>[삭제]</a></th>
			</tr>

		</c:forEach>

	</table>

	<jsp:include page="/Tail.jsp" />
</body>
</html>