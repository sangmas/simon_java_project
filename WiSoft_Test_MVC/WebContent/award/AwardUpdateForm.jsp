<%-- 프런트 컨트롤러 적용 - 링크에 .do 붙임 --%>
<%@ page 
  language="java" 
  contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
  "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>회원정보</title>
</head>
<body>
<h1>회원정보</h1>
<form action='update.do' method='post'>
번호: <input type='text' name='id' value='${award.id}' readonly><br>
수상 등급: <input type='text' name='awardClass' value='${award.awardClass}'><br>
수상자 소속: <input type='text' name='affiliation' value='${award.affiliation}'><br>
수상명: <input type='text' name='awardName' value='${award.awardName}'><br>
수상 내용: <input type='text' name='awardContent' value='${award.awardContent}'><br>
수여 기관: <input type='text' name='organization' value='${award.organization}'><br>

수상 날짜: ${award.receivedDate}<br>
<input type='submit' value='저장'>
<input type='button' value='삭제' 
  onclick='location.href="delete.do?id=${award.id}";'>
<input type='button' value='취소' onclick='location.href="list.do"'>
</form>
</body>
</html>