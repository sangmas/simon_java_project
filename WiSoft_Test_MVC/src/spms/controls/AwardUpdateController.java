package spms.controls;

import java.util.Map;

import spms.annotation.Component;
import spms.bind.DataBinding;
import spms.dao.AwardDao;
import spms.vo.Award;

// Annotation 적용
@Component("/award/update.do")
public class AwardUpdateController implements Controller, DataBinding {
	AwardDao awardDao;

	public AwardUpdateController setAwardDao(AwardDao awardDao) {
		this.awardDao = awardDao;
		return this;
	}

	public Object[] getDataBinders() {
		return new Object[] { "id", Integer.class, "award", spms.vo.Award.class };
	}

	@Override
	public String execute(Map<String, Object> model) throws Exception {
		Award award = (Award) model.get("award");

		if (award.getAwardName() == null) {
			Integer id = (Integer) model.get("id");
			Award detailInfo = awardDao.selectOne(id);
			model.put("award", detailInfo);
			return "/award/AwardUpdateForm.jsp";

		} else {
			awardDao.update(award);
			return "redirect:list.do";
		}
	}
}
