package spms.controls;

import java.util.Map;

import spms.annotation.Component;
import spms.dao.AwardDao;

@Component("/award/list.do")
public class AwardListController implements Controller {
	AwardDao awardDao;

	public AwardListController setAwardDao(AwardDao awardDao) {
		System.out.println("setAwardDao @@@" + awardDao);
		this.awardDao = awardDao;
		return this;
	}

	@Override
	public String execute(Map<String, Object> model) throws Exception {
		model.put("awards", awardDao.selectList());
		return "/award/AwardList.jsp";
	}
}
