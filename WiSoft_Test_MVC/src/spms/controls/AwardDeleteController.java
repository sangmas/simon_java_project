package spms.controls;

import java.util.Map;

import spms.annotation.Component;
import spms.bind.DataBinding;
import spms.dao.AwardDao;

// Annotation 적용
@Component("/award/delete.do")
public class AwardDeleteController implements Controller, DataBinding {
	AwardDao awardDao;

	public AwardDeleteController setAwardDao(AwardDao awardDao) {
		this.awardDao = awardDao;
		return this;
	}

	public Object[] getDataBinders() {
		return new Object[] { "id", Integer.class };
	}

	@Override
	public String execute(Map<String, Object> model) throws Exception {
		Integer id = (Integer) model.get("id");
		awardDao.delete(id);

		return "redirect:list.do";
	}
}
