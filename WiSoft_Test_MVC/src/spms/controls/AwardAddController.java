package spms.controls;

import java.util.Map;

import spms.annotation.Component;
import spms.bind.DataBinding;
import spms.dao.AwardDao;
import spms.vo.Award;

// Annotation 적용
@Component("/award/add.do")
public class AwardAddController implements Controller, DataBinding {
	AwardDao awardDao;

	public AwardAddController setAwardDao(AwardDao awardDao) {
		this.awardDao = awardDao;
		return this;
	}

	public Object[] getDataBinders() {
		return new Object[] { "award", spms.vo.Award.class };
	}

	@Override
	public String execute(Map<String, Object> model) throws Exception {
		Award award = (Award) model.get("award");
		if (award.getAwardName() == null) { // 입력폼을 요청할 때
			return "/award/AwardForm.jsp";
		} else { // 회원 등록을 요청할 때
			awardDao.insert(award);
			return "redirect:list.do";
		}
	}
}
