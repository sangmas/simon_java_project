package spms.dao;

// AwardDao Interface define
import java.util.List;

import spms.vo.Award;

public interface AwardDao {
	List<Award> selectList() throws Exception;

	int insert(Award award) throws Exception;

	int delete(int id) throws Exception;

	Award selectOne(int id) throws Exception;

	int update(Award award) throws Exception;

}
