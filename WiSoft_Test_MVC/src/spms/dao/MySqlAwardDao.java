package spms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import spms.annotation.Component;
import spms.vo.Award;

@Component("awardDao")
public class MySqlAwardDao implements AwardDao {
	DataSource ds;

	public void setDataSource(DataSource ds) {
		this.ds = ds;
	}

	@Override
	public List<Award> selectList() throws Exception {
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			connection = ds.getConnection();
			stmt = connection.createStatement();
			rs = stmt
					.executeQuery("SELECT A_ID, A_CLASS, A_AFFILIATION, A_NAME, A_CONTENT, A_RECEIVED_DATE, A_ORGANIZATION"
							+ " FROM AWARD" + " ORDER BY A_ID ASC");

			ArrayList<Award> awards = new ArrayList<>();

			while (rs.next()) {
				awards.add(new Award(rs.getInt("A_ID"),
						rs.getString("A_CLASS"), rs.getString("A_AFFILIATION"),
						rs.getString("A_NAME"), rs.getString("A_CONTENT"), rs
								.getDate("A_RECEIVED_DATE"), rs
								.getString("A_ORGANIZATION")));

			}

			return awards;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (Exception e) {
			}
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (connection != null)
					connection.close();
			} catch (Exception e) {
			}
		}

	}

	@Override
	public int insert(Award award) throws Exception {
		Connection connection = null;
		PreparedStatement stmt = null;

		try {
			connection = ds.getConnection();
			stmt = connection
					.prepareStatement("INSERT INTO AWARD(a_class, a_affiliation, a_name, a_content, a_received_date, a_organization)"
							+ " VALUES (?,?,?,?,NOW(),?)");
			stmt.setString(1, award.getAwardClass());
			stmt.setString(2, award.getAffiliation());
			stmt.setString(3, award.getAwardName());
			stmt.setString(4, award.getAwardContent());
			stmt.setString(5, award.getOrganization());

			return stmt.executeUpdate();

		} catch (Exception e) {
			throw e;

		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (connection != null)
					connection.close();
			} catch (Exception e) {
			}
		}
	}

	@Override
	public int delete(int id) throws Exception {
		Connection connection = null;
		Statement stmt = null;

		try {
			connection = ds.getConnection();
			stmt = connection.createStatement();
			return stmt.executeUpdate("DELETE FROM AWARD WHERE a_id=" + id);

		} catch (Exception e) {
			throw e;

		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (connection != null)
					connection.close();
			} catch (Exception e) {
			}
		}
	}

	@Override
	public Award selectOne(int id) throws Exception {
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			connection = ds.getConnection();
			stmt = connection.createStatement();
			rs = stmt
					.executeQuery("SELECT a_id,a_class,a_affiliation,a_name, a_content, a_received_date, a_organization FROM AWARD"
							+ " WHERE a_id=" + id);
			if (rs.next()) {
				return new Award(rs.getInt("A_ID"), rs.getString("A_CLASS"),
						rs.getString("A_AFFILIATION"), rs.getString("A_NAME"),
						rs.getString("A_CONTENT"),
						rs.getDate("A_RECEIVED_DATE"),
						rs.getString("A_ORGANIZATION"));

			} else {
				throw new Exception("해당 번호의 회원을 찾을 수 없습니다.");
			}

		} catch (Exception e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (Exception e) {
			}
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (connection != null)
					connection.close();
			} catch (Exception e) {
			}
		}
	}

	@Override
	public int update(Award award) throws Exception {
		Connection connection = null;
		PreparedStatement stmt = null;
		try {
			connection = ds.getConnection();
			stmt = connection
					.prepareStatement("UPDATE AWARD SET a_class=?, a_affiliation=?, a_name=?, a_content=?, a_organization=?"
							+ " WHERE a_id=?");
			stmt.setString(1, award.getAwardClass());
			stmt.setString(2, award.getAffiliation());
			stmt.setString(3, award.getAwardName());
			stmt.setString(4, award.getAwardContent());
			stmt.setString(5, award.getOrganization());
			stmt.setInt(6, award.getId());
			return stmt.executeUpdate();

		} catch (Exception e) {
			throw e;

		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (connection != null)
					connection.close();
			} catch (Exception e) {
			}
		}
	}

}
