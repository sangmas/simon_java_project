package spms.vo;

import java.util.Date;

public class Award {
	protected int id; // 수상
	protected String awardClass; // 수상 등급
	protected String affiliation; // 수상자 소속단체
	protected String awardName; // 상 이름
	protected String awardContent; // 수상 내용
	protected Date receivedDate; // 상받은 날짜
	protected String organization; // 상 준 단체명

	public Award() {

	}

	public Award(int id, String awardClass, String affiliation,
			String awardName, String awardContent, Date receivedDate,
			String organization) {
		this.id = id;
		this.awardClass = awardClass;
		this.affiliation = affiliation;
		this.awardName = awardName;
		this.awardContent = awardContent;
		this.receivedDate = receivedDate;
		this.organization = organization;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAwardClass() {
		return awardClass;
	}

	public void setAwardClass(String awardClass) {
		this.awardClass = awardClass;
	}

	public String getAffiliation() {
		return affiliation;
	}

	public void setAffiliation(String affiliation) {
		this.affiliation = affiliation;
	}

	public String getAwardName() {
		return awardName;
	}

	public void setAwardName(String awardName) {
		this.awardName = awardName;
	}

	public String getAwardContent() {
		return awardContent;
	}

	public void setAwardContent(String awardContent) {
		this.awardContent = awardContent;
	}

	public Date getReceivedDate() {
		return receivedDate;
	}

	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

}
