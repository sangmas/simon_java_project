package process;

import java.util.Comparator;
import java.util.Scanner;


public class Process
{	
	public int burst_sec, arrival_time, priority, alloc_size, waitTime, exeTime;
	public String name = "";	
	public static Comparator<Process> arrivalComparator = new Comparator<Process>() {
		public int compare(Process p1, Process p2) {
			return p1.arrival_time - p2.arrival_time;
		}
	};
	
	//버스트 시간에 대해 정렬
	public static Comparator<Process> burstComparator = new Comparator<Process>() {
		public int compare(Process p1, Process p2) {
			return p1.burst_sec - p2.burst_sec; //큰 값이 나오면 비교
			//보통 1, 0, -1 로 구분한다.
		}
	};

	public static Comparator<Process> abComparator = new Comparator<Process>() {
		public int compare(Process p1, Process p2) {
			if (p1.arrival_time == p2.arrival_time)
				return p1.burst_sec - p2.burst_sec;
			else
				return p1.arrival_time - p2.arrival_time;
		}
	};	
	
	public static Comparator<Process> pabComparator = new Comparator<Process>() {
		public int compare(Process p1, Process p2) {
			if (p1.priority == p2.priority)
				if (p1.arrival_time == p2.arrival_time)
					return p1.burst_sec - p2.burst_sec;
				else					
					return p1.arrival_time - p2.arrival_time;
			else
				return p1.priority - p2.priority;
		}
	};
	
	static Scanner input = new Scanner(System.in);			

	public Process() {	
		
		String proname ="";
		int a,b,c,d;		
		
		System.out.println("프로세스 이름 입력해봐 : ");
			proname = input.next();
		System.out.println("버스트 시간은 몇으로 할거야? : ");
			a = input.nextInt();
		System.out.println("도착시간은? : ");
			b = input.nextInt();		
		System.out.println("우선권은? 몇?");
			c = input.nextInt();
			/*
		System.out.println("할당되어질 메모리 사이즈는? : ");
			d = input.nextInt();
		System.out.println("Done.\n\n");*/
		
		this.name = proname;
		this.burst_sec = a;
		this.arrival_time = b;
		this.priority = c;
		//this.alloc_size = d;			
		
		}
	
	public void printProcess(){
		
		System.out.println("Process Name : " +name);
		System.out.println("Burst Time : " +burst_sec);
		System.out.println("Arrival Time : " +arrival_time);		
		System.out.println("Priority : " +priority);
		System.out.println("---------------------");
		//System.out.println("Memory Size to be allocated : " +alloc_size+"\n\n");
				
	   }		

	
	public static void main(String[] args)
	{	
		
		int select;		
		while(true)
		{					
			System.out.println("\n==========Welcome to CPU Scheduling world.==========\n");
			System.out.println("하고싶은 스케줄링 방법을 선택하시오.\n");
			System.out.println("1. FCFS   2. SJF   3. Priority   4. RR \n");
			select = input.nextInt();
					
			ProcessHandler handler = new ProcessHandler();
						
			switch(select)
			{
			case 1:				
				handler.FCFS();			
				break;
			case 2:		
				handler.SJF();
				break;			
			case 3:			
				handler.PriorityScheduling();
				break;
			case 4:			
				handler.RR();
				break;
			default:
				System.out.println("please try again.\n");
				break;
			}				
		}
	} //main ends	
}
		


