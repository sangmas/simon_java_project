package process;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class RoundThread extends Thread 
{
	private final ReentrantLock entLock = new ReentrantLock();
	private final Condition readCond=entLock.newCondition();
	private final Condition writeCond=entLock.newCondition();
	//String threadName;
	
	RoundArray r;
	public RoundThread(RoundArray ra)
	{		
		r=ra;
	}
	
	public void run()
	{				
		entLock.lock();
		System.out.println("\n");
		System.out.println("===========Value : "+r.value+" ===========");
		System.out.println("process Number : "+r.pNum+" ===========");		
		try {
			sleep(1000); //실제시간처럼 보이게
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			entLock.unlock();
		}
		
	}
	
}
