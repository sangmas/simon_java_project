package slowSerial;

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import gnu.io.UnsupportedCommOperationException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.TooManyListenersException;

public class slowRxTx implements Runnable, SerialPortEventListener {
	private static final byte A = 0;
	private static final byte B = 0;
	private static final byte C = 0;
	private static final byte E = 0;
	private static final byte D = 0;
	private static final byte F = 0;
	static CommPortIdentifier portId;
	static Enumeration<CommPortIdentifier> portList;
	static CommPort commPort;
	static InputStream inputStream;
	static OutputStream outputStream;
	static SerialPort serialPort;
	Thread readThread;//////////////////////////////////////////////////////////////////////////주석처리해도 잘됨
	static String targetPort = "COM3";                                     //기본시리얼포트	
	static private int maxMsgBufferSize = 50;                              //메시지크기의 2배 -1
	static String saveRecivePacket = "";                                   //수신 메시지 패킷
	static String switchCommand = "";
	public String DemonStr ="";
	static public String tmpMsg="";
	
	public static slowRxTx xxx = null;
	public static slowRxTx create()
	{
		if (xxx == null)
			xxx = new slowRxTx();
		
		return xxx;		
	}
	
	public void init(String targetPort1) throws Exception{
			System.out.println(" targetPort is : "+targetPort1);
			CommPortIdentifier portIdentifier/*포트 식별자*/ = CommPortIdentifier.getPortIdentifier( targetPort1 );
			int timeout = 2000;
			if( portIdentifier.isCurrentlyOwned() ) {                 //포트가 사용중일때
				System.out.println( "Port is currently in use" );			
				this.tx();
			} else {                                                  //포트가 사용중이지 않을 때 포트연결생성
				commPort/*연결된 포트*/ = portIdentifier/*포트연결생성*/.open(this.getClass().getName(), timeout );
	
				if(commPort instanceof SerialPort ) {   /*연결된 포트가 시리얼 포트로 형변환 가능하다면*/
					serialPort = (SerialPort)commPort;        //형변환시켜서 위에 스태틱으로 설정한 serialPort에 넣는다
					serialPort.setSerialPortParams(9600, //속도설정
							SerialPort.DATABITS_8,
							SerialPort.STOPBITS_1,
							SerialPort.PARITY_NONE );
	
					inputStream = serialPort.getInputStream();  // 위의 스태틱으로 설정한 인풋스트림과 아웃풋스트림에 시리얼통신의 getInputStream과 getOutputStream을 넣는다
					outputStream = serialPort.getOutputStream();
					this.rx();                     //수신
					this.tx();                     //송신
				} else {
					System.out.println( "Error: Only serial ports are handled by this example." );
				}
			}
		}		
		  
	public void rx(){
		try{
			serialPort.addEventListener(this);
		}catch(TooManyListenersException e) {
		}
		serialPort.notifyOnDataAvailable(true);
		try {
			serialPort.setSerialPortParams(9600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
		}catch(UnsupportedCommOperationException e1){
		}
	}
	
	static boolean on = false;
	//byte[] msg = new byte[3];
	//byte[] onMsg = {'A','C','E'};
	//byte[] offMsg = {'B','D','F'};//outputStream.write('\r');
	public void tx(){		
			try {					
					if(on == true){
						//outputStream.write(onMsg, 0, 3);						
						outputStream.write('A');//LED off
						try {
							Thread.sleep(5);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						outputStream.write('C');
						try {
							Thread.sleep(5);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						outputStream.write('E');
						try {
							Thread.sleep(5);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						System.out.println("crt tx A : " + on);
						//System.out.println("dddddddd"+ msg[0]);
						on = false;
					}					
					else{
						//outputStream.write(offMsg, 0, 3);							
						outputStream.write('B');//LED on
						try {
							Thread.sleep(5);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						outputStream.write('D');
						try {
							Thread.sleep(5);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						outputStream.write('F');
						try {
							Thread.sleep(5);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						System.out.println("crt tx B : " + on);						
						on = true;
					}
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
	}	
	
	public void run(){
		System.out.println("\nHere is ComRxTx Thread");
		try {			
				this.init("COM3");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				this.init("COM3");  
			}catch (Exception _e) {}
		}
	}
	
	public void serialEvent(SerialPortEvent event){
		switch(event.getEventType()){
		case SerialPortEvent.BI:
		case SerialPortEvent.OE:
		case SerialPortEvent.FE:
		case SerialPortEvent.PE:
		case SerialPortEvent.CD:
		case SerialPortEvent.CTS:
		case SerialPortEvent.DSR:			
		case SerialPortEvent.RI:
		case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
			break;
		case SerialPortEvent.DATA_AVAILABLE:			
			
			byte[] readBuffer = new byte[1024];
			int numBytes = -1;
			try{
				while(inputStream.available()>8){
					numBytes = inputStream.read(readBuffer);
				}
				
				DemonStr="";
				tmpMsg="";
				tmpMsg = new String(readBuffer, 0, numBytes);
				saveRecivePacket(tmpMsg);
				StringBuffer sb = new StringBuffer();				
				DemonStr=sb.append(DemonStr).append(tmpMsg).toString();
				System.out.println("test : "+DemonStr);								
				DemonStr="";
				tmpMsg="";
			}catch (IOException e){}
			
		} 
	}

	
	public static void saveRecivePacket(String tmpMsg){
		if(maxMsgBufferSize<saveRecivePacket.length()){
			saveRecivePacket = "";
		}
		saveRecivePacket +=tmpMsg;
	} 
		
	public static String getReciveMsg(){
		return saveRecivePacket;
	}

}


