package week2.interpreter.newVer;

import java.util.Scanner;

public class TestDrive {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String[] order = new String[50];
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("몇번 명령 실행할꺼야? :");
		int times = scanner.nextInt(); //몇번할거니?
		int order100Cnt = 0; //100만나면 카운트시킴
		Ram ram = new Ram();
		Register reg = new Register();
		int temRAM, ramCnt = 0, tem;
		String temOrderStr = "";
		int until100 = 0;
		int whatTime=0;
		
			System.out.println("명령할거 입력해라");
			
			for(int i=0;i<order.length;i++){				
				order[i] = scanner.next();					
				ram.ramNumber[i]=order[i]; //램에 명령받은걸 기록
				ramCnt++;
				if(order[i].equals("100")){					
					order100Cnt++;
					until100=ramCnt;					
				}				
				if(order100Cnt==times)
					break;			
			}
			
			
			
			
			for(int i=0;i<order.length;i++)//램에 있는거 출력
				System.out.println(i+" :  "+ram.ramNumber[i]);
			
			
			
			for(int i=0;i<until100;i++){//////////////////////////////////////////////////////////
				
				
					switch(ram.ramNumber[i].charAt(0)){
					
					case '1'://앞부분이 1이면 //종료며 여기까지 입력되어야 한 명령이다
						if(ram.ramNumber[i].charAt(1)=='0' && ram.ramNumber[i].charAt(2)=='0'){
							//order100Cnt++;//100 명령이 몇번인지 세준다
							
							reg.divide();
							reg.printReg();
							
							System.out.println("    RAM "+whatTime+" : "+ram.ramNumber[i]);
							System.out.println();
							whatTime++;
							break;
						}
						else
							System.out.println("1로 시작되는 인코딩은 100뿐이므로 입력하신 인코딩은 잘못된 것입니다. 다시입력합니다.");						
					break;
					
					case '2':			
						reg.regNum[ram.ramNumber[i].charAt(1)-48] = ram.ramNumber[i].charAt(2)-48; //캐릭터에서 48을 빼준게 int 값 //아스키코드가 그러함
						
						reg.divide();
						reg.printReg();
						
						System.out.println("    RAM "+whatTime+" : "+ram.ramNumber[i]);
						System.out.println();
						whatTime++;
					break;
					
					case '3':
						reg.regNum[ram.ramNumber[i].charAt(1)-48] += ram.ramNumber[i].charAt(2)-48;
						
						reg.divide();
						reg.printReg();
						
						System.out.println("    RAM "+whatTime+" : "+ram.ramNumber[i]);
						System.out.println();
						whatTime++;
					break;
						
					case '4':
						reg.regNum[ram.ramNumber[i].charAt(1)-48] *= ram.ramNumber[i].charAt(2)-48;
						
						reg.divide();
						reg.printReg();
						
						System.out.println("    RAM "+whatTime+" : "+ram.ramNumber[i]);
						System.out.println();
						whatTime++;
					break;
					
					case '5':
						reg.regNum[ram.ramNumber[i].charAt(1)-48] = reg.regNum[ram.ramNumber[i].charAt(2)-48];
						
						reg.divide();
						reg.printReg();
						
						System.out.println("    RAM "+whatTime+" : "+ram.ramNumber[i]);
						System.out.println();
						whatTime++;
					break;
					
					case '6':					
						reg.regNum[ram.ramNumber[i].charAt(1)-48] += reg.regNum[ram.ramNumber[i].charAt(2)-48];
						
						reg.divide();
						reg.printReg();
						
						System.out.println("    RAM "+whatTime+" : "+ram.ramNumber[i]);
						System.out.println();
						whatTime++;
					break;
					
					case '7':
						reg.regNum[ram.ramNumber[i].charAt(1)-48] *= reg.regNum[ram.ramNumber[i].charAt(2)-48];
						
						reg.divide();
						reg.printReg();
						
						System.out.println("    RAM "+whatTime+" : "+ram.ramNumber[i]);
						System.out.println();
						whatTime++;
					break;
							
					case '8':						
						temRAM = Integer.parseInt(ram.ramNumber[ram.ramNumber[i].charAt(2)-48]); //892가 들어왓다면 램 2번째 있는 임의의 [세자리 숫자]의 램[세자리숫자] 번호로 이동						
						reg.regNum[ram.ramNumber[i].charAt(1)-48] = Integer.parseInt(ram.ramNumber[temRAM]); //형변환이 지저분하지만 제대로 들어감 
						
						reg.divide();
						reg.printReg();
						
						System.out.println("    RAM "+whatTime+" : "+ram.ramNumber[i]);
						System.out.println();
						whatTime++;
					break;
					
					case '9':
						temRAM = Integer.parseInt(ram.ramNumber[ram.ramNumber[i].charAt(2)-48]);
						ram.ramNumber[temRAM]=Integer.toString(reg.regNum[ram.ramNumber[i].charAt(1)-48]);
						
						reg.divide();
						reg.printReg();
						
						System.out.println("    RAM "+whatTime+" : "+ram.ramNumber[i]);
						System.out.println();
						whatTime++;
					break;
					
					case '0': //ram[i] 앞부분이 0일 경우					
						
						reg.divide();
						reg.printReg();
						
						System.out.println("    RAM "+whatTime+" : "+ram.ramNumber[i]);
						System.out.println();
						whatTime++;
						
						
						/////////////////////////@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@힌트는 arr[tem] |tem++
						if(0<reg.regNum[ram.ramNumber[i].charAt(2)-48] && reg.regNum[ram.ramNumber[i].charAt(2)-48]<1000){					
							
							
							
											
							tem = reg.regNum[ram.ramNumber[i].charAt(1)-48];//7레지스터가 가지고 있는 값을 임시변수에 넣음 그 수는 9일 것임 (예제에서)							
							 //7레지스터의 값인 9가 tem 에들어갔으니깐 689 가 들어갈 것임
							//System.out.println("0명령으로 인해 들어간 명령은"+ram.ramNumber[tem]);
								//여기 변경소스는 ver2에서								
							
							
							switch(ram.ramNumber[tem].charAt(0)){
							
							case '1'://앞부분이 1이면 //종료며 여기까지 입력되어야 한 명령이다
								if(ram.ramNumber[tem].charAt(1)=='0' && ram.ramNumber[tem].charAt(2)=='0'){
									//order100Cnt++;//100 명령이 몇번인지 세준다
									
									reg.divide();
									reg.printReg();
									ram.ramNumber[ramCnt]=ram.ramNumber[tem];
									System.out.println("    RAM "+whatTime+" : "+ram.ramNumber[ramCnt]);
									System.out.println();
									ramCnt++;
									whatTime++;
								}
								else
									System.out.println("1로 시작되는 인코딩은 100뿐이므로 입력하신 인코딩은 잘못된 것입니다. 다시입력합니다.");						
							break;
							
							case '2':			
								reg.regNum[ram.ramNumber[tem].charAt(1)-48] = ram.ramNumber[tem].charAt(2)-48; //캐릭터에서 48을 빼준게 int 값 //아스키코드가 그러함
								
								reg.divide();
								reg.printReg();
								ram.ramNumber[ramCnt]=ram.ramNumber[tem];
								System.out.println("    RAM "+whatTime+" : "+ram.ramNumber[ramCnt]);
								System.out.println();
								ramCnt++;
								whatTime++;
							break;
							
							case '3':
								reg.regNum[ram.ramNumber[tem].charAt(1)-48] += ram.ramNumber[tem].charAt(2)-48;
								
								reg.divide();
								reg.printReg();
								ram.ramNumber[ramCnt]=ram.ramNumber[tem];
								System.out.println("    RAM "+whatTime+" : "+ram.ramNumber[ramCnt]);
								System.out.println();
								ramCnt++;
								whatTime++;
							break;
								
							case '4':
								reg.regNum[ram.ramNumber[tem].charAt(1)-48] *= ram.ramNumber[tem].charAt(2)-48;
								
								reg.divide();
								reg.printReg();
								ram.ramNumber[ramCnt]=ram.ramNumber[tem];
								System.out.println("    RAM "+whatTime+" : "+ram.ramNumber[ramCnt]);
								System.out.println();
								ramCnt++;
								whatTime++;
							break;
							
							case '5':
								reg.regNum[ram.ramNumber[tem].charAt(1)-48] = reg.regNum[ram.ramNumber[tem].charAt(2)-48];
								
								reg.divide();
								reg.printReg();
								ram.ramNumber[ramCnt]=ram.ramNumber[tem];
								System.out.println("    RAM "+whatTime+" : "+ram.ramNumber[ramCnt]);
								System.out.println();
								ramCnt++;
								whatTime++;
							break;
							
							case '6':					
								reg.regNum[ram.ramNumber[tem].charAt(1)-48] += reg.regNum[ram.ramNumber[tem].charAt(2)-48];
								
								reg.divide();
								reg.printReg();
								ram.ramNumber[ramCnt]=ram.ramNumber[tem];
								System.out.println("    RAM "+whatTime+" : "+ram.ramNumber[ramCnt]);
								System.out.println();
								ramCnt++;
								whatTime++;
							break;
							
							case '7':
								reg.regNum[ram.ramNumber[tem].charAt(1)-48] *= reg.regNum[ram.ramNumber[tem].charAt(2)-48];
								
								reg.divide();
								reg.printReg();
								ram.ramNumber[ramCnt]=ram.ramNumber[tem];
								System.out.println("    RAM "+whatTime+" : "+ram.ramNumber[ramCnt]);
								System.out.println();
								ramCnt++;
								whatTime++;
							break;
									
							case '8':						
								temRAM = Integer.parseInt(ram.ramNumber[ram.ramNumber[tem].charAt(2)-48]); //892가 들어왓다면 램 2번째 있는 임의의 [세자리 숫자]의 램[세자리숫자] 번호로 이동						
								reg.regNum[ram.ramNumber[tem].charAt(1)-48] = Integer.parseInt(ram.ramNumber[temRAM]); //형변환이 지저분하지만 제대로 들어감 
								
								reg.divide();
								reg.printReg();
								ram.ramNumber[ramCnt]=ram.ramNumber[tem];
								System.out.println("    RAM "+whatTime+" : "+ram.ramNumber[ramCnt]);
								System.out.println();
								ramCnt++;
								whatTime++;
							break;
							
							case '9':
								temRAM = Integer.parseInt(ram.ramNumber[ram.ramNumber[tem].charAt(2)-48]);
								ram.ramNumber[temRAM]=Integer.toString(reg.regNum[ram.ramNumber[tem].charAt(1)-48]);
								
								reg.divide();
								reg.printReg();
								ram.ramNumber[ramCnt]=ram.ramNumber[tem];
								System.out.println("    RAM "+whatTime+" : "+ram.ramNumber[ramCnt]);
								System.out.println();
								ramCnt++;
								whatTime++;
							break;			
							
							
						}//switch							
							
							i=tem;							
							
						} //0번 이프문					
							
					break;//0번 break
						
				} //switch 문 종료
				
				
			}
			
			
			System.out.println();
			System.out.println();
			System.out.println("램에 기록된 세자리 정수? : "+whatTime+" 번");
		
	}//main ends

}
