package week3.poker;

public class Result {
	Card card = new Card();
	
	//초기값 false 확인 이 값을 리턴 받을 블랙, 화이트라는 객체가 필요
	int highCard, onePair, twoPair, threeCard, straight, flush, fullHouse, fourCard, straightFlush;//각 1~9점
	int blackScore, blackHighCard, whiteScore, whiteHighCard; //여기에 하이카드는 1점 원페어는 2점 이런식으로 매겨서 승패가림
	int high;
	int sameNumber;	
	
	public void arrangeCard(){
					
		for(int i=0;i<card.blackCardStatus.length;i++){
			if(card.blackCardStatus[i]==2){//one pair
				onePair+=1;
				high=(i+2);
			}
			else if(card.blackCardStatus[i]==3){
				threeCard+=1;
				high=(i+2);
			}
			else if(card.blackCardStatus[i]==4){
				fourCard+=1;
				high=(i+2);
			}
			else if(card.blackCardStatus[i]==1){
				highCard+=1;
				if(highCard==5){
					high=(i+2); //i가 2 작게 설계함 첫 패가 2니까 //그리고 순서대로 도니까 마지막 i를 저장					
				}
			}			
		}
		
		for(int i=0;i<card.blackCardStatus.length-1/*요기는 예외처리*/;i++){
			if(card.blackCardStatus[i]==1 && card.blackCardStatus[i+1]==1){
				sameNumber+=1;		//이게 4면 스트레이트				
			}	
		}
	}	
	
	public void judgeCard(){		
		
		if(highCard == 5 && card.blackCardFlush==false && sameNumber!=4){						
			highCard=1;			
			System.out.println("high card : "+high);			
		}
		else if(onePair==1 && threeCard==0){
			highCard=0;
			System.out.println("one pair : "+high);			
		}
		else if(onePair==2){
			highCard=0;
			onePair=0; //원페어 두개니까 초기화시키고
			twoPair+=1; //투페어로 만들어줌
			System.out.println("two pair : "+high);
		}
		else if(threeCard==1 && onePair==0){
			highCard=0;
			System.out.println("three card :"+high);
		}
		
		else if(sameNumber==4 && highCard == 5 && card.blackCardFlush==false){
			highCard=0;
			straight=1;
			System.out.println("straight : "+high);
		}		
		
		else if(card.blackCardFlush==true && highCard == 5 && sameNumber!=4){
			highCard=0;
			System.out.println("flush : "+high);
		}
		else if(threeCard==1 && onePair==1){
			highCard=0;
			onePair=0;
			threeCard=0;
			fullHouse+=1;
			System.out.println("full house : "+high);
		}
		else if(fourCard==1){
			highCard=0;
			System.out.println("four card"+high);
		}		
		
		else if(sameNumber==4 && highCard == 5 && card.blackCardFlush==true){
			highCard=0;
			straightFlush=1;
			System.out.println("straight flush : "+high);
		}
		
		
		
		//5의 배수이며 5*(i+2)+10 = 20  && 2에 1이있고 6에 1이 있고
		//위의 이프문들 사이에 black 넣어서 점수 주고 // 이쯤에서 다 초기화하고 화이트 반복
		
	}
}
