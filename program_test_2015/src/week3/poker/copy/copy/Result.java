package week3.poker.copy.copy;

public class Result {
	Card card = new Card();
	
	//초기값 false 확인 이 값을 리턴 받을 블랙, 화이트라는 객체가 필요
	int highCard, onePair, twoPair, threeCard, straight, flush, fullHouse, fourCard, straightFlush;//각 1~9점
	int blackScore, blackHighCard, whiteScore, whiteHighCard; //여기에 하이카드는 1점 원페어는 2점 이런식으로 매겨서 승패가림
	int high, low; //ex) 투페어면 투페어의 최고의 숫자의 값이 high // highNext는 투페어 제외하고 남은 패 //low 는 백스트레이트 구분
	int sameNumber;	
	int cnt=0;
	int blackNext2, blackNext3, blackNext4, blackNext5;
	int whiteNext2, whiteNext3, whiteNext4, whiteNext5;	
	
	public void cardInit(){ //화이트를 판별하기 전에 초기화시켜줌
		highCard = 0;
		onePair = 0;
		twoPair = 0;
		threeCard = 0;
		straight = 0;
		flush = 0;
		fullHouse = 0;
		fourCard = 0;
		straightFlush = 0;
		
		high = 0;
		low = 0;
		cnt = 0;
				
		sameNumber = 0;		
	}
	
	public void blackArrangeCard(){
			
		if(card.blackCardStatus[0]==1){ //백스트레이트 판별 외엔 쓰이지 않는다..
			low = 2;
		}	
		
		for(int i=0;i<card.blackCardStatus.length;i++){
			if(card.blackCardStatus[i]==2){//one pair
				onePair+=1;
				high=(i+2);
			}
			else if(card.blackCardStatus[i]==3){
				threeCard+=1;
				high=(i+2);
			}
			else if(card.blackCardStatus[i]==4){
				fourCard+=1;
				high=(i+2);
			}
			else if(card.blackCardStatus[i]==1){
				highCard+=1;				
				if(highCard==5){
					high=(i+2); //i가 2 작게 설계함 첫 패가 2니까 //그리고 순서대로 도니까 마지막 i를 저장					
				}				
			}
			else if(card.blackCardStatus[i]==5)
				System.out.println(" * Black card error");		
		}	
		
		for(int i=0;i<card.blackCardStatus.length;i++){////////////////////////////////////////////
			if(card.blackCardStatus[i]==1){	
				cnt++;				
				if(cnt==1) //5번 찾기 직전거를 저장해놓음 //하이카드 다음꺼판별
					blackNext5=(i+2);
				else if(cnt==2)
					blackNext4=(i+2);
				else if(cnt==3)
					blackNext3=(i+2);
				else if(cnt==4)
					blackNext2=(i+2);
				//젤큰건 처리함.. 여기서 다시 해주면 좋지만 귀찮음..								
			}			
		}	
				
		for(int i=0;i<card.blackCardStatus.length-1/*요기는 예외처리*/;i++){ //스트레이트 판별
			if(card.blackCardStatus[i]==1 && card.blackCardStatus[i+1]==1){
				sameNumber+=1;		//이게 4면 스트레이트				
			}	
		}
	}	
	
	public void blackJudgeCard(){		
		
		if(onePair==1 && threeCard==0 && card.blackCardFlush==false){
			highCard=0;
			System.out.println("one pair : "+high);
			blackScore=2;
			blackHighCard = high;
		}
		else if(onePair==2 && card.blackCardFlush==false){
			highCard=0;
			onePair=0; //원페어 두개니까 초기화시키고
			twoPair+=1; //투페어로 만들어줌
			System.out.println("two pair : "+high);
			blackScore=3;
			blackHighCard = high;
		}
		else if(threeCard==1 && onePair==0 && card.blackCardFlush==false){
			highCard=0;
			System.out.println("three card :"+high);
			blackScore=4;
			blackHighCard = high;
		}
		
		else if(sameNumber>=0 && highCard == 5 && card.blackCardFlush==false){
			if(sameNumber==4){
				highCard=0;
				straight=1;
				System.out.println("straight : "+high);
				blackScore=5;
				blackHighCard = high;
			}
			else if(sameNumber==3 && high==14 && low==2){ //백스트레이트: 이건 5스트레이트랑 마찬가지라고 합니다.. 따라서 6스트레이트가 이긴거임
				highCard=0;
				straight=1;
				blackHighCard = high;
				high = 5; //하이를 임의로 바꿔줘야함
				System.out.println("straight : "+high);
				blackScore=5;				
			}
			else{
				highCard=1;			
				System.out.println("high card : "+high);
				blackScore=1;
				blackHighCard = high;
			}
		}
		
		else if(card.blackCardFlush==true && sameNumber<4 && fourCard!=1){
			highCard=0;
			System.out.println("flush : "+high);
			blackScore=6;
			blackHighCard = high;
		}
		else if(threeCard==1 && onePair==1){
			highCard=0;
			onePair=0;
			threeCard=0;
			fullHouse+=1;
			System.out.println("full house : "+high);
			blackScore=7;
			blackHighCard = high;
		}
		else if(fourCard==1){
			highCard=0;
			System.out.println("four card : "+high);
			blackScore=8;
			blackHighCard = high;
		}		
		
		else if(sameNumber==4 && highCard == 5 && card.blackCardFlush==true){
			highCard=0;
			straightFlush=1;
			System.out.println("straight flush : "+high);
			blackScore=9;
			blackHighCard = high;
		}	
		else
			System.out.println(" ** Black card error");			
	}
	
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ WHITE
	public void whiteArrangeCard(){
		
		if(card.whiteCardStatus[0]==1){ //백스트레이트 판별 외엔 쓰이지 않는다..
			low = 2;
		}
		
		for(int i=0;i<card.whiteCardStatus.length;i++){
			if(card.whiteCardStatus[i]==2){//one pair
				onePair+=1;
				high=(i+2);
			}
			else if(card.whiteCardStatus[i]==3){
				threeCard+=1;
				high=(i+2);
			}
			else if(card.whiteCardStatus[i]==4){
				fourCard+=1;
				high=(i+2);
			}
			else if(card.whiteCardStatus[i]==1){
				highCard+=1;
				if(highCard==5){
					high=(i+2); //i가 2 작게 설계함 첫 패가 2니까 //그리고 순서대로 도니까 마지막 i를 저장					
				}
			}
			else if(card.whiteCardStatus[i]==5)
				System.out.println(" * White card error");			
		}
		
		for(int i=0;i<card.whiteCardStatus.length;i++){
			if(card.whiteCardStatus[i]==1){
				cnt++;				
				if(cnt==1) //5번 찾기 직전거를 저장해놓음 //하이카드 다음꺼판별
					whiteNext5=(i+2);
				else if(cnt==2)
					whiteNext4=(i+2);
				else if(cnt==3)
					whiteNext3=(i+2);
				else if(cnt==4)
					whiteNext2=(i+2);
				//젤큰건 처리함.. 여기서 다시 해주면 좋지만 귀찮음..
			}
		}		
		
		
		for(int i=0;i<card.whiteCardStatus.length-1/*요기는 예외처리*/;i++){
			if(card.whiteCardStatus[i]==1 && card.whiteCardStatus[i+1]==1){
				sameNumber+=1;		//이게 4면 스트레이트				
			}	
		}
	}	
	
	public void whiteJudgeCard(){		
				
		if(onePair==1 && threeCard==0 && card.whiteCardFlush==false){
			highCard=0;
			System.out.println("one pair : "+high);
			whiteScore=2;
			whiteHighCard = high;
		}
		else if(onePair==2 && card.whiteCardFlush==false){
			highCard=0;
			onePair=0; //원페어 두개니까 초기화시키고
			twoPair+=1; //투페어로 만들어줌
			System.out.println("two pair : "+high);
			whiteScore=3;
			whiteHighCard = high;
		}
		else if(threeCard==1 && onePair==0 && card.whiteCardFlush==false){
			highCard=0;
			System.out.println("three card :"+high);
			whiteScore=4;
			whiteHighCard = high;
		}
		
		else if(sameNumber>=0 && highCard == 5 && card.whiteCardFlush==false){
			if(sameNumber==4){
				highCard=0;
				straight=1;
				System.out.println("straight : "+high);
				whiteScore=5;
				whiteHighCard = high;
			}
			else if(sameNumber==3 && high==14 && low==2){ //백스트레이트: 이건 5스트레이트랑 마찬가지라고 합니다.. 따라서 6스트레이트가 이긴거임
				highCard=0;
				straight=1;
				whiteHighCard = high;
				high = 5; //하이를 임의로 바꿔줘야함
				System.out.println("straight : "+high);
				whiteScore=5;				
			}
			else{
				highCard=1;			
				System.out.println("high card : "+high);
				whiteScore=1;
				whiteHighCard = high;
			}
		}
		
		else if(card.whiteCardFlush==true && sameNumber<4 && fourCard!=1){
			highCard=0;
			System.out.println("flush : "+high);
			whiteScore=6;
			whiteHighCard = high;
		}
		else if(threeCard==1 && onePair==1){
			highCard=0;
			onePair=0;
			threeCard=0;
			fullHouse+=1;
			System.out.println("full house : "+high);
			whiteScore=7;
			whiteHighCard = high;
		}
		else if(fourCard==1){
			highCard=0;
			System.out.println("four card : "+high);
			whiteScore=8;
			whiteHighCard = high;
		}		
		
		else if(sameNumber==4 && highCard == 5 && card.whiteCardFlush==true){
			highCard=0;
			straightFlush=1;
			System.out.println("straight flush : "+high);
			whiteScore=9;
			whiteHighCard = high;
		}	
		else
			System.out.println(" ** White card error");	
	}//
	
	
	
	
	
	
}
