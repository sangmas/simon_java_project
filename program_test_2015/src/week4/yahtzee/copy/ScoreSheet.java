package week4.yahtzee.copy;

import java.util.Scanner;

public class ScoreSheet {
	
	int[] yahtzeeScore = new int[13];
	boolean[] yahtzeeScoreUsed = new boolean[13];
	Scanner scan;
	Dice dice;	
	boolean threeOfKind = false, fourOfKind = false, yahtzee = false, fullHouse = false;
	boolean gameOver = false;
	boolean turnExpire = false;
	int straightJudge = 0;
	int cntTurn;
	int select;
	
	
	
	public ScoreSheet(){
		dice = new Dice();
	}
	
	public void diceRoll(){
		dice.rollDice();
		dice.arrange();
		dice.showDiceStatus();
	}
	
	public void printScoreTable(){		
		 //각 항목은 boolean 값을 걸어놔야 겠음
		System.out.println("---------------------------------------");
		System.out.println("1. Aces");
		System.out.println("2. Twos");
		System.out.println("3. Threes");
		System.out.println("4. Fours");
		System.out.println("5. Fives");
		System.out.println("6. Sixes");
		System.out.println("7. Chance");
		System.out.println("8. 3 of a Kind");
		System.out.println("9. 4 of a Kind");
		System.out.println("10. Yahtzee (50 points)");
		System.out.println("11. Small Straight (25 points)");
		System.out.println("12. Large Straight (35 points)");
		System.out.println("13. Full House (40 points)");
		System.out.println("---------------------------------------");
		System.out.println("◆ 한번 선택한 점수는 다음 차례에 재선택이 불가합니다. 점수를 선택하세요 : ");				
	}	

	
	public void judgeScore(){
				
		for(int i=0;i<dice.diceStatus.length;i++){
			if(dice.diceStatus[i]>=3)
				threeOfKind = true;
			else if(dice.diceStatus[i]>=4)
				fourOfKind = true;
			else if(dice.diceStatus[i]>=5)
				yahtzee = true;
			else if(dice.diceStatus[i]>=2 && dice.diceStatus[i]>=3)
				fullHouse = true;
		}		
		
		for(int i=0;i<dice.diceStatus.length-1/*요기는 예외처리*/;i++){ //스트레이트 판별
			if(dice.diceStatus[i]>=1 && dice.diceStatus[i+1]>=1){
				straightJudge+=1;		//이게 3이면 스몰 스트레이트, 4면 라지 스트레이트				
			}
			else
				straightJudge = 0;		
		}
	}		
		
	public boolean turnExpire(){//수정요망
		for(int i=0;i<yahtzeeScoreUsed.length;i++){
			if(yahtzeeScoreUsed[i]==true){
				cntTurn+=1;			
				if(cntTurn==13){
					System.out.println("턴 초과");
					turnExpire = true;
				}
			}
		}		
		return turnExpire;
	}
	
	
	public void inputNumber(){
		scan = new Scanner(System.in);		
		select = scan.nextInt();	
		scan.close();
	}
		
	public void selectScore(){
				
		if(turnExpire()==false){
			this.cntTurn = 0;
			inputNumber();						
			switch(select){
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
				if(yahtzeeScoreUsed[select - 1]==false){
					yahtzeeScore[select - 1]=(select*dice.diceStatus[select - 1]);
					yahtzeeScoreUsed[select - 1]=true;		
				}
				break;
				
			/*case 1: if(yahtzeeScoreUsed[0]==false){
						yahtzeeScore[0]=(1*dice.diceStatus[0]);
						yahtzeeScoreUsed[0]=true;		
					}
			break;
			case 2: if(yahtzeeScoreUsed[1]==false){
						yahtzeeScore[1]=(2*dice.diceStatus[1]);
						yahtzeeScoreUsed[1]=true;		
			}
			break;
			case 3: if(yahtzeeScoreUsed[2]==false){
						yahtzeeScore[2]=(3*dice.diceStatus[2]);
						yahtzeeScoreUsed[2]=true;		
					}
			break;
			case 4: if(yahtzeeScoreUsed[3]==false){
						yahtzeeScore[3]=(4*dice.diceStatus[3]);
						yahtzeeScoreUsed[3]=true;		
					}
			break;
			case 5: if(yahtzeeScoreUsed[4]==false){
						yahtzeeScore[4]=(5*dice.diceStatus[4]);
						yahtzeeScoreUsed[4]=true;		
					}
			break;
			case 6: if(yahtzeeScoreUsed[5]==false){
						yahtzeeScore[5]=(6*dice.diceStatus[5]);
						yahtzeeScoreUsed[5]=true;		
					}
			break;*/
			case 7: if(yahtzeeScoreUsed[6]==false){
						yahtzeeScore[6]=(1*dice.diceStatus[0])+(2*dice.diceStatus[1])+(3*dice.diceStatus[2])+(4*dice.diceStatus[3])+(5*dice.diceStatus[4])+(6*dice.diceStatus[5]);
						yahtzeeScoreUsed[6]=true;		
					} //chance
			break;
			case 8:	
				if(yahtzeeScoreUsed[7]==false){
					if(threeOfKind == true){ 
						yahtzeeScore[7]=(1*dice.diceStatus[0])+(2*dice.diceStatus[1])+(3*dice.diceStatus[2])+(4*dice.diceStatus[3])+(5*dice.diceStatus[4])+(6*dice.diceStatus[5]);
					}
					else
						yahtzeeScore[7]=0;
					yahtzeeScoreUsed[7]=true;	
				}					
			break;
			case 9: 
				if(yahtzeeScoreUsed[8]==false){
					if(fourOfKind == true){ 
						yahtzeeScore[8]=(1*dice.diceStatus[0])+(2*dice.diceStatus[1])+(3*dice.diceStatus[2])+(4*dice.diceStatus[3])+(5*dice.diceStatus[4])+(6*dice.diceStatus[5]);
					}
					else
						yahtzeeScore[8]=0;
					yahtzeeScoreUsed[8]=true;	
				}
			break;
			case 10: 
				if(yahtzeeScoreUsed[9]==false){
					if(yahtzee == true){ 
						yahtzeeScore[9]=50;
					}
					else
						yahtzeeScore[9]=0;
					yahtzeeScoreUsed[9]=true;	
				} 
				break;
			case 11://small straight
				if(yahtzeeScoreUsed[10]==false){
					if(straightJudge >= 3){ 
						yahtzeeScore[10]=25;
					}
					else
						yahtzeeScore[10]=0;
					yahtzeeScoreUsed[10]=true;	
				} 
			break;
			case 12: 
				if(yahtzeeScoreUsed[11]==false){
					if(straightJudge >= 4){ 
						yahtzeeScore[11]=35;
					}
					else
						yahtzeeScore[11]=0;
					yahtzeeScoreUsed[11]=true;	
				} 
			break;
			case 13:
				if(yahtzeeScoreUsed[12]==false){
					if(fullHouse == true){ 
						yahtzeeScore[12]=40;
					}
					else
						yahtzeeScore[12]=0;
					yahtzeeScoreUsed[12]=true;	
				} 			
			break;
			default: System.out.println("◆ 알맞은 수를 입력해 주세요."); break;
			}//switch		
		}//if문 턴 종료
		else{
			System.out.println("게임이 종료되었습니다.");
			this.gameOver = true;
		}
	}//selectScore
	
	public void printScore(){
		
		int totalScore = 0;
		for(int i=0;i<yahtzeeScore.length;i++){
			totalScore+=yahtzeeScore[i];
		}
		int BonusScore = 0, temp = 0;
		for(int i=0;i<6;i++){
			temp+=yahtzeeScore[i];
			if(temp>=63)
				BonusScore = 35;
		}
		
		System.out.println();
		System.out.println("@@@@@@@@@@@@@@@@@ 현 재 점 수 @@@@@@@@@@@@@@@@@");
		System.out.println("1. Aces : " + yahtzeeScore[0]);
		System.out.println("2. Twos : " + yahtzeeScore[1]);
		System.out.println("3. Threes : " + yahtzeeScore[2]);
		System.out.println("4. Fours : " + yahtzeeScore[3]);
		System.out.println("5. Fives : " + yahtzeeScore[4]);
		System.out.println("6. Sixes : " + yahtzeeScore[5]);
		System.out.println("7. Chance : " + yahtzeeScore[6]);
		System.out.println("8. 3 of a Kind : " + yahtzeeScore[7]);
		System.out.println("9. 4 of a Kind : " + yahtzeeScore[8]);
		System.out.println("10. Yahtzee (50 points) : " + yahtzeeScore[9]);
		System.out.println("11. Small Straight (25 points) : " + yahtzeeScore[10]);
		System.out.println("12. Large Straight (35 points) : " + yahtzeeScore[11]);
		System.out.println("13. Full House (40 points) : " + yahtzeeScore[12]);
		System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		System.out.println("Bonus :" + BonusScore);
		System.out.println("Total : " + totalScore);
		System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		System.out.println();	
		
	}	
	
}
